package com.maker.soap;

// NO OLVIDAR CAMBIAR EL PUERTO 500xx

import java.util.ArrayList;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import com.maker.lanuevasolodos.CALLE_ALTURA;
import com.maker.lanuevasolodos.Direccion;
import com.maker.lanuevasolodos.R;
import com.maker.lanuevasolodos.Respuesta;
import com.maker.lanuevasolodos.ULTIMO_VIAJE;

import android.content.Context;
import android.util.Base64;

public class conexionServicioWebSOAP {

	Context contexto;

	public conexionServicioWebSOAP(Context _contexto)
	{
		contexto = _contexto;
	}

    public Respuesta call_GuardarMsg_obtRespuesta_esquina(String _area, String _telefono, String _texto, String _version, String _localidad)
    {
        Respuesta _rta = new Respuesta();

        try
        {
            _rta = GuardarMsg_obtRespuesta_internet(_area, _texto, _texto, _telefono, _version, _localidad);
        }
        catch (Exception ex)
        {
            _rta.setMostrarAyuda(true);
        }

        return _rta;
    }

    // ------------------------------------------
    // Desencriptar una cadena
    // ------------------------------------------
    public Respuesta GuardarMsg_obtRespuesta(String _area, String _telefono, String _texto,  String _direccion, String _version, String _localidad)
    {
        Respuesta _rta = new Respuesta();

        try
        {
            _rta = GuardarMsg_obtRespuesta_internet(_area, _texto, _direccion, _telefono, _version, _localidad);
        }
        catch (Exception ex)
        {
            _rta.setMostrarAyuda(true);
        }

        return _rta;
    }

    // ------------------------------------------
    // Obtener de internet la clave
    // ------------------------------------------
    private Respuesta GuardarMsg_obtRespuesta_esquina(String _area, String _texto, String _telefono, String _version, String _localidad) {

        Respuesta _rta = new Respuesta();

        final String NAMESPACE = "makersevicios.com.ar";
        final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Guardar_mensaje_y_obtener_rta";
        final String METHOD_NAME = "Guardar_mensaje_y_obtener_rta";
        final String SOAP_ACTION = "makersevicios.com.ar/Guardar_direccion_completa_y_obtener_rta";

        // Crear el objeto JABON (SOAP)
        SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

        // Agregar parámetros
        soapObject.addProperty("_mensaje", _texto);
        soapObject.addProperty("_numero", _telefono);
        soapObject.addProperty("_area", _area);
        soapObject.addProperty("_version", _version);
        soapObject.addProperty("_localidad", _localidad);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(soapObject);

        // Capa de transporte del HTTP con un timeout de 2500 milisegundos
        HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

        try
        {
            transporte.call(SOAP_ACTION, envelope);

            // Para tipos primitivos (int, string)
            //SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

            // Para objectos
            SoapObject resultado = (SoapObject) envelope.getResponse();

            // Log.e("valor","cantidad de propiedades: " + resultado.getPropertyCount());

            // Control de version
            _rta.setVersionIncorrecta(Boolean.parseBoolean(resultado.getPropertyAsString("_versionIncorrecta")));
            _rta.setActualizacionObligatoria(Boolean.parseBoolean(resultado.getPropertyAsString("_actualizacionObligatoria")));
            _rta.setURI(resultado.getPropertyAsString("_uri"));

            // Servicio on line ?
            _rta.setServicioOffLine(Boolean.parseBoolean(resultado.getPropertyAsString("_servicioOffLine")));

            // resto
            _rta.setCallesAlternativas(Boolean.parseBoolean(resultado.getPropertyAsString("_callesAlternativas")));
            _rta.setDireccionInvalida(Boolean.parseBoolean(resultado.getPropertyAsString("_direccionInvalida")));
            _rta.setMostrarAyuda(Boolean.parseBoolean(resultado.getPropertyAsString("_mostrarAyuda")));
            _rta.setNoPuedeSolicitar(Boolean.parseBoolean(resultado.getPropertyAsString("_noPuedeSolicitar")));
            _rta.setOtraAgencia(Boolean.parseBoolean(resultado.getPropertyAsString("_otraAgencia")));
            _rta.setPedirConfirmacion(Boolean.parseBoolean(resultado.getPropertyAsString("_pedirConfirmacion")));
            _rta.setViajeReciente(Boolean.parseBoolean(resultado.getPropertyAsString("_viajeReciente")));
            _rta.setTexto(resultado.getPropertyAsString("_texto"));
            _rta.setDireccion(resultado.getPropertyAsString("_direccion"));
            _rta.setAbrLocalidad(resultado.getPropertyAsString("_abrLocalidad"));
            _rta.setCalle1(resultado.getPropertyAsString("_calle"));

            if (!resultado.getPropertyAsString("_esquina").isEmpty() )
                _rta.setCalle2(resultado.getPropertyAsString("_esquina"));
            else
                _rta.setCalle2(" ");
        }
        catch (Exception e)
        {
            _rta.setMostrarAyuda(true);

            return _rta;
        }

        return _rta;
    }

    // ------------------------------------------
	// Obtener de internet la clave
	// ------------------------------------------
	private Respuesta GuardarMsg_obtRespuesta_internet(String _area, String _texto, String _direccion, String _telefono, String _version, String _localidad) {

		Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Guardar_direccion_completa_y_obtener_rta";
		//final String URL = "http://192.168.0.209:50366/Pedidos.asmx"; //para pruebas con el WS corriendo en localhost
		final String METHOD_NAME = "Guardar_direccion_completa_y_obtener_rta";
		final String SOAP_ACTION = "makersevicios.com.ar/Guardar_direccion_completa_y_obtener_rta";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_area", _area);
		soapObject.addProperty("_mensaje", _texto);
		soapObject.addProperty("_direccionamostrar", _direccion);
		soapObject.addProperty("_numero", _telefono);

		soapObject.addProperty("_version", _version);
		soapObject.addProperty("_localidad", _localidad);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para tipos primitivos (int, string)
			//SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();

			// Log.e("valor","cantidad de propiedades: " + resultado.getPropertyCount());

			// Control de version
			_rta.setVersionIncorrecta(Boolean.parseBoolean(resultado.getPropertyAsString("_versionIncorrecta")));
			_rta.setActualizacionObligatoria(Boolean.parseBoolean(resultado.getPropertyAsString("_actualizacionObligatoria")));
			_rta.setURI(resultado.getPropertyAsString("_uri"));

			// Servicio on line ?
			_rta.setServicioOffLine(Boolean.parseBoolean(resultado.getPropertyAsString("_servicioOffLine")));

			// resto
			_rta.setCallesAlternativas(Boolean.parseBoolean(resultado.getPropertyAsString("_callesAlternativas")));
			_rta.setDireccionInvalida(Boolean.parseBoolean(resultado.getPropertyAsString("_direccionInvalida")));
			_rta.setMostrarAyuda(Boolean.parseBoolean(resultado.getPropertyAsString("_mostrarAyuda")));
			_rta.setNoPuedeSolicitar(Boolean.parseBoolean(resultado.getPropertyAsString("_noPuedeSolicitar")));
			_rta.setOtraAgencia(Boolean.parseBoolean(resultado.getPropertyAsString("_otraAgencia")));
			_rta.setPedirConfirmacion(Boolean.parseBoolean(resultado.getPropertyAsString("_pedirConfirmacion")));
			_rta.setViajeReciente(Boolean.parseBoolean(resultado.getPropertyAsString("_viajeReciente")));
			_rta.setTexto(resultado.getPropertyAsString("_texto"));
			_rta.setDireccion(resultado.getPropertyAsString("_direccion"));
			_rta.setAbrLocalidad(resultado.getPropertyAsString("_abrLocalidad"));
			_rta.setError_conexion(Boolean.parseBoolean(resultado.getPropertyAsString("_errorConexion")));
			_rta.setCalle1(resultado.getPropertyAsString("_calle"));

			if (!resultado.getPropertyAsString("_esquina").isEmpty())
				_rta.setCalle2(resultado.getPropertyAsString("_esquina"));
			else
				_rta.setCalle2(" ");
		}
		catch (Exception e)
		{
			_rta.setError_conexion(true);

			return _rta;
		}

		return _rta;
	}

	// ------------------------------------------
	// Confirmaci?n del pedido
	// ------------------------------------------
	public Respuesta Confirmar_pedido(String _area, String _texto, String _localidad, String _baul, String _cambio, String _comentario) {

		Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Confirmar_pedido_realizado_2";
		final String METHOD_NAME = "Confirmar_pedido_realizado_2";
		final String SOAP_ACTION = "makersevicios.com.ar/Confirmar_pedido_realizado_2";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", _texto);
		soapObject.addProperty("_area", _area);
		soapObject.addProperty("_localidad", _localidad);

		soapObject.addProperty("_baul", _baul);
		soapObject.addProperty("_cambio", _cambio);
		soapObject.addProperty("_mensaje", _comentario);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para tipos primitivos (int, string)
			//SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();

			_rta.setNoHayPedidoRealizado((Boolean.parseBoolean(resultado.getPropertyAsString("_noHayPedidoRealizado"))));
			_rta.setPedidoCancelado((Boolean.parseBoolean(resultado.getPropertyAsString("_pedidoCancelado"))));
			_rta.setNoPuedeSolicitar((Boolean.parseBoolean(resultado.getPropertyAsString("_noPuedeSolicitar"))));
			_rta.setPedidoConfirmado(Boolean.parseBoolean(resultado.getPropertyAsString("_pedidoConfirmado")));

			//	Log.e("valor","llego aca " + _rta.getNoHayPedidoRealizado().toString());
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-pedido " + e.getMessage());
			_rta.setNoPuedeSolicitar(true);
		}

		return _rta;
	}

	// ------------------------------------------
	// Confirmaci?n del pedido
	// ------------------------------------------
	public Respuesta Cancelar_pedido(String _area, String _texto) {

		Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Cancelar_pedido_realizado_2";
		final String METHOD_NAME = "Cancelar_pedido_realizado_2";
		final String SOAP_ACTION = "makersevicios.com.ar/Cancelar_pedido_realizado_2";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", _texto);
		soapObject.addProperty("_area", _area);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);
			SoapObject resultado = (SoapObject) envelope.getResponse();

			_rta.setPedidoCancelado((Boolean.parseBoolean(resultado.getPropertyAsString("_pedidoCancelado"))));
		}
		catch (Exception e)
		{
			_rta.setPedidoCancelado(false);
		}

		return _rta;
	}

	// ------------------------------------------
	// Confirmaci?n del pedido
	// ------------------------------------------
	public String Pedir_codigo_x_sms(String _area, String _telefono) {

		String _rta = "error";

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Solicitar_autorizacion";
		final String METHOD_NAME = "Solicitar_autorizacion";
		final String SOAP_ACTION = "makersevicios.com.ar/Solicitar_autorizacion";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", _telefono);
		soapObject.addProperty("_area", _area);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive response = (SoapPrimitive)envelope.getResponse();

	        _rta= String.valueOf(response.toString());
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-pedido " + e.getMessage());
			_rta = "error";
		}

		return _rta;
	}

	// ------------------------------------------
	// Validar codigo SMS
	// ------------------------------------------
	public Boolean Validar_codigo_x_sms(String _area, String _telefono, String _codigo) {

		Boolean _rta = false;

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Confirmar_autorizacion";
		final String METHOD_NAME = "Confirmar_autorizacion";
		final String SOAP_ACTION = "makersevicios.com.ar/Confirmar_autorizacion";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_area", _area);
		soapObject.addProperty("_numero", _telefono);
		soapObject.addProperty("_clave", _codigo);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive response = (SoapPrimitive)envelope.getResponse();

	        _rta = Boolean.valueOf(response.toString());
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-codigo" + e.getMessage());
			_rta = false;
		}

		return _rta;
	}

	// ------------------------------------------
	// Obtener viaje reciente
	// ------------------------------------------
	public Respuesta Obtener_viaje_reciente(String _area, String _texto) {
	    Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Obtener_viaje_reciente";
		final String METHOD_NAME = "Obtener_viaje_reciente";
		final String SOAP_ACTION = "makersevicios.com.ar/Obtener_viaje_reciente";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", _texto);
		soapObject.addProperty("_area", _area);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 8000 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);
			SoapObject resultado = (SoapObject) envelope.getResponse();

			_rta.setNoHayPedidoRealizado(Boolean.parseBoolean(resultado.getPropertyAsString("_noHayPedidoRealizado")));
			_rta.setNegativo(Boolean.parseBoolean(resultado.getPropertyAsString("_negativo")));
			_rta.setTexto(resultado.getPropertyAsString("_texto"));
			_rta.setTieneMovil(Boolean.parseBoolean(resultado.getPropertyAsString("_tieneMovil")));
			_rta.setYaOcupado(Boolean.parseBoolean(resultado.getPropertyAsString("_yaOcupado")));
			_rta.setMovil(resultado.getPropertyAsString("_Movil"));
			_rta.setNroViaje((Integer.parseInt(resultado.getPropertyAsString("_nroViaje"))));
			_rta.setEliminado(Boolean.parseBoolean(resultado.getPropertyAsString("_eliminado")));
            _rta.setError_conexionServidor(Boolean.parseBoolean(resultado.getPropertyAsString("_errorConexion")));
		}
		catch (Exception e)
		{
			_rta.setError_conexion(true);
		}

		return _rta;
	}

	// ------------------------------------------
	// Obtener viaje reciente
	// _mensaje,  (_aDesarrollador ? "1" : "0"), _telefono
	// ------------------------------------------
	public boolean Enviar_comentarios(String _area, String _mensaje, String _aDesarrollador, String _telefono) {

		Boolean _rta = false;

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Agregar_comentarios_desarrollador";
		final String METHOD_NAME = "Agregar_comentarios_desarrollador";
		final String SOAP_ACTION = "makersevicios.com.ar/Agregar_comentarios_desarrollador";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_area", _area);
		soapObject.addProperty("_numero", _telefono);
		soapObject.addProperty("_comentario", _mensaje);
		soapObject.addProperty("_alDesarrollador", _aDesarrollador);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			//SoapPrimitive response = (SoapPrimitive)envelope.getResponse();

	        _rta = true;
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-codigo" + e.getMessage());
			_rta = false;
		}

		return _rta;
	}

	// ------------------------------------------
	// Obtener estado de servicio
	// ------------------------------------------
	public Respuesta obtenerEstadoServicio(String _area, String _telefono, String _version) {
	    Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Obtener_estado_del_servicio";
		final String METHOD_NAME = "Obtener_estado_del_servicio";
		final String SOAP_ACTION = "makersevicios.com.ar/Obtener_estado_del_servicio";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", _telefono);
		soapObject.addProperty("_area", _area);
		soapObject.addProperty("_version", _version);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);
			SoapObject resultado = (SoapObject) envelope.getResponse();

			// Control de version
			_rta.setVersionIncorrecta(Boolean.parseBoolean(resultado.getPropertyAsString("_versionIncorrecta")));
			_rta.setActualizacionObligatoria(Boolean.parseBoolean(resultado.getPropertyAsString("_actualizacionObligatoria")));
			_rta.setURI(resultado.getPropertyAsString("_uri"));
			_rta.setEsAdministrador(Boolean.parseBoolean(resultado.getPropertyAsString("_esAdmininistrador")));
			_rta.setServicioOffLine(Boolean.parseBoolean(resultado.getPropertyAsString("_servicioOffLine")));
		}
		catch (Exception e)
		{
			_rta.setPedidoCancelado(false);
		}

		return _rta;
	}

	// ------------------------------------------
	// detener iniciar el servicio
	// ------------------------------------------
	public Boolean detenerIniciarServicio(String _detener) {

		Boolean _rta = false;

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Detener_Iniciar";
		final String METHOD_NAME = "Detener_Iniciar";
		final String SOAP_ACTION = "makersevicios.com.ar/Detener_Iniciar";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_detener", _detener);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive response = (SoapPrimitive)envelope.getResponse();

	        _rta = Boolean.parseBoolean(response.getValue().toString());
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-codigo" + e.getMessage());
			_rta = false;
		}

		return _rta;
	}

	// -------------------------------------
	// Obtener calles
	// -------------------------------------
	@SuppressWarnings("unchecked")
	public ArrayList Obtener_calles(String parteNombre, String localidad) {

		ArrayList _rta = new ArrayList<Direccion>();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Obtener_calles";
		final String METHOD_NAME = "Obtener_calles";
		final String SOAP_ACTION = "makersevicios.com.ar/Obtener_calles";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_nombre", parteNombre);
		soapObject.addProperty("_localidad", localidad);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			Direccion _direccion;

			transporte.call(SOAP_ACTION, envelope);

			SoapObject resultado = (SoapObject) envelope.getResponse();

			String calleTmp;
			for (int i = 0; i < 10; i++) {
				calleTmp = resultado.getPropertyAsString(i);
				if (calleTmp.length() > 0)
				{
				_direccion = new Direccion();
				_direccion.setDireccion(calleTmp);

				 _rta.add(_direccion);
				}
			}
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmar-codigo" + e.getMessage());
		}

		return _rta;
	}

	// ------------------------------------------
	// Resolver calle altura por latitud longitud
	// ------------------------------------------
	public CALLE_ALTURA ResolverDireccion(String _latitud, String _longitud, String _version) {

		CALLE_ALTURA _rta = new CALLE_ALTURA();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Obtener_calle_cercana";
		final String METHOD_NAME = "Obtener_calle_cercana";
		final String SOAP_ACTION = "makersevicios.com.ar/Obtener_calle_cercana";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_latitud", _latitud);
		soapObject.addProperty("_longitud", _longitud);
		soapObject.addProperty("_version", _version);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();

			// Control de version
			_rta.setVersionIncorrecta(Boolean.parseBoolean(resultado.getPropertyAsString("_versionIncorrecta")));
			_rta.setActualizacionObligatoria(Boolean.parseBoolean(resultado.getPropertyAsString("_actualizacionObligatoria")));
			_rta.setURI(resultado.getPropertyAsString("_uri"));
			_rta.setServicioOffLine(Boolean.parseBoolean(resultado.getPropertyAsString("_servicioOffLine")));

			if (!resultado.getPropertyAsString("_zona").equals("0"))
			{
				_rta.setLocalidad(resultado.getPropertyAsString("_localidad"));
				_rta.setIdlocalidad(resultado.getPropertyAsString("_idlocalidad"));
				_rta.setZona(resultado.getPropertyAsString("_zona"));
				_rta.setAltura(resultado.getPropertyAsString("_altura"));
				_rta.setCalle(resultado.getPropertyAsString("_calle"));
				_rta.setDesde(resultado.getPropertyAsString("_desde"));
				_rta.setHasta(resultado.getPropertyAsString("_hasta"));
				_rta.setDemora(resultado.getPropertyAsString("_demora"));
			}
			else
				_rta.setHayError(true);

			return _rta;
		}
		catch (Exception e)
		{
			_rta.setHayError(true);
			_rta.setServicioOffLine(true);
		}

		return _rta;
	}

	// ------------------------------------------
	// Pedir viaje por mapa
	// ------------------------------------------
	public Respuesta Confirmar_pedido_mapa(String area, String numero, String latitud,
										   String longitud, String idLocalidad, String localidad,
										   String calleynro, String baul, String cambio, String mensaje,
										   String zona, String version) {

		Respuesta _rta = new Respuesta();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Pedir_viaje_x_mapa";
		final String METHOD_NAME = "Pedir_viaje_x_mapa";
		final String SOAP_ACTION = "makersevicios.com.ar/Pedir_viaje_x_mapa";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_numero", numero);
		soapObject.addProperty("_area", area);
		soapObject.addProperty("_localidad", localidad);
		soapObject.addProperty("_longitud", longitud);
		soapObject.addProperty("_latitud", latitud);
		soapObject.addProperty("_idLocalidad", idLocalidad);
		soapObject.addProperty("_calleynro", calleynro);
		soapObject.addProperty("_baul", baul);
		soapObject.addProperty("_cambio", cambio);
		//soapObject.addProperty("_mensaje", mensaje);
		soapObject.addProperty("_mensaje", "");
		soapObject.addProperty("_zona", zona);
		soapObject.addProperty("_version", version);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();

			_rta.setNoHayPedidoRealizado((Boolean.parseBoolean(resultado.getPropertyAsString("_noHayPedidoRealizado"))));
			_rta.setPedidoCancelado((Boolean.parseBoolean(resultado.getPropertyAsString("_pedidoCancelado"))));
			_rta.setNoPuedeSolicitar((Boolean.parseBoolean(resultado.getPropertyAsString("_noPuedeSolicitar"))));
			_rta.setPedidoConfirmado(Boolean.parseBoolean(resultado.getPropertyAsString("_pedidoConfirmado")));
		}
		catch (Exception e)
		{
			//	Log.e("valor","confirmarPedido" + e.getMessage());
			_rta.setNoPuedeSolicitar(true);
		}

		return _rta;
	}

	// -------------------------------------
	// Obtener el ultimo viaje
	// -------------------------------------
	public ULTIMO_VIAJE Obtener_ultimo_viaje(String numero, String area) {
		ULTIMO_VIAJE _rta = new ULTIMO_VIAJE();

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Obtener_datos_ultimo_viaje";
		final String METHOD_NAME = "Obtener_datos_ultimo_viaje";
		final String SOAP_ACTION = "makersevicios.com.ar/Obtener_datos_ultimo_viaje";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros

		soapObject.addProperty("_telefono", numero);
		soapObject.addProperty("_area", area);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para tipos primitivos (int, string)
			//SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();

			_rta.setOrigen(resultado.getPrimitivePropertySafelyAsString("Origen"));
			_rta.setMovil(resultado.getPrimitivePropertySafelyAsString("Movil"));
			_rta.setChofer(resultado.getPrimitivePropertySafelyAsString("Chofer"));
			_rta.setFecha_hora(resultado.getPrimitivePropertySafelyAsString("Fecha_hora"));
			_rta.setMts(resultado.getPrimitivePropertySafelyAsString("Mts"));
			_rta.setImporte(resultado.getPrimitivePropertySafelyAsString("Importe"));
			_rta.setEspera(resultado.getPrimitivePropertySafelyAsString("Espera"));
			_rta.setNroviaje(resultado.getPrimitivePropertySafelyAsString("Nro_viaje"));
			_rta.setVelmax(resultado.getPrimitivePropertySafelyAsString("Velmax"));
			_rta.setUbicacion_fin(resultado.getPrimitivePropertySafelyAsString("Ubicacion_fin"));
		}
		catch (Exception e)
		{
			//Log.e("valor","confirmarPedido" + e.getMessage());
			_rta.setNroviaje("-1");
		}

		return _rta;
	}

	// -------------------------------------
	// Calificar el ?tlimo viaje (se califica al chofer)
	// -------------------------------------
	public Boolean Calificar_viaje(String nroViaje, String estrellas) {
		Boolean _rta = false;

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://www.makeravl.com.ar:" + contexto.getResources().getString(R.string.DB_puertoServicio) + "/Pedidos.asmx?op=Calificar_viaje";
		final String METHOD_NAME = "Calificar_viaje";
		final String SOAP_ACTION = "makersevicios.com.ar/Calificar_viaje";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Agregar par?metros
		soapObject.addProperty("_nroViaje", nroViaje);
		soapObject.addProperty("_estrellas", "" + estrellas);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 10000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

			_rta= Boolean.valueOf(resultado.toString());
		}
		catch (Exception e)
		{
			_rta = false;
		}

		return _rta;
	}

	// -------------------------------------
	// Hello World Autenticado
	// -------------------------------------
	public String HelloWorld() {

		final String NAMESPACE = "makersevicios.com.ar";
		final String URL = "http://192.168.0.209:50366/Pedidos.asmx"; //para pruebas con el WS corriendo en localhost
		final String METHOD_NAME = "Test";
		final String SOAP_ACTION = "makersevicios.com.ar/Test";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

		// Soap Header
		String key = "ABC123";

		String base64 = Base64.encodeToString(key.getBytes(), Base64.DEFAULT);

		Element authHeader = new Element().createElement(NAMESPACE, "Auth");

		Element apiKey = new Element().createElement(NAMESPACE, "ApiKey");
		apiKey.addChild(Node.TEXT, base64);

		authHeader.addChild(Node.ELEMENT, apiKey);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);

		envelope.headerOut = new Element[1];
		envelope.headerOut[0] = authHeader;

		// Capa de transporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL, 180000);

		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive resultado = (SoapPrimitive)envelope.getResponse();

			return resultado.toString();
		}
		catch (Exception e)
		{
			return "catch";
		}
	}
}