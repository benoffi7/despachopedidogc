package com.maker.lanuevasolodos;



import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

public class AyudaCalle extends Activity {

	ImageButton button_bcalle;
	Button button_salir, button_volver;
	Adaptador_nombreCalle adaptador;
	ListView listView;
	EditText editText_calle;
	
	// Variables para extras, osea parametros, calleNro es calle 1 o calle 2
	String _calleNro, _calleSeleccionada ,_hCalle,_hAltura ,_hEsquina,_hComentario, _hLocalidad,_pref_telefono, _pref_area, _textoObtenido;
	
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ayuda_calle);
		
		// Exrtas para volver a devolver al MainActivityEsquina
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
			{
				_pref_telefono = getIntent().getExtras().getString("_pref_telefono");
				_pref_area = getIntent().getExtras().getString("_pref_area");
				if (getIntent().getExtras().getString("calleNro") != null) _calleNro = getIntent().getExtras().getString("calleNro");
				if (getIntent().getExtras().getString("calle") != null) _hCalle = getIntent().getExtras().getString("calle");
				if (getIntent().getExtras().getString("altura") != null) _hAltura = getIntent().getExtras().getString("altura");
				if (getIntent().getExtras().getString("esquina") != null) _hEsquina = getIntent().getExtras().getString("esquina");
				if (getIntent().getExtras().getString("comentario") != null) _hComentario = getIntent().getExtras().getString("comentario");
				if (getIntent().getExtras().getString("localidad") != null) _hLocalidad = getIntent().getExtras().getString("localidad");
				if (getIntent().getExtras().getString("prellenado") != null) _textoObtenido = getIntent().getExtras().getString("prellenado");
			}
		}
		
		findViews();
		
		createEvents();
		
		if ( (""+editText_calle.getText()).trim().length() > 0)
		{
			button_bcalle.performClick();
			button_volver.requestFocus();
		}
		
	}



	private void findViews() {
		button_bcalle 	= (ImageButton)findViewById(R.id.imageButton_bcalle);
		button_salir 	= (Button)findViewById(R.id.buttonSalir_Calle);
		listView 		= (ListView)findViewById(R.id.listView1);
		editText_calle 	= (EditText)findViewById(R.id.editText_calleBuscar);
		button_volver 	= (Button)findViewById(R.id.buttonSeleccionarYVolver);
		
		if (_textoObtenido.trim().length() > 0)
		{
			if (_textoObtenido.trim().length() > 3)
				editText_calle.setText(_textoObtenido.trim().substring(0, 3));
			else
				editText_calle.setText(_textoObtenido.trim());
			
			
		}
	}


	@Override
	public void onBackPressed() {
		Intent intento  = new  Intent(AyudaCalle.this, MainActivity.class);;
		intento.putExtra("seleccionCalle", "1");
		if (_hCalle != null )intento.putExtra("calle", _hCalle);
		if (_hEsquina != null) intento.putExtra("esquina", _hEsquina);
		if (_hAltura != null) intento.putExtra("altura",  ""+_hAltura);
		if (_hComentario != null) intento.putExtra("comentario", _hComentario);
		if (_hLocalidad != null) intento.putExtra("localidad", _hLocalidad);
		
	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

	    	startActivity(intento);
		
		finish();
	}
	
	private void createEvents() {
		
		// Boton Buscar
		button_bcalle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (editText_calle.getText().length() < 3)
					return;
			   	obtenerCalles oCalles = new obtenerCalles();
			   	oCalles.execute(editText_calle.getText().toString(), _hLocalidad);
			}
		});
		
		// Bot�n volver
		button_volver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			
				String _pasar = null ;
				
				try
				{
					_pasar = adaptador.oCalle.getDireccion();
				}
				catch (Exception ex)
				{
					return;
				}
				finally
				{
				
				}
				
				if (_pasar != null)
				{
					if (_pasar.equals("Sin similares"))
						return;
					
//					Toast toast;
//					toast = Toast.makeText(AyudaCalle.this,_pasar , Toast.LENGTH_SHORT);
//					toast.setGravity(Gravity.CENTER_VERTICAL, 0, editText_calle.getTop());
//					toast.show();
					
					Intent intento  = new  Intent(AyudaCalle.this, MainActivity.class);;
					intento.putExtra("seleccionCalle", "1");
					
					
					if (_calleNro.equals("1"))
					{
						intento.putExtra("calle", _pasar);
						if (_hEsquina != null) 
							intento.putExtra("esquina", _hEsquina);
					}
					else
					{
						intento.putExtra("esquina", _pasar);
						if (_hCalle != null) 
							intento.putExtra("calle", _hCalle);
					}
					if (_hAltura 		!= null)
						intento.putExtra("altura", ""+_hAltura);
					if (_hComentario 	!= null) 
						intento.putExtra("comentario", _hComentario);
					if (_hLocalidad 	!= null) 
						intento.putExtra("localidad", _hLocalidad);
					
	     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	
	     	    	startActivity(intento);
					
					finish();
				}
			}
		});
		
		// Boton salir
		button_salir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent intento  = new  Intent(AyudaCalle.this, MainActivity.class);;
				intento.putExtra("seleccionCalle", "1");
				if (_hCalle != null )intento.putExtra("calle", _hCalle);
				if (_hEsquina != null) intento.putExtra("esquina", _hEsquina);
				if (_hAltura != null) intento.putExtra("altura",  ""+_hAltura);
				if (_hComentario != null) intento.putExtra("comentario", _hComentario);
				if (_hLocalidad != null) intento.putExtra("localidad", _hLocalidad);
				
     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

     	    	startActivity(intento);
				
				finish();
			}
		});
		
	}

	
	// ------------------------------------------
    // clase para obtener lista de calles
    // ------------------------------------------
    public class obtenerCalles extends AsyncTask<String , Void,  ArrayList>
    
	{   
    	ProgressDialog pd = new ProgressDialog(AyudaCalle.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeCalles));
			pd.setCancelable(false);
			pd.show();
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected ArrayList doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(AyudaCalle.this);
			
			ArrayList  _rta = null;
			
			_rta = cw.Obtener_calles("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(final ArrayList resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

			if (resultado != null)
			{
				//Creo una instancia del adaptador
				//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), this);
				//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), getApplicationContext());
				
				adaptador = new Adaptador_nombreCalle(resultado,AyudaCalle.this);
				
				//Setea el adapatador a la lista
				listView.setAdapter(adaptador);
				 
				Funciones.ocultarTeclado(AyudaCalle.this, listView);
			}
			super.onPostExecute(resultado);
		}

	  }
    
}
