package com.maker.lanuevasolodos;

public class Direccion {

	private int idFavorita;
	private int idLocalidad;
	private String localidad;
	private String calle;
	private int altura;
	private String esquina;
	private String comentario;
	private String direccion;
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	protected int getIdFavorita() {
		return idFavorita;
	}
	protected void setIdFavorita(int idFavorita) {
		this.idFavorita = idFavorita;
	}
	protected int getIdLocalidad() {
		return idLocalidad;
	}
	protected void setIdLocalidad(int idLocalidad) {
		this.idLocalidad = idLocalidad;
	}
	protected String getLocalidad() {
		return localidad;
	}
	protected void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	protected String getCalle() {
		return calle;
	}
	protected void setCalle(String calle) {
		this.calle = calle;
	}
	protected int getAltura() {
		return altura;
	}
	protected void setAltura(int altura) {
		this.altura = altura;
	}
	protected String getEsquina() {
		return esquina;
	}
	protected void setEsquina(String esquina) {
		this.esquina = esquina;
	}
	protected String getComentario() {
		return comentario;
	}
	protected void setComentario(String comentario) {
		this.comentario = comentario;
	}

	
}
