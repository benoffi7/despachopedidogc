package com.maker.lanuevasolodos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.maker.soap.conexionServicioWebSOAP;

public class Estado_servicio extends Activity {

	Button button_Verificar, button_Iniciar, button_Detener, button_Salir;
	TextView textView_estado;
	String _NroTelefono, _Area, _version;
	LinearLayout layoutBotones;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_estado_servicio);
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				// Tiene el campo _codigo? (siempre deber�a ser SI)
				if (getIntent().getExtras().getString("telefono") != null)
				{
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
				}
		}
		
		
       //find views
       finds_views();
        
       
        //crear eventos
       event_create();
       
       //Obtener estado actual y si corresponde mostrar los botones para iniciar, detener
       
       verificarEstadoServicio();
       
	}

	
	// crear vista, encontrar botones
	private void finds_views() {
		
		PackageInfo info ;
		PackageManager manager = this.getPackageManager();
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			
			_version = "" + info.versionName;
		} catch (NameNotFoundException e) {
			Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
			finish();
		}

		
		button_Detener = (Button)findViewById(R.id.button_Detener_estado);
		button_Iniciar = (Button)findViewById(R.id.button_Iniciar_estado);
		button_Verificar = (Button)findViewById(R.id.button_Verificar_estado);
		button_Salir = (Button)findViewById(R.id.button_Salir_estado);
		
		textView_estado =(TextView)findViewById(R.id.textView_estado_servicio);
		button_Verificar.setEnabled(false);
		
		layoutBotones = (LinearLayout)findViewById(R.id.linearLayout2_est);
		layoutBotones.setVisibility(View.GONE);
		
	}
	
	// ------------------------------------------
	// crear eventos para botones
	// ------------------------------------------
	private void event_create() {

		// Boton salir
		button_Salir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		// Bot�n verificar
		button_Verificar.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View arg0)
			{
				
				 verificarEstadoServicio();
				 button_Verificar.setEnabled(false);
			}
		});
		
		// Boton detener
		button_Detener.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				detenerIniciarServicio or = new detenerIniciarServicio();
			     // Paso el par�metor al hilo paralelo
			     or.execute("true");
			     
			}
		});
		
		
		// Boton iniciar
		button_Iniciar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				detenerIniciarServicio or = new detenerIniciarServicio();
			     // Paso el par�metor al hilo paralelo
			     or.execute("false");
			}
		});
		
	}

	// ------------------------------------------
	// Control para ver si tiene un viaje pedido
    // ------------------------------------------
	private void verificarEstadoServicio() {
		obtenerEstadoServicio or = new obtenerEstadoServicio();
	     // Paso el par�metor al hilo paralelo
	     or.execute(_Area, _NroTelefono, _version);

	}
	
	
	 // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class obtenerEstadoServicio extends AsyncTask<String , Void, Respuesta>
	{
    	String _NroTelefono ; 
    	ProgressDialog pd = new ProgressDialog(Estado_servicio.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._ser_verificando));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {


			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(Estado_servicio.this);
			
			
			Respuesta _rta;
			// area, telefono, version
			
			_rta = cw.obtenerEstadoServicio("" + _texto[0], "" + _texto[1], "" + _texto[2]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			


			// Control de versi�n de aplicaci�n
			if (resultado.getVersionIncorrecta())
			{
				if (resultado.getActualizacionObligatoria())
				{
					Intent intento = new  Intent(Estado_servicio.this, Activity_version_obsoleta.class);
				    intento.putExtra("direccion", resultado.getURI());
					startActivity(intento);
					finish();
				}
				else
				{
					Toast tostada = Toast.makeText(Estado_servicio.this, R.string._act_version_antigua, Toast.LENGTH_LONG);
					tostada.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					tostada.show();
	
				}
			}
			
			// Escribir en la pantalla
			textView_estado.setText(resultado.getServicioOffLine() ? getString(R.string._ser_offLine) : getString(R.string._ser_onLine));
	
			// Mostrar o no los botones de detener
			layoutBotones.setVisibility(resultado.getEsAdministrador() ? View.VISIBLE : View.GONE);
			
			super.onPostExecute(resultado);
		}

	  }

    
    // ------------------------------------------
    // Iniciar servicio
    // ------------------------------------------
    public class detenerIniciarServicio extends AsyncTask<String , Void, Boolean>
	{
    	String _pedido ; 
    	ProgressDialog pd = new ProgressDialog(Estado_servicio.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._ser_aguardeCambiandoEstado));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Boolean doInBackground(String... _texto) {


			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(Estado_servicio.this);
			
			
			Boolean _rta;
			// la sig variable se usa para saber si estoy deteniendo o iniciando, en el post para el toast
			_pedido = "" + _texto[0];
			
			_rta = cw.detenerIniciarServicio("" + _texto[0]);
			
			return _rta;
		}
	
		@Override
		protected void onPostExecute(Boolean resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			// 
			if (resultado)
			{
				// Escribir en la pantalla
				textView_estado.setText( _pedido.equalsIgnoreCase("true") ? getString(R.string._ser_offLine) : getString(R.string._ser_onLine));

				Toast.makeText(Estado_servicio.this, _pedido.equalsIgnoreCase("true") ? Estado_servicio.this.getString(R.string._ser_detenidoOk) : Estado_servicio.this.getString(R.string._ser_iniciadoOk) , Toast.LENGTH_SHORT).show();
			}
			else
				Toast.makeText(Estado_servicio.this,Estado_servicio.this.getString(R.string._ser_errorAlIniciarDetener), Toast.LENGTH_SHORT).show();
			
			button_Verificar.setEnabled(true);
			
			super.onPostExecute(resultado);
		}

	  }
    

}
