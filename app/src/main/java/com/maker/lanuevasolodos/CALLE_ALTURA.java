package com.maker.lanuevasolodos;


public class CALLE_ALTURA {
	private String calle;
	private String altura;
	private String desde;
	private String demora;
	private String localidad;
	private String idlocalidad;
	private boolean hayError;
	private String zona;
	private boolean versionIncorrecta;
	private boolean actualizacionObligatoria;
	private String URI;
	private boolean servicioOffLine;
	private boolean errorConexion;
	private boolean calleNoEncontrada;
	
	public boolean isServicioOffLine() {
		return servicioOffLine;
	}

	public void setServicioOffLine(boolean servicioOffLine) {
		this.servicioOffLine = servicioOffLine;
	}

	public boolean isVersionIncorrecta() {
		return versionIncorrecta;
	}

	public void setVersionIncorrecta(boolean versionIncorrecta) {
		this.versionIncorrecta = versionIncorrecta;
	}

	public boolean isActualizacionObligatoria() {
		return actualizacionObligatoria;
	}

	public void setActualizacionObligatoria(boolean actualizacionObligatoria) {
		this.actualizacionObligatoria = actualizacionObligatoria;
	}

	public String getURI() {
		return URI;
	}

	public void setURI(String uRI) {
		URI = uRI;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public boolean isHayError() {
		return hayError;
	}

	public void setHayError(boolean hayError) {
		this.hayError = hayError;
	}

	public String getIdlocalidad() {
		return idlocalidad;
	}

	public void setIdlocalidad(String idlocalidad) {
		this.idlocalidad = idlocalidad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getDemora() {
		return demora;
	}
	
	public void setDemora(String demora) {
		this.demora = demora;
	}
	
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	private String hasta;
	
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}

	public boolean isErrorConexion() {
		return errorConexion;
	}

	public void setErrorConexion(boolean errorConexion) {
		this.errorConexion = errorConexion;
	}

	public boolean isCalleNoEncontrada() {
		return calleNoEncontrada;
	}

	public void setCalleNoEncontrada(boolean calleNoEncontrada) {
		this.calleNoEncontrada = calleNoEncontrada;
	}
}
