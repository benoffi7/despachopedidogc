package com.maker.lanuevasolodos;

import java.util.ArrayList;

import com.maker.lanuevasolodos.BaseDeDatos.CALLE;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class Autocompletar extends ArrayAdapter<CALLE> implements Filterable
{
	private BaseDeDatos bd ;
	
	private LayoutInflater mInflater;

	String _xLocalidad;
	
	public Autocompletar(final Context contexto, String localidad)
	{
		super(contexto,-1);
		mInflater = LayoutInflater.from(contexto);
       // Crear objeto base de datos
		
   		bd = new BaseDeDatos(contexto.getApplicationContext(), contexto.getResources().getString(R.string.DB_nombre) , 
			null , Integer.parseInt(contexto.getResources().getString(R.string.DB_version)));
   		
   		_xLocalidad = localidad;
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent)
	{
		final TextView tv;
		if (convertView != null)
		{
			tv = (TextView) convertView;
		}
		else
		{
			tv = (TextView) mInflater.inflate(R.layout.renglon_autocompletar, parent, false);
		}
		
		tv.setText(getItem(position).getNombre());
		tv.setTag(getItem(position).getLocalidad());
		
		return tv;
	}
	
		
	@Override
	public Filter getFilter()
	{
		Filter myFilter = new Filter()
		{
			@Override
			protected FilterResults performFiltering(final CharSequence LoQueIngresaElUsuario)
			{
				ArrayList <CALLE> addressList = null;
				if ((LoQueIngresaElUsuario != null) && (LoQueIngresaElUsuario.length()>=3))
				{
					addressList = bd.obtenerCallesCombo(LoQueIngresaElUsuario.toString(), _xLocalidad);
				}
				if (addressList == null)
				{
					addressList = new ArrayList<CALLE>();
				}
				
				final FilterResults filterResults = new FilterResults();
				filterResults.values = addressList;
				filterResults.count = addressList.size();
				
				return filterResults;
			}

			
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(final CharSequence contraint, final FilterResults results)
			{
				clear();
				for (CALLE calles : (ArrayList<CALLE>) results.values)
				{
					add(calles);
				}
				// Si encontro al menos uno entonces publicalo, mostralo en pantlla
				if (results.count > 0)
				{
					notifyDataSetChanged();
				}
				else
				{
					notifyDataSetInvalidated();
				}
			}
			
			@Override
			public CharSequence convertResultToString(final Object objetoResultado)
			{
				//return objetoResultado == null ? "" : ((Calle)objetoResultado).getNombre();
				return  objetoResultado == null ? "" : objetoResultado.toString();
			}
		};
	return myFilter;
}

	



}
