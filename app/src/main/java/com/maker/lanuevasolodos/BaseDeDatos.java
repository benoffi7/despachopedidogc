package com.maker.lanuevasolodos;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseDeDatos extends SQLiteOpenHelper{

	
	SQLiteDatabase dbName;
	String _insertDatos;
	Context contexto;
	
	//----------------------
	// Constructor
	//----------------------
	public BaseDeDatos(Context _contexto, String _dbName, CursorFactory _cursorFactory, int _version) {
	
		super(_contexto, _dbName , _cursorFactory, _version);

		contexto = _contexto;
	}

	
	//----------------------
	// S�lo se ejcuta si no encuentra la base que se pas� en el constructor
	//----------------------
	@Override
	public void onCreate(SQLiteDatabase _db) {
		_db.execSQL(contexto.getString(R.string.DB_crear_tabla_direcciones));
		_db.execSQL(contexto.getString(R.string.DB_crear_tabla_calles));
	}

	
	//----------------------
	// S�lo se ejcuta si la version pasada es <> a la versi�n actual
	//----------------------
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		
	}
	
	
	//----------------------
	// Insert del mensaje
	//----------------------
	public void Guardar_calle(String _calle, String _localidad  ){
	
		if (!existeCalle(_calle, _localidad))
		{
			// Abro la base para leer o escribir
			dbName = getWritableDatabase();
			String sql = generarSentenciaInsertCalle( _calle, _localidad );
					
					
			dbName.execSQL(sql);
			
			// Cierro la base
			dbName.close();
		}


	}
	
	
	private boolean existeCalle(String _calle, String _localidad) {
		boolean _rta = false;
		// Abro la base para leer o escribir
		dbName = getWritableDatabase();

	//	Cursor cursor = dbName.rawQuery(contexto.getResources().getString(R.string.sql_direcciones), null);
		Cursor cursor = dbName.rawQuery(generarCalleExiste(_calle, _localidad), null);

		if (cursor.getCount()> 0){
			_rta = true;
		}
		
		// Cierro la base
		dbName.close();
		return _rta;
	}

    // --------------------------------
	// Guardar direcci�n como favorita
	// --------------------------------
	public void Guardar_direccion(String _calle, String _esquina, int _altura, String _localidad, String _comentario, String _direccion) {

			if (!existeDireccion(_calle, _esquina, _altura, _localidad))
			{
	
				// Abro la base para leer o escribir
				dbName = getWritableDatabase();
				
				String sql = generarSentenciaInsertDireccion( _calle, _esquina, _altura,  _localidad, _comentario, _direccion );
						
						
				dbName.execSQL(sql);
				
				// Cierro la base
				dbName.close();
				
			}
		//	Log.d("valor", "Exception:" + ex.getMessage());
	}




	// --------------------------------
	// Exite direcion
	// --------------------------------
    private boolean existeDireccion(String _calle, String _esquina, int _altura, String _localidad) {
    	boolean _rta = false;
		// Abro la base para leer o escribir
		dbName = getWritableDatabase();

		Cursor cursor = dbName.rawQuery(generarDireccionExiste(_calle, _esquina, _altura, _localidad), null);

		if (cursor.getCount()> 0){
			_rta = true;
		}
		
		// Cierro la base
		dbName.close();
		return _rta;
	}




	// --------------------------------
	// Obtener favoritas
	// --------------------------------
	public ArrayList<Direccion> Obtener_Direcciones(){
		// Objeto mensaje que agrego al arrayList
		Direccion _direccion;
		
		// Array para retornar
		ArrayList<Direccion> _rta = new ArrayList<Direccion>();
		
		
		// Abro la base para leer o escribir
		dbName = getWritableDatabase();

	//	Cursor cursor = dbName.rawQuery(contexto.getResources().getString(R.string.sql_direcciones), null);
		Cursor cursor = dbName.rawQuery(generarScriptObtenerDirecciones(), null);
		

		while (cursor.moveToNext()){
			_direccion = new Direccion();
			_direccion.setIdFavorita(cursor.getInt(cursor.getColumnIndex("ID")));
			//_direccion.setIdLocalidad(cursor.getInt(cursor.getColumnIndex("ID_LOCALIDAD")));
			_direccion.setCalle(cursor.getString(cursor.getColumnIndex("CALLE")));
			_direccion.setAltura(cursor.getInt(cursor.getColumnIndex("ALTURA")));
			_direccion.setEsquina(cursor.getString(cursor.getColumnIndex("ESQUINA")));
			_direccion.setComentario(cursor.getString(cursor.getColumnIndex("COMENTARIO")));
			_direccion.setLocalidad(cursor.getString(cursor.getColumnIndex("LOCALIDAD")));
			_direccion.setDireccion(cursor.getString(cursor.getColumnIndex("DIRECCION")));
			
			_rta.add(_direccion);
		}
		
		// Cierro la base
		dbName.close();

		// Retorno el ArrayList
		return _rta;
	}
	


	// -----------------------------
	// Obtener calles para el autocompletar
	//------------------------------
	public ArrayList<CALLE> obtenerCallesCombo(String _parametro, String _localidad){
		// Objeto calle que agrego al arrayList
		CALLE _calleTemp;
		
		// Array para retornar
		ArrayList<CALLE> _rta = new ArrayList<CALLE>();
		
		
		// Abro la base para leer o escribir
		dbName = getWritableDatabase();

		Cursor cursor = dbName.rawQuery(generarSentenciaBuscarCalle(_parametro, _localidad), null);

		while (cursor.moveToNext()){
			_calleTemp = new CALLE();
			_calleTemp.setLocalidad(cursor.getString(cursor.getColumnIndex("LOCALIDAD")));
			_calleTemp.setNombre(cursor.getString(cursor.getColumnIndex("CALLE")));

			_rta.add(_calleTemp);
		}

		// Cierro la base
		dbName.close();

		// Retorno el ArrayList
		return _rta;
	}
	
	
	// -----------------------------
	// Elimianr una direccion de favorita
	//------------------------------

	public void Eliminar_direccion(int idFavorita) {
		// Abro la base para leer o escribir
		dbName = getWritableDatabase();

		dbName.execSQL(generarSentenciaDelete(idFavorita));
		
		// Cierro la base
		dbName.close();
		
	}
	
	
	
	// -----------------------------
	// Generar script para eliminar una direccion
	//------------------------------
	private String generarSentenciaDelete(int idFavorita) {
		String _rta = "";
		_rta = contexto.getString(R.string.DB_eliminar_direccion);
		
		// 
		_rta = _rta.replace("martinId", "" + idFavorita);

		
		return _rta;
	}


	// -----------------------------
	// Generar script para obtener direcciones
	//------------------------------
	private String generarScriptObtenerDirecciones() {
		String _rta;
		_rta = contexto.getString(R.string.DB_Obtener_direcciones);
		

		return _rta;
	}

	
	// ----------------------
	// Sentencia a ver si existe una calle para una localidad, para luego hacer el insert o no
	// ----------------------
	private String generarCalleExiste(String _calle, String _localidad) {
		String _rta = "";
		_rta = contexto.getString(R.string.DB_existe_calle_idlocalidad);
		
		// 
		_rta = _rta.replace("martinLocalidad", "" + _localidad);
		_rta = _rta.replace("martinCalle", _calle);

		
		return _rta;
	}

	// --------------------------------
	// Generar el script para ver si existe la direccion
	// --------------------------------
	private String generarDireccionExiste(String _calle, String _esquina, int _altura, String _localidad) {
		String _rta;
		_rta = contexto.getString(R.string.DB_existe_direccion);
		
		// 
		_rta = _rta.replace("martinLocalidad", "" + _localidad);
		_rta = _rta.replace("martinCalle", _calle);
		_rta = _rta.replace("martinEsquina", (_esquina.equalsIgnoreCase("maker") ? "" : _esquina));
		_rta = _rta.replace("martinAltura", "" +_altura);
		
		return _rta	;
	}
	

	
	// ----------------------
	// Sentencia Insert
	// ----------------------
	private String generarSentenciaInsertCalle(String _calle, String _localidad) {
		String _rta = "";
		_rta = contexto.getString(R.string.DB_crear_insert_calles);
		
		// 
		_rta = _rta.replace("martinsql", "'" + _calle + "', '" + _localidad + "'");
		return _rta;
	}
	
	// ----------------------
	// Sentencia Insert de Direcciones
	// ----------------------
	 private String generarSentenciaInsertDireccion(String _calle,String _esquina, int _altura, String _localidad,
			 										String _comentario, String _direccion) 
	{
		String _rta = "";
		_rta = contexto.getString(R.string.DB_crear_insert_direcciones);
		
		//control que no hay segunda calle, en ese caso dice maker como nombre de segunda calle
		if (_esquina.equalsIgnoreCase("maker") )
			_esquina = "";
		
		//  ID_LOCALIDAD, LOCALIDAD,  CALLE, ALTURA, ESQUINA, COMENTARIO, DIRECCION, cantidad
		_rta = _rta.replace("martinsql", " '" + _localidad + "', '"  + _calle + "', " + _altura + ", '" + _esquina + "', '" + _comentario + "', '" + _direccion +"', 0" );
	
		return _rta;
	}
	 
	

	// ----------------------
	// generar sentencia para buscar una calle
	// ----------------------
	private String generarSentenciaBuscarCalle(String _parametro, String localidad) {
		String _rta= ""; 
		_rta = contexto.getResources().getString(R.string.DB_Obtener_calles_combos);
		
		_rta = _rta.replace("martinCalle", _parametro);
		_rta = _rta.replace("martinLocalidad", localidad);
		return _rta;
	}


	
	// ------------------------------------------
	// Clase CALLE
	// ------------------------------------------
	public class CALLE
	{
		
		private String localidad;
		

		public String getLocalidad() {
			return localidad;
		}
		public void setLocalidad(String localidad) {
			this.localidad = localidad;
		}

		private String nombre;
		
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
	
		@Override
		public String toString() {
			return this.nombre.toString();
		}
	}


}
