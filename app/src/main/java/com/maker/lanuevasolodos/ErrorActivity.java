package com.maker.lanuevasolodos;



import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class ErrorActivity extends Activity {

	TextView textView_direccion;
	String _texto_entrada;
	Button button_Volver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_error);
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				// Tiene el campo _codigo? (siempre deber�a ser SI)
				if (getIntent().getExtras().getString("mensaje") != null)
				{
					_texto_entrada = getIntent().getExtras().getString("mensaje");
				}
		}
		
    	// Find views de los componentes a usar
		findViews();

		// Completar el texto en pantalla
		completeText();
		
		// crear eventos
		event_create();
	}

	// ------------------------------------------
	// Realizar todos los findViews de los componentes a usar
    // ------------------------------------------
	private void findViews() {
			// botones
			textView_direccion = (TextView)findViewById(R.id.er_textview_direccion);
			button_Volver = (Button)findViewById(R.id.err_button_Volver);
	}

	// ------------------------------------------
	// Completar el texto que llego a la pantalla
    // ------------------------------------------
	private void completeText() {
			// botones
			textView_direccion.setText(_texto_entrada.replace("\\n", Html.fromHtml("<br/>")));
	}

    // ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void event_create() {
		
		// Bot�n volver
		button_Volver.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View vista) {
				finish();
			}
		});
	}

    
}
