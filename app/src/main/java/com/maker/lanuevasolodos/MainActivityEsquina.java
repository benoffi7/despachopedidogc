package com.maker.lanuevasolodos;


import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;



public class MainActivityEsquina extends Activity {

    // ------------------------------------------
	// Declaraci�n de variables
    // ------------------------------------------
	Button button_solicitar, button_favoritos;
	EditText editText_localidad, editText_calle, editText_esquina2, editText_esquina;
	LinearLayout layoutCalle, layoutEsquina2, layoutEsquina, layoutLocalidad;
	String _Localidad;
	ImageButton button_Teclado_Calle, button_Teclado_Esquina, button_Teclado_Esquina2;
	String _version = "1";
    Spinner sp; 
    Boolean _VerificarViaje;

    
	// localidades
	private String[] localidades = {"LOMAS DE ZAMORA", "LANUS"};
    //private String[] localidades = {"LA PLATA"};
    
	  
    // ------------------------------------------
	// Declaraci�n de variables de recepci�n de habituales
    // ------------------------------------------
	String _hCalle, _hEsquina2, _hEsquina, _hLocalidad, _hIdLocalidad;
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	// ------------------------------------------
	// Declaraci�n de variables de preferencias
    // ------------------------------------------
	String _pref_telefono, _pref_habilitado, _pref_area;
 //	SharedPreferences preferencias;
 	
	//Switch switch_favorita;
	
	
    // ------------------------------------------
	// OnCreate de la aplicaci�n
    // ------------------------------------------
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_esquina);

        _VerificarViaje = true;
    	if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
			{
				if (getIntent().getExtras().getString("telefono") != null)
				{
					_pref_telefono = getIntent().getExtras().getString("telefono");
					_pref_area = getIntent().getExtras().getString("area");

				}
					
				
//				// Si tiene calle, altura esquina comentario, es porque viene de habituales
				if (getIntent().getExtras().getString("calle") != null)
				{
					// poner los datos en variables que lueg ose pasaran a los views (componenentes) una vez
					// que los haya encontrado con el findviews en la llamada siguiente (finds_views())
					_hCalle = getIntent().getExtras().getString("calle");
					_hEsquina2 = getIntent().getExtras().getString("esquina2");
					_hEsquina =  getIntent().getExtras().getString("esquina");
					_hLocalidad = getIntent().getExtras().getString("localidad");
				}
				
				// si viene de AyudaCalle completar la calle
				if (getIntent().getExtras().getString("seleccionCalle") != null)
				{
					_VerificarViaje = false;
					if (getIntent().getExtras().getString("calle")      != null) _hCalle      = getIntent().getExtras().getString("calle");
					if (getIntent().getExtras().getString("esquina2")     != null) _hEsquina2     = getIntent().getExtras().getString("esquina2");
					if (getIntent().getExtras().getString("esquina")    != null) _hEsquina    = getIntent().getExtras().getString("esquina");
					if (getIntent().getExtras().getString("localidad")  != null) _hLocalidad  = getIntent().getExtras().getString("localidad");
				}
			}
		}
    	
    	// Si tiene viaje creado entonces ir directamente a la pantalla de con viaje
    	//verificarSiTieneViajePedido();
    	
        //find views
       finds_views();
        
       
        //crear eventos
       event_create();
       
       // Crear objeto base de datos
   		bdd = new BaseDeDatos(getApplicationContext(), getResources().getString(R.string.DB_nombre) , 
			null , Integer.parseInt(getResources().getString(R.string.DB_version)));
       
   		// S�lo no verifica en el caso de que vuelva de la selecci�n de calle
   		if (_VerificarViaje)
   			verificarSiTieneViajePedido();
   		
   			
       // Poner foco en calle o localidad, depende de las  preferencias
   		editText_calle.requestFocus();

		Mostrar_mensaje_calle_inter();

    }

	private void Mostrar_mensaje_calle_inter() {
		SharedPreferences preferencias;
		preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		String mostrar = preferencias.getString("mostrarAviso", "1");

		if (mostrar.equals("1") || mostrar.length() < 1)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityEsquina.this);

			builder.setMessage("Recuerde siempre ingresar una intersección")
					.setTitle("Importante !")
					.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							dialog.cancel();
						}})
					.setNegativeButton("No volver a mostrar", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							guardarNoMostrarMensaje();
							dialog.cancel();
						}

						private void guardarNoMostrarMensaje() {
							try
							{
								SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);


								// crear el editor de preferencias
								SharedPreferences.Editor editor = preferencias.edit();

								// Agregar como preferencia el telefono
								editor.putString("mostrarAviso", "0");

                                editor.apply();
							}
							catch (Exception ex)
							{

							}
							return;

						}
					});
			builder.create();
			builder.show();
		}

	}


	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		Intent intento;
		
		switch (id) {
		case R.id.action_ayuda:
			intento = new Intent(getApplicationContext(), Activity_help.class);
			startActivity(intento);
			break;
		case R.id.action_estado:
			intento = new Intent(getApplicationContext(), Estado_servicio.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area );
			startActivity(intento);
			break;
		case R.id.action_contacto:
			intento = new Intent(getApplicationContext(), ContactoActivity.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area);
			startActivity(intento);
			break;
		case R.id.action_acercade:
			intento = new Intent(getApplicationContext(), AcercaDeActivity.class);
			startActivity(intento);
			break;
		default:
			return true;
		}
	
		return super.onOptionsItemSelected(item);
	}
	
	// ------------------------------------------
	// Control para ver si tiene un viaje pedido
    // ------------------------------------------
	private void verificarSiTieneViajePedido() {
		 tieneUnViajeReciente or = new tieneUnViajeReciente();
	     // Paso el par�metor al hilo paralelo
	     or.execute(_pref_area, _pref_telefono);

	}


	// ------------------------------------------
	// Realizar todos los findViews de los componentes a usar
    // ------------------------------------------
	private void finds_views() {
		
		PackageInfo info ;
		PackageManager manager = this.getPackageManager();
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			
			_version = "" + info.versionCode;
		} catch (NameNotFoundException e) {
			Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
			finish();
		}


        sp = (Spinner)findViewById(R.id.spinner1);
        sp.setBackgroundColor(getResources().getColor(R.color.gris_campos));

        ArrayAdapter<String> adapter_state  = new ArrayAdapter<String>(this, R.layout.layout_spinner, localidades);
        //ArrayAdapter<String> adapter_state  = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, localidades);
        sp.setAdapter(adapter_state);
        
        if (sp.getCount() < 2)
        	sp.setEnabled(false);
        
		// botones
		button_solicitar = (Button)findViewById(R.id.button_confirmar);
		button_favoritos = (Button)findViewById(R.id.button_favorito);
		button_Teclado_Esquina = (ImageButton)findViewById(R.id.ImageButton_esquina);
		button_Teclado_Esquina2 = (ImageButton)findViewById(R.id.imageButton_esquina2);
		button_Teclado_Calle = (ImageButton)findViewById(R.id.imageButton_calle);
		
		// edit texts
	//	editText_localidad = (EditText)findViewById(R.id.editText_localidad);
	    editText_calle = (EditText)findViewById(R.id.EditText_calle);
	    editText_esquina = (EditText)findViewById(R.id.EditText_esquina);
	    editText_esquina2 = (EditText)findViewById(R.id.EditText_esquina2);

	    // checkbox

	    // Layout
	    layoutCalle = (LinearLayout)findViewById(R.id.linearLayoutCalle);
	    layoutEsquina2 = (LinearLayout)findViewById(R.id.linearLayoutEsquina2);
	    layoutEsquina = (LinearLayout)findViewById(R.id.linearLayoutEsquina);
	    layoutLocalidad = (LinearLayout)findViewById(R.id.linearLayoutLocalidad);
	 
	    
	    if (_hCalle != null) 
	    	editText_calle.setText(_hCalle);

	    if (_hEsquina2 != null)    
	    	editText_esquina2.setText(_hEsquina2);
	    
	    if (_hEsquina != null)    
	    	editText_esquina.setText(_hEsquina);

	    
	    if (_hLocalidad != null)
		    if (Boolean.parseBoolean(getString(R.string.utilizaLocalidad)))
		    	sp.setSelection(((ArrayAdapter<String>)sp.getAdapter()).getPosition(_hLocalidad));
		

	    
	    // Control para ver si muestra el bot�n calle/esqunia
	    button_Teclado_Calle.setVisibility(View.VISIBLE);
	    button_Teclado_Esquina.setVisibility(View.INVISIBLE);
	    button_Teclado_Esquina2.setVisibility(View.INVISIBLE);

	    editText_calle.setInputType(Boolean.parseBoolean(getResources().getString(R.string.campoCalleEsquina_es_Texto)) ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
	    editText_esquina.setInputType(Boolean.parseBoolean(getResources().getString(R.string.campoCalleEsquina_es_Texto)) ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
	    editText_esquina2.setInputType(Boolean.parseBoolean(getResources().getString(R.string.campoCalleEsquina_es_Texto)) ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);

	    //(ImageButton)findViewById(R.id.ImageButton_helpCalle);
	    if ( Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
	    {
	    	button_Teclado_Calle.setImageResource( R.drawable.help );
	    	button_Teclado_Esquina.setImageResource( R.drawable.help);
	    	button_Teclado_Esquina2.setImageResource( R.drawable.help);
	    }
	    else
	    {
	    	button_Teclado_Calle.setImageResource(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
	    	button_Teclado_Esquina.setImageResource(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
	    	button_Teclado_Esquina2.setImageResource(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
	    }
	}

	
    // ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void event_create() {
			
		
		// Bot�n solicitar
		button_solicitar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				//1ro ocultar teclado
				Funciones.ocultarTeclado(getApplicationContext(), v);
				
				
				// Control de textos, si hay algo escrito en calle
				if (editText_calle.getText().length() <=0)
				{
					Toast toast;
					toast = Toast.makeText(MainActivityEsquina.this, R.string._err_sin_calle , Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, editText_calle.getTop());
					toast.show();
					return;
				}
				
				// Control de textos, si hay algo escrito en altura o esquina
				if ( editText_esquina.getText().length() <= 0)
				{
					Toast toast;
					toast = Toast.makeText(MainActivityEsquina.this, R.string._err_sin_nro_o_inter , Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, editText_calle.getTop());
					toast.show();
					return;
				}



				  if (!Funciones.hayInternet(getApplicationContext()))
			    	 {
				    	 AlertDialog ad = new AlertDialog.Builder(MainActivityEsquina.this).create();
				    	 ad.setTitle(R.string._error);
				    	 ad.setCancelable(false);
				    	 
		//		    	 ad.setIcon(R.drawable.icono);
				    	 ad.setMessage(getResources().getString( R.string._err_sin_internet));
				    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								
							}
						});
				    	 
				    	 ad.show();
			    	 }


				    	 obtenerResultado or = new obtenerResultado();
					     // Paso el par�metor al hilo paralelo
					     or.execute(_pref_area,  _pref_telefono, generar_texto_a_enviar(), _version, ""+ sp.getSelectedItem());

	
			}



		    
			// Genera el texto a enviar al webservice
			private String generar_texto_a_enviar() {
				String _rta = "", conector = " y ";
				
				if ( editText_esquina.getText().length() > 0 && editText_esquina2.getText().length() > 0 )
					 conector = " e/ ";
					
				// escribo el  nombre de la calle
				_rta = editText_calle.getText().toString().trim() ;
				if (editText_esquina.getText().length() > 0)
					_rta += conector + editText_esquina.getText().toString().trim();
				
				if (editText_esquina2.getText().length() > 0)
					_rta +=  " y " + editText_esquina2.getText().toString().trim();
				

				return _rta;
			}
		});

		
		// Bot�n favoritos, por ahora solo muestra el nro de tel�fono 
		button_favoritos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intento = new Intent(getApplicationContext(), Activity_favoritas.class);
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area);
				startActivity(intento);
			}
		});

		
		// Bot�n calle
		button_Teclado_Calle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (! Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
				{
					editText_calle.setInputType(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
					button_Teclado_Calle.setImageResource(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
				}
				else
				{ // Aca se llama al encontrar nombre de calle
					Intent intento  = new  Intent(MainActivityEsquina.this, AyudaCalle.class);;
					intento.putExtra("telefono", _pref_telefono);
					intento.putExtra("area", _pref_area);
					intento.putExtra("calleNro", "1");
					intento.putExtra("calle", editText_calle.getText().toString());
					intento.putExtra("esquina2", editText_esquina2.getText().toString());
	     	    	intento.putExtra("esquina", editText_esquina.getText().toString());
					intento.putExtra("localidad", ""+sp.getSelectedItem());

	     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	     	    	
					startActivity(intento);
					finish();

				}
			}
		});
		
		
		// Bot�n esquina
		button_Teclado_Esquina.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (! Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
				{
					editText_esquina.setInputType(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
					button_Teclado_Esquina.setImageResource(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
				}
				else
				{
					Intent intento  = new  Intent(MainActivityEsquina.this, AyudaCalle.class);;
					intento.putExtra("telefono", _pref_telefono);
					intento.putExtra("area", _pref_area);
					intento.putExtra("calleNro", "2");
					intento.putExtra("calle", editText_calle.getText().toString());
					intento.putExtra("esquina2", editText_esquina2.getText().toString());
	     	    	intento.putExtra("esquina", editText_esquina.getText().toString());
					intento.putExtra("localidad", ""+sp.getSelectedItem());

	     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	     	    	
					startActivity(intento);
					finish();
				}
			}
		});
	
	
		
		// Autocompletar de calle ----------------------
	    AutoCompleteTextView autoComplete_edit = (AutoCompleteTextView)findViewById(R.id.EditText_calle);
	    Autocompletar adapter = new Autocompletar(getApplicationContext(), sp.getSelectedItem().toString());
	        
        autoComplete_edit.setAdapter(adapter);
        autoComplete_edit.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
        {
//	        	Log.d("Autocomplete", "Seleccione algo: "+arg1.getTag().toString());
//	        	bandera = true;

        	editText_esquina.requestFocus();
        }
        });
        
        autoComplete_edit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
//					bandera = false;
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
        
        
		// Autocompletar de esquina ----------------------
	    AutoCompleteTextView autoComplete_Esquina = (AutoCompleteTextView)findViewById(R.id.EditText_esquina2);
	    Autocompletar adapterEsquina = new Autocompletar(getApplicationContext(), sp.getSelectedItem().toString());
	        
	    autoComplete_Esquina.setAdapter(adapterEsquina);
	    autoComplete_Esquina.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
        {
//	        	Log.d("Autocomplete", "Seleccione algo: "+arg1.getTag().toString());
//	        	bandera = true;
        	//Toast.makeText(getApplicationContext(), ((TextView)arg1).getText() , Toast.LENGTH_SHORT).show();
        	editText_esquina2.requestFocus();
        }
        });
        
        autoComplete_edit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
        
		// Autocompletar de esquina2 ----------------------
	    AutoCompleteTextView autoComplete_Esquina2 = (AutoCompleteTextView)findViewById(R.id.EditText_esquina2);
	    Autocompletar adapterEsquina2 = new Autocompletar(getApplicationContext(), sp.getSelectedItem().toString());
	        
	    autoComplete_Esquina2.setAdapter(adapterEsquina);
	    autoComplete_Esquina2.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
        {
//	        	Log.d("Autocomplete", "Seleccione algo: "+arg1.getTag().toString());
//	        	bandera = true;
        	//Toast.makeText(getApplicationContext(), ((TextView)arg1).getText() , Toast.LENGTH_SHORT).show();
        }
        });
        
        autoComplete_edit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
        
        // Al entrar a calle, altura, esquina o mensaje cambiar el color de fondo, poner a todos color fondo y al que entr� colo gris
        editText_calle.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutEsquina.setBackgroundResource(R.color.fondo_oscuro);
					layoutCalle.setBackgroundResource(R.color.gris_campos);
					layoutEsquina.setBackgroundResource(R.color.fondo_oscuro);
					layoutLocalidad.setBackgroundResource(R.color.fondo_oscuro);
					
					button_Teclado_Esquina.setBackgroundResource(R.color.gris_campos);
					button_Teclado_Esquina2.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Calle.setBackgroundResource(R.color.gris_campos);
				    button_Teclado_Esquina.setVisibility(View.INVISIBLE);
				    button_Teclado_Esquina2.setVisibility(View.INVISIBLE);
				    button_Teclado_Calle.setVisibility(View.VISIBLE);

				}
			}
		});
        
        editText_esquina2.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutEsquina2.setBackgroundResource(R.color.gris_campos);
					layoutCalle.setBackgroundResource(R.color.fondo_oscuro);
					layoutEsquina.setBackgroundResource(R.color.fondo_oscuro);
					layoutLocalidad.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Esquina.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Calle.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Esquina2.setBackgroundResource(R.color.gris_campos);
				    button_Teclado_Calle.setVisibility(View.INVISIBLE);
				    button_Teclado_Esquina.setVisibility(View.INVISIBLE);
				    button_Teclado_Esquina2.setVisibility(View.VISIBLE);
				}
			}
		});
        

        
        editText_esquina.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutEsquina2.setBackgroundResource(R.color.fondo_oscuro);
					layoutCalle.setBackgroundResource(R.color.fondo_oscuro);
					layoutEsquina.setBackgroundResource(R.color.gris_campos);
					layoutLocalidad.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Esquina.setBackgroundResource(R.color.gris_campos);
					button_Teclado_Calle.setBackgroundResource(R.color.fondo_oscuro);
					button_Teclado_Esquina2.setBackgroundResource(R.color.fondo_oscuro);
				    button_Teclado_Calle.setVisibility(View.INVISIBLE);
				    button_Teclado_Esquina.setVisibility(View.VISIBLE);
				    button_Teclado_Esquina2.setVisibility(View.INVISIBLE);
				}
			}
		});

        
        
	}
	
	
	
	// Otros Clases -----------------------------------------------------------------------------------------------------------------------------
	
    // ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class obtenerResultado extends AsyncTask<String , Void, Respuesta>
    
	{   
    	ProgressDialog pd = new ProgressDialog(MainActivityEsquina.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeSolitud));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

		
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivityEsquina.this);
			
			// Los favoritos se guardan en Confirmacion JAVA

				// Texto 0 es codigo de area: 223  
				// texto 1 es lo que eescribo: chile 1918, 
				// text 2 es el telefono: 5821315, 
				// texto 3 es la version
				// texto 4 localidad
			Respuesta _rta;
			_rta = cw.call_GuardarMsg_obtRespuesta_esquina("" + _texto[0], "" + _texto[1], "" + _texto[2], "" + _texto[3], "" + _texto[4]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			// Control de versi�n de aplicaci�n
			if (resultado.getVersionIncorrecta())
			{
				if (resultado.getActualizacionObligatoria())
				{
					Intent intento = new  Intent(MainActivityEsquina.this, Activity_version_obsoleta.class);
				    intento.putExtra("direccion", resultado.getURI());
					startActivity(intento);
					finish();
				}
				else
				{
					Toast tostada = Toast.makeText(MainActivityEsquina.this, R.string._act_version_antigua, Toast.LENGTH_LONG);
					tostada.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					tostada.show();
	
				}
			}
			
			if (resultado.getServicioOffLine())
			{
				Intent intento = new  Intent(MainActivityEsquina.this, Estado_servicio.class);
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area);
				startActivity(intento);
				return;
			}
			
			
			
			if (resultado.getPedirConfirmacion())  
			{
			
				Intent intento = new  Intent(MainActivityEsquina.this, Confirmacion.class);
				
				intento.putExtra("direccion", resultado.getTexto().toString());
			    intento.putExtra("calle1", resultado.getCalle1().toString());
     	    	intento.putExtra("calle2", resultado.getCalle2().toString());
				intento.putExtra("localidad", ""+sp.getSelectedItem());
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area);
     	    	intento.putExtra("esquina2", editText_esquina2.getText().toString());
     	    	intento.putExtra("textoAliasMovil", resultado.getDireccion());
     	    	intento.putExtra("abrLocalidad", resultado.getAbrLocalidad());
     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
     	    	
     	    	
				startActivity(intento);
				finish();
			}
			else
			{
				if (resultado.getDireccionInvalida()
						||
					resultado.getCallesAlternativas())
				{
					Intent intento = new  Intent(MainActivityEsquina.this,ErrorActivity.class);
					intento.putExtra("mensaje", resultado.getTexto().toString());
					startActivity(intento);
				}
				else
				{
					if (resultado.getViajeReciente())
					{
						Intent intento = new  Intent(MainActivityEsquina.this,ConViajeActivity.class);
						intento.putExtra("mensaje", resultado.getTexto().toString());
						intento.putExtra("telefono", _pref_telefono);
						intento.putExtra("area", _pref_area);
						intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(intento);
						finish();
					}
					else
					{
						if (resultado.getTexto() == null)
						{
							 AlertDialog ad = new AlertDialog.Builder(MainActivityEsquina.this).create();
					    	 ad.setTitle(R.string._error);
					    	 ad.setCancelable(false);
					    	 
		//			    	 ad.setIcon(R.drawable.icono);
					    	 ad.setMessage(getResources().getString( R.string._errorConexionWS));
					    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
									
								}
							});
					    	 
					    	 ad.show();
						}
					}
				}
			}
			
			super.onPostExecute(resultado);
		}
	  }


    // ------------------------------------------
    // clase para obtener viaje reciente en forma asincr�nica
    // ------------------------------------------
    public class obtenerViajeReciente extends AsyncTask<String , Void, Respuesta>
	{
    	String _NroTelefono ; 
    	ProgressDialog pd = new ProgressDialog(MainActivityEsquina.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivityEsquina.this);
			
			
			Respuesta _rta;
			_NroTelefono = "" + _texto[0];
			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			// textView_resultado.setText(result);
			
			// textView_resultado.setVisibility(View.VISIBLE);

			 AlertDialog ad = new AlertDialog.Builder(MainActivityEsquina.this).create();
	    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
				}
			});
	    	 
			if (resultado.getNoHayPedidoRealizado())  
			{
		    	 ad.setTitle(R.string._error);
		    	 ad.setCancelable(false);
		//    	 ad.setIcon(R.drawable.icono);
		    	 ad.setMessage(getResources().getString( R.string._vr_No_hay_viaje));

		    	 ad.show();
			}
			else
			{
				if (resultado.getNegativo())
				{
			    	 ad.setTitle(R.string._error);
			    	 ad.setCancelable(false);
		//	    	 ad.setIcon(R.drawable.icono);
			    	 ad.setMessage(getResources().getString( R.string._vr_ViajeNegativo));

			    	 ad.show();
				}
				else
				{
					if (resultado.getYaOcupado())
					{
				    	 ad.setTitle(R.string._error);
				    	 ad.setCancelable(false);
			//	    	 ad.setIcon(R.drawable.icono);
				    	 ad.setMessage(getResources().getString( R.string._vr_ViajeOcupado));

				    	 ad.show();
					}
					else
					{
						if (resultado.getNroViaje() > 0)
						{
							Intent intento = new  Intent(MainActivityEsquina.this,ConViajeActivity.class);
							intento.putExtra("mensaje", resultado.getTexto().toString());
							intento.putExtra("telefono", _pref_telefono);
							intento.putExtra("area", _pref_area);
							if (resultado.getTieneMovil())
								intento.putExtra("movil", resultado.getMovil());
							intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intento);
						}
						else // Ac� no deber�a llegar, creo
						{
							 ad.setTitle(R.string._error);
					    	 ad.setCancelable(false);
				//	    	 ad.setIcon(R.drawable.icono);
					    	 ad.setMessage(getResources().getString( R.string._vr_No_hay_viaje));

					    	 ad.show();
						}
					}
				}
			}
				
		
			super.onPostExecute(resultado);
		}

	  }

    
    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class tieneUnViajeReciente extends AsyncTask<String , Void, Respuesta>
	{
    	String _NroTelefono ; 
    	ProgressDialog pd = new ProgressDialog(MainActivityEsquina.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {


			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivityEsquina.this);
			
			
			Respuesta _rta;
			_NroTelefono = "" + _texto[0];
			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			//textView_resultado.setText(result);
			
		//	textView_resultado.setVisibility(View.VISIBLE);

			 AlertDialog ad = new AlertDialog.Builder(MainActivityEsquina.this).create();
	    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
				}
			});
	    	 
	    	 
			if (resultado.getNroViaje() > 0 && !resultado.getYaOcupado() && !resultado.getNegativo() && !resultado.getYaInformoMovil())
			{
				Intent intento = new  Intent(MainActivityEsquina.this,ConViajeActivity.class);
				intento.putExtra("mensaje", resultado.getTexto().toString());
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area );
				intento.putExtra("vinoAutomatico", true);
				if (resultado.getTieneMovil())
					intento.putExtra("movil", resultado.getMovil());
				
				intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				startActivity(intento);
				finish();
			}
				

			super.onPostExecute(resultado);
		}

	  }
    


}
