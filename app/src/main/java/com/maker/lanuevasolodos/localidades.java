package com.maker.lanuevasolodos;

public class localidades {

		private String localidad;
		private String id_localidad;
		
		public String getLocalidad() {
			return localidad;
		}
		public void setLocalidad(String localidad) {
			this.localidad = localidad;
		}
		public String getId_localidad() {
			return id_localidad;
		}
		public void setId_localidad(String id_localidad) {
			this.id_localidad = id_localidad;
		}
		
}
