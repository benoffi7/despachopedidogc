package com.maker.lanuevasolodos;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class Adaptador extends BaseAdapter{
	// Array con los mensajes
	ArrayList<Direccion> conversacion;
	
	// Direccion, es el objeto que uso cuando vuelve del adaptador para tomar los datos en Activity_favoritas
	Direccion direccion = new Direccion();
	
	// Obtener el contexto
	Context contexto;
	
	// id de seleccionada para que se pinten bien
	private int _idSeleccion = -5;
	
	// Para los colores
	LinearLayout layoutConversacion;
	TextView textView_Espacios;
	

	
	// Generar constructor con Source -> Generate constructor
	public Adaptador(ArrayList<Direccion> conversacion, Context Contexto) {
		super();
		// Se usa el this porque las dos variables se llaman iguales
		// la segunda le puse may�sculas entonces son distinctas y no necesita
		// el this
		this.conversacion = conversacion;
		contexto = Contexto;
	}

	
	// Lop siguiente se genera con Source Ov errider implements methods
	
	// Obtener la cantidad de items
	@Override
	public int getCount() {
		
		return conversacion.size();
	}

	// Obtener el item por posicion
	@Override
	public Direccion getItem(int _pos) {
		return conversacion.get(_pos);
	}

	// no se usa
	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	// Obtener la vista que luego implementar� el constryctor
	@Override
	public View getView(int _pos, View _vistaFila, final ViewGroup _vistaGrupo) {

		//Obtengo un item que es de tipo MENSAJE, por lo tanto ser� un oBjeto menssaje
		final Direccion oDireccion = getItem(_pos);
		
		//defino un contedor
		final viewHolder _vistaFila_contenedora;
		
		if (_vistaFila == null){
			
			//Instanciar el servicio de inflador en su contexto
			LayoutInflater inflador = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			//transformo un xml en una variable Java 
			_vistaFila = inflador.inflate(R.layout.activity_renglon_favoritas, _vistaGrupo, false);
			
			//creamos una instancia de fila_Aux
			_vistaFila_contenedora = new viewHolder();
			
			//asignamos los valores, es como una activity, para poner los valores de la clase MENSAJE
			_vistaFila_contenedora.mensaje = (TextView)_vistaFila.findViewById(R.id.fav_textViewDireccion);
			_vistaFila_contenedora.comentario = (TextView)_vistaFila.findViewById(R.id.fav_textViewComentario);
			

			// BUscar el RelativeLayout para que me sirva para  el click
			// el del xml activuti_renglon, es el click sobre el activity que en realidad lo veo com olinea
			_vistaFila_contenedora.RelativeLayout_Fila = (RelativeLayout)_vistaFila.findViewById(R.id.fav_relativeLayoutRenglon);
			
						
			// PAra el formato y colores
			_vistaFila_contenedora.listView_direcciones = (LinearLayout) _vistaFila.findViewById(R.id.fav_linearLayoutListado);
			
			
			// Si es la que est� seleccionada entonces es color agencia sino blanca
			if ( _idSeleccion != _vistaFila_contenedora.idFavorita)
			{
				_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.dac_gris_sin_borde);
			}
			else
			{
				_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.dac_seleccion_favoritos);

			}
			
			
			//asigno a la fila un metadato con sus componentes ya instanciados a la vista del par�metro que llama �ste m�todo
			_vistaFila.setTag(_vistaFila_contenedora);
		}
		else {
			//levanto los metadatos de la fila para acceder a sus componentes ya instanciados la 1ra vez
			_vistaFila_contenedora = (viewHolder)_vistaFila.getTag();	
			
			// Si es la que est� seleccionada entonces es color agencia sino blanca
			if ( _idSeleccion != _vistaFila_contenedora.idFavorita)
			{
				_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.dac_gris_sin_borde);
			}
			else
			{
				_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.dac_seleccion_favoritos);

			}
		}
		
		// setear
		
		_vistaFila_contenedora.mensaje.setText(oDireccion.getDireccion());
        _vistaFila_contenedora.idFavorita = oDireccion.getIdFavorita();
		_vistaFila_contenedora.comentario.setText(oDireccion.getComentario());
		_vistaFila_contenedora.objDireccionSeleccionada = oDireccion;
		

		
//		_vistaFila_contenedora.linearLayout_Conversacion.setBackgroundColor(Color.rgb(oMensaje.is_salida() ? 255: 175, 0, 122));
//		_vistaFila_contenedora.imagen.setBackgroundColor(Color.rgb(oMensaje.is_salida()? 255: 210, 175, 12));
//		_vistaFila_contenedora.imagen.setVisibility(View.GONE);
		
		
		//siempre se trabaja con el holder porque es el que tiene los componentes XML
		//Se pueden asignar eventos y propiedades
		 // Click
		_vistaFila_contenedora.RelativeLayout_Fila.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(contexto, "Item a borrar la pr�xima!", Toast.LENGTH_SHORT).show();
				
				direccion = _vistaFila_contenedora.objDireccionSeleccionada;
				
				for (int i = 0; i < _vistaGrupo.getChildCount(); i++) {
					_vistaGrupo.getChildAt(i).setBackgroundResource(R.drawable.dac_gris_sin_borde);
				}

				//_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundColor(contexto.getResources().getColor(R.color.color_agencia));
				_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.dac_seleccion_favoritos);
			}
			
		});
		
		// Retorno un View, el mismo que lleg� por par�metro y fue modificado
		return _vistaFila;
		
	}

	
	// -----------------------------------
	// ViewHolder en realidad es un contenedor de vistas o algo as�
	// tiene que tener tantas variables como campos tenga el XML o clase
	// -----------------------------------
	class viewHolder {
		TextView mensaje;
		TextView comentario;
		Direccion objDireccionSeleccionada;

		RelativeLayout RelativeLayout_Fila;
		LinearLayout listView_direcciones;
		
		int idFavorita;
		
	}	
	
}
