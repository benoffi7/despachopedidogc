package com.maker.lanuevasolodos;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

//import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.provider.Telephony.Sms.Conversations;
//import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.maker.soap.conexionServicioWebSOAP;


public class ConViajeActivity extends Activity {

    // ------------------------------------------
	// Declaraci�n de variables
    // ------------------------------------------
	Button button_cancelar, button_buscar, button_salir;
	TextView textView_viajeReciente, textView_movil, textView_ultima;
	String _direccion, _Area, _NroTelefono, _movil = "";
	Boolean _vinoAutomatico, _esMapa;  // Significa que vino porque al querer entrar a la aplicacion ya tenia un viaje pedido
	Calendar _calendario; // Es para  controlar que no use el boton buscar movil indiscriminadamente, se in;
	Date _fecha;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_con_viaje);
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				if (getIntent().getExtras().getString("mensaje") != null)
				{
					_esMapa = ("" + getIntent().getExtras().getString("esmapa")).equals("1");
					_direccion = getIntent().getExtras().getString("mensaje");
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
					_vinoAutomatico = getIntent().getExtras().getBoolean("vinoAutomatico");
				}
			if (getIntent().getExtras().getString("movil") != null)
				_movil = getIntent().getExtras().getString("movil");
		}
		
	    //find views
       	finds_views();
       
        //crear eventos
       	event_create();
       
       	// Si viene desde Main o Map y abrio la app y tenia un viaje asignado, no verificar
		if (!_vinoAutomatico)
       		verificar_Viaje_Reciente();
	}
	
	// -------------------------------
	// find views
	// -------------------------------
	private void finds_views() {
		
		// botones
		button_cancelar = (Button)findViewById(R.id.cv_button_cancelar);
		button_buscar = (Button)findViewById(R.id.cv_button_volver);
		button_salir = (Button)findViewById(R.id.cv_button_salir);    
		
	     // TextViews
	    textView_viajeReciente = (TextView)findViewById(R.id.textView_viajeReciente);
	    textView_movil = (TextView)findViewById(R.id.TextView_vr_movil);
		textView_viajeReciente.setText(_direccion);
		textView_ultima = (TextView)findViewById(R.id.TextView_hora_consulta);
		
		if (_movil.length() > 0)
			textView_movil.setText(_movil);
		
		_calendario = new GregorianCalendar();
		_fecha = _calendario.getTime();
		
		textView_ultima.setVisibility(View.GONE);
	}

	// -------------------------------
	// intenta volver con android a la activity anterior
	// -------------------------------
	@SuppressWarnings("deprecation")
	@Override
	public void onBackPressed() {
		 AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
    	 ad.setTitle(R.string._atencion);
    	 ad.setCancelable(false);
    	 
    	 //ad.setIcon(R.drawable.icono);
    	 ad.setMessage(getResources().getString( R.string._vr_no_puede_volver));
    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				
			}
		});
    	 
    	 ad.show();
	}
	
	// -------------------------------
	// Crear eventos
	// -------------------------------
	private void event_create() {
		
		// Bot�n de salir
		button_salir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				finish();
				
			}
		});
		
		// Bot�n cancelar viaje
		button_cancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				DialogoPersonalizado dp = new DialogoPersonalizado( );
//			    dp.show(getFragmentManager(), null);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ConViajeActivity.this);
				 
				// set title
				alertDialogBuilder.setTitle(getString(R.string.title_activity_con_viaje));
				// set dialog message
				alertDialogBuilder
					.setMessage(getString(R.string._vr_tituloEliminacion))
					.setCancelable(false)
					.setPositiveButton(getString(R.string._si),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							 cancelarPedido or = new cancelarPedido();
						     // Paso el par�metor al hilo paralelo
						     or.execute(_Area, _NroTelefono);
						}
					  })
					.setNegativeButton(getString(R.string._no),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
			}
		});
		
		// Bot�n bucar movil
		button_buscar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {


				_calendario = new GregorianCalendar();
	
				if (_fecha.compareTo(_calendario.getTime()) >= 0) 
				{
				     textView_ultima.setText(R.string._vr_Ultima_consulta);
				     textView_ultima.setText(textView_ultima.getText().toString() + " " + formatear_hora(_fecha));
	
			    	 AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
			    	 ad.setTitle(R.string._error);
			    	 ad.setCancelable(false);
			    	 
		//	    	 ad.setIcon(R.drawable.icono);
			    	 ad.setMessage(getResources().getString( R.string._vr_No_puede_consultar_rapido));
			    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							
						}
					});
			    	 
					ad.show();
	
					return;
				}
				else
				{
					verificar_Viaje_Reciente();
				}
			}

		});
	}

	// -------------------------------
	// Formatear la hora en HH:mm:ss
	// -------------------------------
	@SuppressWarnings("deprecation")
	public String formatear_hora(Date _fecha) {
		String _rta = "";
		
		_rta  +=(_fecha.getHours() < 10 ? "0" +_fecha.getHours() : _fecha.getHours());

		_rta += ":" +(_fecha.getMinutes() < 10 ? "0" +_fecha.getMinutes() : _fecha.getMinutes());
		
		_rta += ":" +(_fecha.getSeconds() < 10 ? "0" +_fecha.getSeconds() : _fecha.getSeconds());
		
		return _rta;
	} 
	
	// -------------------------------
	// verificar viaje reciente, est� ac� para poder llamarlo por c�digo en vez del click
	// -------------------------------
	public void verificar_Viaje_Reciente()
	{
		_calendario.add(Calendar.SECOND, 15);
	    _fecha = _calendario.getTime();

		buscarMovilAsignado or = new buscarMovilAsignado();
	    // Paso el par�metor al hilo paralelo
	    or.execute(_Area, _NroTelefono);
	     
	    textView_ultima.setText(R.string._vr_Ultima_consulta);
	    textView_ultima.setText(textView_ultima.getText().toString() + " " + formatear_hora(_fecha));
	    textView_ultima.setVisibility(View.VISIBLE);
	}
	
	// -------------------------------
	// Crear evento que inicie el activity main
	// -------------------------------
	public void lanzar_Activity_Main() {
		Intent intento;
		if (_esMapa)
			intento = new Intent(ConViajeActivity.this, MapActivity.class);
		else
			intento = new Intent(ConViajeActivity.this, MainActivity.class);
		
		intento.putExtra("telefono", _NroTelefono);
		intento.putExtra("area", _Area);

	    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	    	
		startActivity(intento);
		finish();
	}

	// ------------------------------------------
	// clase dialog box
	// ------------------------------------------
//	public class DialogoPersonalizado extends DialogFragment {
//
//		public Boolean rta = false;
//		@SuppressLint("InflateParams") @Override
//		public Dialog onCreateDialog(Bundle savedInstanceState) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//		
//		LayoutInflater inflater = getActivity().getLayoutInflater();
//
//		builder.setView(inflater.inflate(R.layout.dialogsino, null))
//			.setNegativeButton(getString(R.string._no), new  DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//					
//				}
//			})
//			.setPositiveButton(getString(R.string._si), new DialogInterface.OnClickListener() {
//		
//				public void onClick(DialogInterface dialog, int id) {
//					
//			    	 cancelarPedido or = new cancelarPedido();
//				     // Paso el par�metor al hilo paralelo
//				     or.execute(_Area, _NroTelefono);
//					dialog.dismiss();
//				}
//		    }
//
//			);
//		return builder.create();
//		}
//		
//	}
	
	
	// ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class cancelarPedido extends AsyncTask<String, Void, Respuesta>	{

    	ProgressDialog pd = new ProgressDialog(ConViajeActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._vr_intentandoCancelar));
			pd.setCancelable(false);
			pd.show();
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(ConViajeActivity.this);
			
			Respuesta _rta;
			_rta = cw.Cancelar_pedido("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

			 AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
	    	 ad.setCancelable(false);
	    	 
			if (resultado.getPedidoCancelado())  
			{
				ad.setTitle(R.string._vr_tituloEliminadoOk);
		//		ad.setIcon(R.drawable.icono);
				ad.setMessage(getResources().getString( R.string._vr_EliminadoCorrectamente));
				ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						lanzar_Activity_Main();
					}
				});
				ad.show();
			}
			else
			{
				ad.setTitle(R.string._vr_tituloEliminadoNo);
	//			ad.setIcon(R.drawable.icono);
				ad.setMessage(getResources().getString( R.string._vr_NoSePuedoBorrar));
				ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				ad.show();
			}
			 
			super.onPostExecute(resultado);
		}
	}
    
    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class buscarMovilAsignado extends AsyncTask<String, Void, Respuesta>
	{
    	ProgressDialog pd = new ProgressDialog(ConViajeActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._vr_buscando_movil));
			pd.show();

			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(ConViajeActivity.this);
			
			Respuesta _rta;

			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

			if (resultado.getError_conexion())
            {
                AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString(R.string._error_internet));
                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
            else
            if (resultado.getError_conexionServidor())
            {
                AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString(R.string._err_servidor));
                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
            else {

                if (resultado.getEliminado()) {  // uno
                    AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
                    ad.setTitle(R.string._atencion);
                    ad.setCancelable(false);

                    //			 ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString(R.string._vr_Viaje_eliminado));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            dialog.dismiss();
                            lanzar_Activity_Main();
                        }
                    });

                    ad.show();
                }  // uno
                else {  // dos else ppal
                    if (resultado.getNegativo()) {
                        AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
                        ad.setTitle(R.string._atencion);
                        ad.setCancelable(false);

                        //			 ad.setIcon(R.drawable.icono);
                        ad.setMessage(getResources().getString(R.string._vr_ViajeNegativo));
                        ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                                lanzar_Activity_Main();
                            }
                        });

                        ad.show();

                    } else {
                        if (resultado.getTieneMovil()) {
                            if (resultado.getYaOcupado()) {
                                AlertDialog ad = new AlertDialog.Builder(ConViajeActivity.this).create();
                                ad.setTitle(R.string._atencion);
                                ad.setCancelable(false);

                                //		    	 ad.setIcon(R.drawable.icono);
                                ad.setMessage(getResources().getString(R.string._vr_ViajeOcupado) + resultado.getMovil());
                                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(final DialogInterface dialog, final int which) {
                                        dialog.dismiss();
                                        lanzar_Activity_Main();
                                    }
                                });

                                ad.show();
                            } else
                                textView_movil.setText(resultado.getMovil());
                        } else {
                            textView_movil.setText(R.string._vr_Sin_Movil);
                        }
                    }
                } // dos else ppal
            }

			super.onPostExecute(resultado);
		}
	}
}