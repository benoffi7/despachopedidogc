package com.maker.lanuevasolodos;

import com.maker.soap.conexionServicioWebSOAP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class DatosUltimoViajeActivity extends Activity {

	TextView txtOrigen, txtMovil, txtChofer, txtNroViaje, txtEspera, txtMetros, txtFinViaje, txtImporte, txtVelmax, txtFecha, txtNoHayViaje;
	TextView txtYaCalificado;
	Button btnVolver, btnPrueba;
	
	ScrollView scrollV;

	String _NroTelefono, _Area, _deDonde, _nroViajeMostrado;
	
	Integer numeroViajeCalificado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_datos_ultimo_viaje);
		
		_nroViajeMostrado = "0";
		numeroViajeCalificado = 0;
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				if (getIntent().getExtras().getString("telefono") != null)
				{
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
					_deDonde = getIntent().getExtras().getString("deDonde");
				}
		}
		
		findViews();
		
		createEvents();
		
		obtenerUltimoViaje ouv = new obtenerUltimoViaje();
		ouv.execute(_NroTelefono, _Area);
	}
	
	private void createEvents() {
		btnPrueba.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intento = new  Intent(DatosUltimoViajeActivity.this, CalificarActivity.class);
				intento.putExtra("nroViaje", _nroViajeMostrado);
				
				startActivity(intento);
				v.setVisibility(View.INVISIBLE);
			}
		});
		
		// Bot�n volver
		btnVolver.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					switch (_deDonde) {
					case "map":
						Intent intento = new  Intent(DatosUltimoViajeActivity.this, MapActivity.class);
						intento.putExtra("telefono", _NroTelefono);
						intento.putExtra("area", _Area);
						startActivity(intento);
						break;

					default:
						Intent intento2 = new  Intent(DatosUltimoViajeActivity.this, MainActivity.class);
						intento2.putExtra("telefono", _NroTelefono);
						intento2.putExtra("area", _Area);
						startActivity(intento2);
						break;
					}

					finish();
				}
			});
	}

	private void findViews() {
		// boton para calificar
		btnPrueba = (Button)findViewById(R.id.buttonPrueba);
		btnPrueba.setVisibility(View.GONE);
		
		scrollV = (ScrollView)findViewById(R.id.scrollViewsuv);
		scrollV.setVisibility(View.INVISIBLE);
		
		txtNoHayViaje = (TextView)findViewById(R.id.txtNoHayViaje);
		txtNoHayViaje.setVisibility(View.GONE);
		
		btnVolver = (Button)findViewById(R.id.button_volver_duv);
		txtOrigen = (TextView)findViewById(R.id.duvOrigen);
		txtMovil = (TextView)findViewById(R.id.duvMovil);
		txtChofer = (TextView)findViewById(R.id.duvChofer);
		txtNroViaje = (TextView)findViewById(R.id.duvNroViaje);
		txtEspera = (TextView)findViewById(R.id.duvEspera);
		txtMetros = (TextView)findViewById(R.id.duvMetros);
		txtFinViaje = (TextView)findViewById(R.id.duvFinalizado);
		txtImporte = (TextView)findViewById(R.id.duvImporte);
		txtVelmax = (TextView)findViewById(R.id.duvVelMax);
		txtFecha = (TextView)findViewById(R.id.duvFecha);
		txtYaCalificado = (TextView)findViewById(R.id.textViewYaCalificado);
	}
	
    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class obtenerUltimoViaje extends AsyncTask<String , Void, ULTIMO_VIAJE>
	{
    	ProgressDialog pd = new ProgressDialog(DatosUltimoViajeActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected ULTIMO_VIAJE doInBackground(String... _texto) {

			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(DatosUltimoViajeActivity.this);
			
			ULTIMO_VIAJE _rta;

			_rta = cw.Obtener_ultimo_viaje("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(ULTIMO_VIAJE r) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

            //Datos Último Viaje Activity, utilizar valor -1 como código de error "Sin Internet". Valor -2: "Servidor respondió con error" // Valor 0: "No tiene Viajes".

            if (r.getNroviaje().equals("-1"))
            {
                AlertDialog ad = new AlertDialog.Builder(DatosUltimoViajeActivity.this).create();
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString(R.string._error_internet));
                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
            else
            if (r.getNroviaje().equals("-2"))
            {
                AlertDialog ad = new AlertDialog.Builder(DatosUltimoViajeActivity.this).create();
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString(R.string._err_servidor));
                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
            else
			if (r.getNroviaje().equals("0"))
			{
				txtNoHayViaje.setVisibility(View.VISIBLE);
			}
			else
			{
				txtChofer.setText(r.getChofer());
				txtMetros.setText(r.getMts());
				txtEspera.setText(r.getEspera());
				txtFecha.setText(r.getFecha_hora());
				txtMovil.setText(r.getMovil());
				txtFinViaje.setText(r.getUbicacion_fin());
				txtImporte.setText(r.getImporte());
				txtVelmax.setText(r.getVelmax());
				txtOrigen.setText(r.getOrigen());
				txtNroViaje.setText(("0000"+ r.getNroviaje()).substring(("0000"+ r.getNroviaje()).length() - 4));
				scrollV.setVisibility(View.VISIBLE);
				
				_nroViajeMostrado = ""+r.getNroviaje();
				
				SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
				numeroViajeCalificado = preferencias.getInt("nroViajeCalificado", 0);
				
				if (numeroViajeCalificado == Integer.parseInt(_nroViajeMostrado ) )
				{
					txtYaCalificado.setVisibility(View.VISIBLE);
					btnPrueba.setVisibility(View.INVISIBLE);
				}
				else
				{
					btnPrueba.setVisibility(View.VISIBLE);
					txtYaCalificado.setVisibility(View.INVISIBLE);
				}				
			}
				
			super.onPostExecute(r);
		}
	}
}
