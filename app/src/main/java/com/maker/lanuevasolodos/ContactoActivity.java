package com.maker.lanuevasolodos;

//import com.maker.pedirremis.ConViajeActivity.DialogoPersonalizado;
import com.maker.soap.conexionServicioWebSOAP;

        import android.app.Activity;
import android.app.AlertDialog;
        import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
        import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ContactoActivity extends Activity {

	String _NroTelefono, _Area;
	EditText editText_texto;
	Button button_Agencia, button_Maker, button_Volver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacto);
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				// Tiene el campo _codigo? (siempre deber�a ser SI)
				if (getIntent().getExtras().getString("telefono") != null)
				{
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
				}
		}
		
		findViews();
		
		createEvents();
		
		editText_texto.requestFocus();
	}

	// ----------------------------
	// Find views
	// ----------------------------
	private void findViews() {
		editText_texto = (EditText)findViewById(R.id.editText_ct_aEnviar);
		button_Agencia = (Button)findViewById(R.id.ct_button_desarrollo);
		button_Maker = (Button)findViewById(R.id.ct_button_enviar);
		button_Volver = (Button)findViewById(R.id.button_volverCtc);
	}
	
	// ----------------------------
	// Create events
	// ----------------------------
	private void createEvents() {
		
		// Bot�n a maker
		button_Maker.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View arg0) {
				if (!controlaTexto())
					return;
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContactoActivity.this);
				
				alertDialogBuilder.setTitle(getString(R.string.title_activity_contacto));
				// set dialog message
				alertDialogBuilder
					.setMessage(getString(R.string._ct_enviar_comentarios))
					.setCancelable(false)
					.setPositiveButton(getString(R.string._si),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
					    	 enviarComentarios or = new enviarComentarios();
						     // Paso el par�metor al hilo paralelo
						     or.execute(_Area, editText_texto.getText().toString().trim(), "0", _NroTelefono);
						}
                    })
					.setNegativeButton(getString(R.string._no),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			}
		});
		
		// Bot�n a agencia
		button_Agencia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!controlaTexto())
					return;
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContactoActivity.this);
				
				alertDialogBuilder.setTitle(getString(R.string.title_activity_contacto));
				// set dialog message
				alertDialogBuilder
					.setMessage(getString(R.string._ct_enviar_comentarios))
					.setCancelable(false)
					.setPositiveButton(getString(R.string._si),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
					    	 enviarComentarios or = new enviarComentarios();
						     // Paso el par�metor al hilo paralelo
						     or.execute(_Area, editText_texto.getText().toString().trim(),  "1", _NroTelefono);
						}
					  })
					.setNegativeButton(getString(R.string._no),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			}

		});
		
		// Bot�n volver
		button_Volver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	// ------------------------------------------
	// Controla que haya texto a enviar, al menos 20 letras
	// ------------------------------------------
	private boolean controlaTexto() {
		return (editText_texto.getText().length() >= 20);
	}
	
	// ------------------------------------------
    // clase para enviar el mensaje
    // ------------------------------------------
    public class enviarComentarios extends AsyncTask<String, Void, Boolean>	{
    	ProgressDialog pd = new ProgressDialog(ContactoActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._ct_enviando_comentarios));
			pd.setCancelable(false);
			pd.show();
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Boolean doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(ContactoActivity.this);
			
			boolean  _rta;
			
			_rta = cw.Enviar_comentarios("" + _texto[0], "" + _texto[1], "" + _texto[2], "" + _texto[3]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(final Boolean resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
            AlertDialog ad = new AlertDialog.Builder(ContactoActivity.this).create();
	    	ad.setCancelable(false);

            ad.setTitle(R.string._comentario);
	    	 
	  //  	ad.setIcon(R.drawable.icono);
	    	if (resultado)
                ad.setMessage(getResources().getString( R.string._ct_enviado_ok));
	    	else
	    		ad.setMessage(getResources().getString( R.string._ct_enviado_no));
	    	 
            ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					if (resultado)
						finish();
				}
			});

			ad.show();
			 
			super.onPostExecute(resultado);
		}
    }
}
