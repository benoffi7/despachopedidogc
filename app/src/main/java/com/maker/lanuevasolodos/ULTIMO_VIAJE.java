package com.maker.lanuevasolodos;

public class ULTIMO_VIAJE {
	
    public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getChofer() {
		return chofer;
	}

	public void setChofer(String chofer) {
		this.chofer = chofer;
	}

	public String getFecha_hora() {
		return fecha_hora;
	}

	public void setFecha_hora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

	public String getMts() {
		return mts;
	}

	public void setMts(String mts) {
		this.mts = mts;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getEspera() {
		return espera;
	}

	public void setEspera(String espera) {
		this.espera = espera;
	}

	public String getNroviaje() {
		return nroviaje;
	}

	public void setNroviaje(String nroviaje) {
		this.nroviaje = nroviaje;
	}

	public String getVelmax() {
		return velmax;
	}

	public void setVelmax(String velmax) {
		this.velmax = velmax;
	}

	public String getUbicacion_fin() {
		return ubicacion_fin;
	}

	public void setUbicacion_fin(String ubicacion_fin) {
		this.ubicacion_fin = ubicacion_fin;
	}

	private String origen;

    private String movil;

    private String chofer;

    private String fecha_hora;
 
    private String mts;

    private String importe;

    private String espera;

    private String nroviaje; 
 
    private String velmax;

    private String ubicacion_fin;


}
