package com.maker.lanuevasolodos;


import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

public class Confirmacion extends Activity {

	// ------------------------------------------
	// Variables
	// ------------------------------------------
	String _direccion = "",	_Area, _NroTelefono, _calle1, _calle2, _comentario, _localidad, _textoAliasMovil, _altura = "", _abrLocalidad;
	TextView textView_direccion, textView_cantidad, textView_altura;
	EditText txtComentario, txtAlturaExacta, txt_altura;
	Button button_Volver, button_confirmar;
	Boolean _agregaAFavoritas = false;
	CheckBox chkHabitual, chkBaul, chkCambio;
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmacion);
	
		// verificar si el intent no es nulo
		// si no es nulo lo tomo
		if (getIntent() != null)
		{
			// Si tiene extras
			if (getIntent().getExtras() != null)
				if (getIntent().getExtras().getString("direccion") != null)
				{
					_direccion = getIntent().getExtras().getString("direccion");
					_localidad = getIntent().getExtras().getString("localidad");
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
					_calle1 = getIntent().getExtras().getString("calle1");
					_calle2 = (getIntent().getExtras().getString("calle2").compareTo("anyType{}") == 0 ? "maker" : getIntent().getExtras().getString("calle2"));
					_altura = (getIntent().getExtras().getString("altura").compareTo("anyType{}") == 0 ? "" : getIntent().getExtras().getString("altura"));

					if ( getIntent().getExtras().getString("comentario") != null)
						_comentario = (getIntent().getExtras().getString("comentario").compareTo("anyType{}") == 0 ? "" : getIntent().getExtras().getString("comentario"));
					else
						_comentario = "";
					
					_textoAliasMovil =  getIntent().getExtras().getString("textoAliasMovil");
					
					// Verificaci�n para agregar localidad entre parentesis a la direcci�n, s�lo en caso de que utilice localidades
					if (Boolean.parseBoolean( getString(R.string.utilizaLocalidad)))
					{
						_abrLocalidad = getIntent().getExtras().getString("abrLocalidad");
						_textoAliasMovil = "(" + _abrLocalidad + ") " + _textoAliasMovil;
					}
					
					// Para hacer esto con getString(campo, default) se debe usar minimo API 12 y ahora se esta usando 11
					if (_altura.length() <= 0)
						_altura = "0";
				}
		}
		
        //find views
        finds_views();
       
        // Iniciar base de datos
	   	bdd = new BaseDeDatos(getApplicationContext(), getResources().getString(R.string.DB_nombre), null, Integer.parseInt(getResources().getString(R.string.DB_version)));
	   	
		// Completar la pantalla
		Completar_datos();
	
		// crear eventos
		event_create();
	}

	@Override
	public void onBackPressed() {
		
		Intent intento = new Intent(Confirmacion.this, MainActivity.class);
		
		intento.putExtra("telefono", _NroTelefono);
		intento.putExtra("area", _Area);

	    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	    	
		startActivity(intento);
		finish();
		//super.onBackPressed();
	}

	// ------------------------------------------
	// Realizar todos los findViews de los componentes a usar
	// ------------------------------------------
	private void finds_views() {
		// botones
		button_confirmar = (Button)findViewById(R.id.conf_buttonConfirmar);

		// Comentarios
		txtComentario = (EditText)findViewById(R.id.EditText_comentarios);
		txtComentario.setHint(R.string._hint_comentario);

		// textviews
		textView_direccion = (TextView)findViewById(R.id.cnf_textView_direccion);
		
		// Altura, ocultar
		textView_altura = (TextView)findViewById(R.id.TextView_comentarioMap);
		textView_altura.setVisibility(View.GONE);
		txt_altura= (EditText)findViewById(R.id.editText_AlturaConf);
		txt_altura.setVisibility(View.GONE);
	
		// chk habitual
		chkHabitual = (CheckBox)findViewById(R.id.checkBox_habitualConf);
		chkHabitual.setChecked(_agregaAFavoritas);
		chkHabitual.setTextColor(_agregaAFavoritas ? getResources().getColor(R.color.color_agencia) : getResources().getColor(R.color.blanco));
		
		chkBaul = (CheckBox)findViewById(R.id.checkBoxBaul);
		chkCambio = (CheckBox)findViewById(R.id.checkBoxCambio);
	}
		
	// ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void Completar_datos() {
		textView_direccion.setText(_direccion);

		if (_calle1.length() > 3)
			bdd.Guardar_calle(_calle1, _localidad);
		
		if (_calle2.length() > 3 && _calle2.compareTo("maker") != 0)
			bdd.Guardar_calle(_calle2, _localidad);

		if (_agregaAFavoritas)
			bdd.Guardar_direccion(_calle1, _calle2, Integer.parseInt(_altura), _localidad, _comentario, _textoAliasMovil);
	}
	
    // ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void event_create() {
		
		// Bot�n confirmar
		button_confirmar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View arg0) {
				
				if (!Funciones.hayInternet(getApplicationContext())) {
					AlertDialog ad = new AlertDialog.Builder(Confirmacion.this).create();
				    ad.setTitle(R.string._error);
				    ad.setCancelable(false);
				    	 
				    //	 ad.setIcon(R.drawable.icono);
				    ad.setMessage(getResources().getString( R.string._err_sin_internet));
				    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
					});
				    	 
					ad.show();
				}
				else {
					if (chkHabitual.isChecked())
						bdd.Guardar_direccion(_calle1, _calle2, Integer.parseInt(_altura), _localidad, _comentario, _textoAliasMovil);

					// Creo objeto para ejecutr el hilo paralelo
					confirmarResultado or = new confirmarResultado();
					// Paso el par�metor al hilo paralelo
					or.execute(_Area, _NroTelefono, _localidad, chkBaul.isChecked() ? "true" : "false", chkCambio.isChecked() ? "true" : "false", "" + txtComentario.getText());
				}
			}
		});
		
//		// Bot�n volver
//		button_Volver.setOnClickListener( new OnClickListener() {
//			
//			@Override
//			public void onClick(View vista) {
//				finish();
//			}
//		});
		
        // Cambiar el color del check cada vez que cambia de estado
        chkHabitual.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				chkHabitual.setTextColor(isChecked ? getResources().getColor(R.color.color_agencia) : getResources().getColor(R.color.blanco));
				
			}
		});
	}

	// Ac� s�lo entrar�a en el supuesto caso de error de numerode telefono
	private void iniciarErrorTelefono()
	{
		SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferencias.edit();
		editor.putString("telefono", "");
		editor.putString("area", "");
		editor.putString("telefonoCompleto", "");
		editor.putString("habilitado", "");
		editor.apply();
		
		Intent intento = new  Intent(Confirmacion.this,RegistrationActivity.class);
		intento.putExtra("mensaje", _textoAliasMovil );
		intento.putExtra("telefono", _NroTelefono);
		intento.putExtra("area", _Area);
		intento.putExtra("esmapa", "1");
		intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		startActivity(intento);
		
		finish();
	}
	
    // ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class confirmarResultado extends AsyncTask<String, Void, Respuesta>
	{
    	String _comentario;
    	
    	ProgressDialog pd = new ProgressDialog(Confirmacion.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._conf_confirmado_pedido_aguarde));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(Confirmacion.this);
			
			_comentario = "" + _texto[5];

			//iniciarErrorTelefono();
			if (("" + _texto[0]).trim().length() < 1 || ( "" + _texto[1]).trim().length() < 1)
			{
				iniciarErrorTelefono();
			}
			
			if (( "" + _texto[1]).contains("n") || ( "" + _texto[0]).contains("n"))
			{
				iniciarErrorTelefono();
			}
			
			// area, telefono, localidad, baul ("true"/"false"), cambio("true"/"false"), comentario
			Respuesta _rta;
			_rta = cw.Confirmar_pedido("" + _texto[0], "" + _texto[1], "" + _texto[2], "" + _texto[3], "" + _texto[4], "" + _texto[5]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

			AlertDialog ad = new AlertDialog.Builder(Confirmacion.this).create();
			 
			if (resultado.getPedidoConfirmado())  
			{
				ad.setTitle(R.string._conf_titulo_viaje_confirmado);
		    	ad.setCancelable(false);
		    	 
		    //	ad.setIcon(R.drawable.icono);
		    	ad.setMessage(getResources().getString( R.string._conf_viaje_confirmado));
		    	ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						Intent intento = new  Intent(Confirmacion.this, ConViajeActivity.class);
						intento.putExtra("mensaje", _textoAliasMovil + "\n" + _comentario);
						intento.putExtra("telefono", _NroTelefono);
						intento.putExtra("area", _Area);
						intento.putExtra("esmapa", "0");
						intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
						
						startActivity(intento);
						
						finish();
					}

				});
		    	 
				ad.show();
			}
			else
			{
				if (resultado.getNoHayPedidoRealizado())
				{
					ad.setTitle(R.string._conf_pedido_reciente);
			    	ad.setCancelable(false);

			   // 	ad.setIcon(R.drawable.icono);
			    	ad.setMessage(getResources().getString( R.string._conf_hay_viaje_registrado));
					ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			   // 	ad.setIcon(R.drawable.icono);
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							return;
						}
					});
			    	 
			    	ad.show();
				}
                else if (resultado.getNoPuedeSolicitar())
                {
                    ad.setTitle(R.string._error);
                    ad.setCancelable(false);

                    // 	 ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._conf_error_conexcion));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });

                    ad.show();
                }
                else
                {
                    ad.setTitle(R.string._conf_no_hay_viaje_realizado);
                    ad.setCancelable(false);

                    // 	 ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._msg_error_generico_viaje));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });

                    ad.show();
                }
			}
			
			super.onPostExecute(resultado);
		}
	}
}
