package com.maker.lanuevasolodos;

import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;
import com.maker.lanuevasolodos.R.color;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    // ------------------------------------------
	// Declaraci�n de variables
    // ------------------------------------------
	Button button_confirmar, button_favoritos;
	EditText editText_localidad, editText_calle, editText_altura, editText_esquina;
	TextView textView_nroTelefono;
	LinearLayout layoutCalle, layoutAltura, layoutEsquina, layoutLocalidad;
	String _Localidad;
	ImageButton button_Teclado_Calle, button_Teclado_Esquina, button_Mapa;
	String _version;
	Spinner sp; 
    Boolean _VerificarViaje;
    
    // Para ubicacion
    LocationManager locManager;
    Location ubicacion;
    String proveedorLocalizacion;
	
	// localidades
    //private String[] localidades = {"SANTA ROSA"};
    //private String[] localidades = {"LA PLATA"};
    //private String[] localidades = {"MAR DEL PLATA"};
    //private String[] localidades = {"GUALEGUAYCHU"};
    //private String[] localidades = {"PINAMAR", "OSTENDE", "VALERIA DEL MAR", "CARILO"};
	//private String[] localidades = {"LOMAS DE ZAMORA", "LANUS"};
	//private String[] localidades = {"SANTA TERESITA", "LAS TONINAS", "COSTA CHICA", "MAR DEL TUYÚ", "COSTA DEL ESTE", "AGUAS VERDES", "LA LUCILA DEL MAR", "COSTA AZUL", "SAN BERNARDO", "MAR DE AJÓ", "NUEVA ATLANTIS"};
	//private String[] localidades = {"LUJAN"};
	//private String[] localidades = {"RIO TERCERO"};
	private String[] localidades = {"CIPOLLETTI"};

    // ------------------------------------------
	// Declaraci�n de variables de recepci�n de habituales
    // ------------------------------------------
	String _hCalle, _hAltura, _hEsquina, _hComentario, _hLocalidad, _hIdLocalidad;
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	// ------------------------------------------
	// Declaraci�n de variables de preferencias
    // ------------------------------------------
	String _pref_telefono, _pref_habilitado, _pref_area;
 	// SharedPreferences preferencias;
 	
    // ------------------------------------------
	// Para ver si tiene permisos de usar el gps
    // ------------------------------------------
	Boolean _puedeUsarGPS;
	protected String _packageName;
	
    // ------------------------------------------
	// Variable para ver si tiene permisos de aplicaci�n o no
    // ------------------------------------------
	static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
	static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
	
    // ------------------------------------------
	// OnCreate de la aplicaci�n
    // ------------------------------------------
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        _packageName = this.getPackageName();
        _puedeUsarGPS  =true;
         
		if (Build.VERSION.SDK_INT >= 23)
		{
			//Para saber si tiene permiso, no se usa
			//int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
			
			// Permiso de gps
			if (ContextCompat.checkSelfPermission(this,	 Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				_puedeUsarGPS  = false;
				if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
						ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
				} else {
					ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
					
					if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
					} else {
						ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
					}
					Toast toast;
					toast = Toast.makeText(MainActivity.this, R.string._suv_vuelva_a_iniciar, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 50);
					toast.show();

			        finish();
				}
	        }
			
			// Permiso de almacenamiento
			if (ContextCompat.checkSelfPermission(this,	Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
				_puedeUsarGPS  = false;
				if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
				} else {
					ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
				        
					Toast toast;
					toast = Toast.makeText(MainActivity.this, R.string._suv_vuelva_a_iniciar, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 150);
					toast.show();

			        finish();
				}
	        }
		}
		
    	PackageInfo info;
		PackageManager manager = this.getPackageManager();
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			
			_version = "" + info.versionName;
		} catch (NameNotFoundException e) {
			Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
			finish();
		}
        
		SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		_pref_telefono = preferencias.getString("telefono", "");
		_pref_area = preferencias.getString("area", "");
		
        _VerificarViaje = true;
    	if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
			{
				// Si tiene calle, altura, esquina, comentario es porque viene de habituales
				if (getIntent().getExtras().getString("calle") != null)
				{
					// poner los datos en variables que lueg ose pasaran a los views (componenentes) una vez
					// que los haya encontrado con el findviews en la llamada siguiente (finds_views())
					_hCalle = getIntent().getExtras().getString("calle");
					_hAltura = getIntent().getExtras().getString("altura");
					_hEsquina = getIntent().getExtras().getString("esquina");
					_hComentario = getIntent().getExtras().getString("comentario");
					_hLocalidad = getIntent().getExtras().getString("localidad");
				}
				
				// si viene de AyudaCalle completar la calle
				if (getIntent().getExtras().getString("seleccionCalle") != null)
				{
					_VerificarViaje = false;
					if (getIntent().getExtras().getString("calle")      != null) _hCalle      = getIntent().getExtras().getString("calle");
					if (getIntent().getExtras().getString("altura")     != null) _hAltura     = getIntent().getExtras().getString("altura");
					if (getIntent().getExtras().getString("esquina")    != null) _hEsquina    = getIntent().getExtras().getString("esquina");
					if (getIntent().getExtras().getString("comentario") != null) _hComentario = getIntent().getExtras().getString("comentario");
					if (getIntent().getExtras().getString("localidad")  != null) _hLocalidad  = getIntent().getExtras().getString("localidad");
				}
				
				if (getIntent().getExtras().getString("verificaViaje") != null)
				{
					_VerificarViaje = false;
				}
			}
		}

    	Guardar_preferencia_de_pantalla("ST");

        //find views
       	finds_views();

       	// Verificar si tiene GPS, si tiene buscar la calle m�s cercana
       	comenzarLocalizacion();
       
        //crear eventos
       	event_create();
       
       	// Crear objeto base de datos
   		bdd = new BaseDeDatos(getApplicationContext(), getResources().getString(R.string.DB_nombre), null, Integer.parseInt(getResources().getString(R.string.DB_version)));
       
   		// S�lo no verifica en el caso de que vuelva de la selecci�n de calle
   		if (_VerificarViaje)
   			verificarSiTieneViajePedido();
   			
       	// Poner foco en calle o localidad, depende de las  preferencias
   		editText_calle.requestFocus();

		//Mostrar_mensaje_calle_inter();

        //Mostrar_mensaje_calle_laPlata();
    }

    // Muestra un mensaje hasta que el usuario pone no volver a mostrar
	private void Mostrar_mensaje_calle_inter() {
		SharedPreferences preferencias;
		preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		String mostrar = preferencias.getString("mostrarAviso", "1");

		if (mostrar.equals("1") || mostrar.length() < 1)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

			builder.setMessage("Recuerde siempre ingresar una intersección")
			.setTitle("Importante")
			.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}})
			.setNegativeButton("No volver a mostrar", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					guardarNoMostrarMensaje();
					dialog.cancel();
				}

				private void guardarNoMostrarMensaje() {
					try
					{
						SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

						// crear el editor de preferencias
						SharedPreferences.Editor editor = preferencias.edit();

						// Agregar como preferencia el telefono
						editor.putString("mostrarAviso", "0");

						editor.apply();
					}
					catch (Exception ex)
					{

					}

					return;
				}
			});
			builder.create();
			builder.show();
		}
	}

	// Muestra un mensaje hasta que el usuario pone no volver a mostrar
    private void Mostrar_mensaje_calle_laPlata() {
    	SharedPreferences preferencias;
    	preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		String mostrar = preferencias.getString("mostrarAviso", "1");
    	
		if (mostrar.equals("1") || mostrar.length() < 1)
		{
	    	AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
	    	
	    	builder.setMessage("Recuerde que para las direcciones fuera de La Plata debe indicar una esquina")
			.setTitle("Importante")
			.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}})
			.setNegativeButton("No volver a mostrar", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					guardarNoMostrarMensaje();
					dialog.cancel();
				}

				private void guardarNoMostrarMensaje() {
					try
					{
						SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

						// crear el editor de preferencias
						SharedPreferences.Editor editor = preferencias.edit();

						// Agregar como preferencia el telefono
						editor.putString("mostrarAviso", "0");

						editor.apply();
					}
					catch (Exception ex)
					{

					}

					return;
				}
			});
			builder.create();
			builder.show();
		}
	}

	// Localizaci�n por GPS
    private void comenzarLocalizacion()
    {
        // Control de GPS - Obtenemos una referencia al LocationManager
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    }
    
    // Para permisos de version 23 y superior marshmallows y siguientes
	@Override
	public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
	        case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
	            // If request is cancelled, the result arrays are empty.
	            if (grantResults.length > 0
	                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
	            } else {
	
	            }
	            return;
	        } case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
	            // If request is cancelled, the result arrays are empty.
	            if (grantResults.length > 0
	                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
	            } else {
	
	            }
	            return;
	        }
        }
    }
    
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		Intent intento;
		
		switch (id) {
		case R.id.action_ayuda:
			intento = new Intent(getApplicationContext(), Activity_help.class);
			startActivity(intento);
			break;
		case R.id.action_estado:
			intento = new Intent(getApplicationContext(), Estado_servicio.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area);
			startActivity(intento);
			break;
		case R.id.action_contacto:
			intento = new Intent(getApplicationContext(), ContactoActivity.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area);
			startActivity(intento);
			break;
		case R.id.action_acercade:
			intento = new Intent(getApplicationContext(), AcercaDeActivity.class);
			startActivity(intento);
			break;
		case R.id.action_ultimoviaje:
			intento = new Intent(getApplicationContext(), DatosUltimoViajeActivity.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area);
			intento.putExtra("deDonde", "Std");
			startActivity(intento);
			break;
		default:
			return true;
		}
	
		return super.onOptionsItemSelected(item);
	}
	
	// ------------------------------------------
	// Control para ver si tiene un viaje pedido
    // ------------------------------------------
	private void verificarSiTieneViajePedido() {
		 tieneUnViajeReciente or = new tieneUnViajeReciente();
	     // Paso el par�metor al hilo paralelo
	     or.execute(_pref_area, _pref_telefono);
	}
	
	// ------------------------------------------
	// Guardar preferencia de �ltima pantalla
	// ------------------------------------------
	private void Guardar_preferencia_de_pantalla(String pantalla) {
			
		try
		{
			SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

			// crear el editor de preferencias
			SharedPreferences.Editor editor = preferencias.edit();

			// Agregar como preferencia el telefono
			editor.putString("ultimaPantalla", pantalla);

			editor.apply();
		}
		catch (Exception ex)
		{

		}

		return;
	}
	
	// ------------------------------------------
	// Realizar todos los findViews de los componentes a usar
    // ------------------------------------------
	private void finds_views() {
		
		PackageInfo info;
		PackageManager manager = this.getPackageManager();
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			
			_version = "" + info.versionName;
		} catch (NameNotFoundException e) {
			Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
			finish();
		}

        sp = (Spinner)findViewById(R.id.spinner1);
        sp.setBackgroundColor(getResources().getColor(R.color.gris_campos));

        ArrayAdapter<String> adapter_state  = new ArrayAdapter<String>(this, R.layout.layout_spinner, localidades);
        //ArrayAdapter<String> adapter_state  = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, localidades);
        sp.setAdapter(adapter_state);
        
        if (sp.getCount() < 2)
        	sp.setEnabled(false);
        
		// botones
        button_confirmar = (Button)findViewById(R.id.button_confirmar);
		button_favoritos = (Button)findViewById(R.id.button_favorito);
		button_Teclado_Esquina = (ImageButton)findViewById(R.id.imageButton_esquina);
		button_Teclado_Calle = (ImageButton)findViewById(R.id.imageButton_calle);
		button_Mapa = (ImageButton)findViewById(R.id.imageButtonMapa);
		
		// edit texts
		// editText_localidad = (EditText)findViewById(R.id.editText_localidad);
	    editText_calle = (EditText)findViewById(R.id.EditText_calle);
	    editText_altura = (EditText)findViewById(R.id.EditText_altura);
	    editText_esquina = (EditText)findViewById(R.id.EditText_esquina);

	    // Layout
	    layoutCalle = (LinearLayout)findViewById(R.id.linearLayoutCalle);
	    layoutAltura = (LinearLayout)findViewById(R.id.linearLayoutAltura);
	    layoutEsquina = (LinearLayout)findViewById(R.id.linearLayoutEsquina);
	    layoutLocalidad = (LinearLayout)findViewById(R.id.linearLayoutLocalidad);
	    
	    if (_hCalle != null) 
	    	editText_calle.setText(_hCalle);
	    
		//editText_localidad.setText(_hLocalidad);
	    if (_hAltura != null)   
	    {
	    	if (_hAltura.length() > 0 )
	    		if (Integer.parseInt(_hAltura) > 0)
	    			editText_altura.setText(_hAltura);
	    }
	    
	    if (_hEsquina != null)    
	    	editText_esquina.setText(_hEsquina);
 
	    if (_hLocalidad != null)
		    if (Boolean.parseBoolean(getString(R.string.utilizaLocalidad)))
		    	sp.setSelection(((ArrayAdapter<String>)sp.getAdapter()).getPosition(_hLocalidad));
	    
	    // Control para ver si muestra el bot�n calle/esqunia
	    button_Teclado_Calle.setVisibility(View.VISIBLE);
	    button_Teclado_Esquina.setVisibility(View.INVISIBLE);

	    editText_calle.setInputType(Boolean.parseBoolean(getResources().getString(R.string.campoCalleEsquina_es_Texto)) ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
	    editText_esquina.setInputType(Boolean.parseBoolean(getResources().getString(R.string.campoCalleEsquina_es_Texto)) ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
		//(ImageButton)findViewById(R.id.ImageButton_helpCalle);
	    if ( Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
	    {
	    	button_Teclado_Calle.setImageResource( R.drawable.help );
	    	button_Teclado_Esquina.setImageResource( R.drawable.help);
	    }
	    else
	    {
	    	button_Teclado_Calle.setImageResource(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
	    	button_Teclado_Esquina.setImageResource(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
	    }
	}
	
    // ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void event_create() {
			
		// Bot�n mapa
		button_Mapa.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				
				if (!_puedeUsarGPS)
				{
						Toast.makeText(MainActivity.this, "Habilite los accesos al GPS desde Permisos de aplicaci�n", Toast.LENGTH_LONG).show();
//						
//						Intent intent = new Intent();
//						intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//						Uri uri = Uri.fromParts("package", _packageName, null);
//						intent.setData(uri);
//						startActivity(intent);
//						
//						finish();
						return;
				}
				else
				{
					// Verificar si tiene el GPS Encencido, sino ofrecer encender
					if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
					{
							Intent intento = new Intent(getApplicationContext(), MapActivity.class);
							intento.putExtra("telefono", _pref_telefono);
							intento.putExtra("area", _pref_area);
							intento.putExtra("verificaViaje", "false");
							startActivity(intento);
					}
					else
					{
							builder.setMessage("Abrir configuraci�n para activar el GPS.")
									.setTitle("Sin GPS Activado")
									.setPositiveButton("Abrir", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
													startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
													dialog.cancel();
											}})
									.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog, int id) {
															dialog.cancel();
												}
								});
								builder.create();
								builder.show();
					}
				}
			}
		});
		
		// Bot�n solicitar
        button_confirmar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				//1ro ocultar teclado
				Funciones.ocultarTeclado(getApplicationContext(), v);
				
				// Control de textos, si hay algo escrito en calle
				if (editText_calle.getText().length() <=0)
				{
					Toast toast;
					toast = Toast.makeText(MainActivity.this, R.string._err_sin_calle, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, editText_calle.getTop());
					toast.show();
					return;
				}
				
				// Control de textos, si hay algo escrito en altura o esquina
				if (editText_altura.getText().length() <=0 && editText_esquina.getText().length() <= 0)
				{
					Toast toast;
					toast = Toast.makeText(MainActivity.this, R.string._err_sin_nro_o_inter, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, editText_calle.getTop());
					toast.show();
					return;
				}

				if (!Funciones.hayInternet(getApplicationContext()))
                {
                    AlertDialog ad = new AlertDialog.Builder(MainActivity.this).create();
                    ad.setTitle(R.string._error);
                    ad.setCancelable(false);

                    //ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._err_sin_internet));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                    });

                    ad.show();
                }
				else
				{
                    obtenerResultado or = new obtenerResultado();
                    // Paso el par�metor al hilo paralelo
                    or.execute(_pref_area,  _pref_telefono, generar_texto_a_enviar(), generar_texto_a_mostrar(), _version, ""+ sp.getSelectedItem());
				}
			}
		    
			// Genera el texto a enviar al webservice
			private String generar_texto_a_enviar() {
				String _rta = "";
				
				// escribo el  nombre de la calle
				_rta = editText_calle.getText().toString().trim() ;
				if (editText_altura.getText().length() > 0)
					_rta += " " + editText_altura.getText().toString().trim();
				if (editText_esquina.getText().length() > 0)
					_rta +=  " y " + editText_esquina.getText().toString().trim();
				
				return _rta;
			}
			 
			// Genera el texto a enviar al webservice
			private String generar_texto_a_mostrar() {
				String _rta = "";
				
				// escribo el  nombre de la calle
				_rta = editText_calle.getText().toString().toUpperCase().trim() ;
				if (editText_altura.getText().length() > 0)
					_rta += " N� " + editText_altura.getText().toString().trim();
				if (editText_esquina.getText().length() > 0)
					_rta +=  " y " + editText_esquina.getText().toString().toUpperCase().trim();
				
				return _rta;
			}
		});
		
		// Bot�n favoritos, por ahora solo muestra el nro de tel�fono 
		button_favoritos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intento = new Intent(getApplicationContext(), Activity_favoritas.class);
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area);
				//finish();
				startActivity(intento);
			}
		});
		
		// Bot�n calle
		button_Teclado_Calle.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (! Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
				{
					editText_calle.setInputType(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
					button_Teclado_Calle.setImageResource(editText_calle.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
				}
				else
				{
					// Aca se llama al encontrar nombre de calle
					Intent intento  = new  Intent(MainActivity.this, AyudaCalle.class);
					intento.putExtra("telefono", _pref_telefono);
					intento.putExtra("area", _pref_area);
					intento.putExtra("calleNro", "1");
					intento.putExtra("calle", editText_calle.getText().toString());
					intento.putExtra("altura", editText_altura.getText().toString());
	     	    	intento.putExtra("esquina", editText_esquina.getText().toString());
					intento.putExtra("localidad", "" + sp.getSelectedItem());
	     	    	intento.putExtra("comentario", _hComentario);
	     	    	intento.putExtra("prellenado", "" + editText_calle.getText());
	     	    	
	     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	     	    	
					startActivity(intento);
					finish();
				}
			}
		});
		
		// Bot�n esquina
		button_Teclado_Esquina.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (! Boolean.parseBoolean(getResources().getString(R.string.BotonCalleEsquina_muestraAyuda)))
				{
					editText_esquina.setInputType(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
					button_Teclado_Esquina.setImageResource(editText_esquina.getInputType() == InputType.TYPE_CLASS_NUMBER ? R.drawable.aabc_16 : R.drawable.n123_16);
				}
				else
				{
					Intent intento  = new  Intent(MainActivity.this, AyudaCalle.class);;
					intento.putExtra("telefono", _pref_telefono);
					intento.putExtra("area", _pref_area);
					intento.putExtra("calleNro", "2");
					intento.putExtra("calle", editText_calle.getText().toString());
					intento.putExtra("altura", editText_altura.getText().toString());
	     	    	intento.putExtra("esquina", editText_esquina.getText().toString());
					intento.putExtra("localidad", "" + sp.getSelectedItem());
	     	    	intento.putExtra("comentario", _hComentario);
	     	    	intento.putExtra("prellenado", "" + editText_esquina.getText());

	     	    	intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	     	    	
					startActivity(intento);
					finish();
				}
			}
		});
		
		//Presionar una tecla en altura borra esquina 
		editText_altura.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				if (editText_altura.getText().length() > 0)
//					if (editText_esquina.getText().length() > 0)
//						editText_esquina.setText("");
			}

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {

			}
		});
		
		// Presionar una tecla en esquina borra altura
		editText_esquina.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				if (editText_esquina.getText().length() > 0)
//					if (editText_altura.getText().length() > 0)
//						editText_altura.setText("");
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			
			}
			
			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		
		// Autocompletar de calle ----------------------
	    AutoCompleteTextView autoComplete_edit = (AutoCompleteTextView)findViewById(R.id.EditText_calle);
	    Autocompletar adapter = new Autocompletar(getApplicationContext(), sp.getSelectedItem().toString());
	        
        autoComplete_edit.setAdapter(adapter);
        autoComplete_edit.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
        {
//	        	Log.d("Autocomplete", "Seleccione algo: "+arg1.getTag().toString());
//	        	bandera = true;

        	editText_altura.requestFocus();
        }
        });
        
        autoComplete_edit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
//					bandera = false;
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
        
		// Autocompletar de esquina ----------------------
	    AutoCompleteTextView autoComplete_Esquina = (AutoCompleteTextView)findViewById(R.id.EditText_esquina);
	    Autocompletar adapterEsquina = new Autocompletar(getApplicationContext(), sp.getSelectedItem().toString());
	        
	    autoComplete_Esquina.setAdapter(adapterEsquina);
	    autoComplete_Esquina.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
        {
//	        	Log.d("Autocomplete", "Seleccione algo: "+arg1.getTag().toString());
//	        	bandera = true;
        	//Toast.makeText(getApplicationContext(), ((TextView)arg1).getText(), Toast.LENGTH_SHORT).show();
 //       	editText_comentarios.requestFocus();
        }
        });
        
        autoComplete_edit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});

        // Al entrar a calle, altura, esquina o mensaje cambiar el color de fondo, poner a todos color fondo y al que entr� colo gris
        editText_calle.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutAltura.setBackgroundResource(R.color.fondo_oscuro);
					layoutCalle.setBackgroundResource(color.gris_campos);
					layoutEsquina.setBackgroundResource(color.fondo_oscuro);
					layoutLocalidad.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Esquina.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Calle.setBackgroundResource(color.gris_campos);
				    button_Teclado_Esquina.setVisibility(View.INVISIBLE);
				    button_Teclado_Calle.setVisibility(View.VISIBLE);
				}
			}
		});
        
        editText_altura.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutAltura.setBackgroundResource(R.color.gris_campos);
					layoutCalle.setBackgroundResource(color.fondo_oscuro);
					layoutEsquina.setBackgroundResource(color.fondo_oscuro);
					layoutLocalidad.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Esquina.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Calle.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Esquina.setVisibility(View.INVISIBLE);
				    button_Teclado_Calle.setVisibility(View.INVISIBLE);
				}
			}
		});

        editText_esquina.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
				{
					layoutAltura.setBackgroundResource(R.color.fondo_oscuro);
					layoutCalle.setBackgroundResource(color.fondo_oscuro);
					layoutEsquina.setBackgroundResource(color.gris_campos);
					layoutLocalidad.setBackgroundResource(color.fondo_oscuro);
					button_Teclado_Esquina.setBackgroundResource(color.gris_campos);
					button_Teclado_Calle.setBackgroundResource(color.fondo_oscuro);
				    button_Teclado_Calle.setVisibility(View.INVISIBLE);
				    button_Teclado_Esquina.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	// Otros Clases -----------------------------------------------------------------------------------------------------------------------------
	
    // ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class obtenerResultado extends AsyncTask<String, Void, Respuesta> {

    	ProgressDialog pd = new ProgressDialog(MainActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeSolitud));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {
		
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivity.this);
			
			// Los favoritos se guardan en Confirmacion JAVA

			Respuesta _rta;
			
			_rta = cw.GuardarMsg_obtRespuesta("" + _texto[0], "" + _texto[1], "" + _texto[2], "" + _texto[3], "" + _texto[4], "" + _texto[5]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

            if (resultado.getError_conexion())
            {
                AlertDialog ad = new AlertDialog.Builder(MainActivity.this).create();
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString( R.string._errorConexionWS));
                ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                ad.show();
            }
            else
            {
                // Control de versi�n de aplicaci�n
                if (resultado.getVersionIncorrecta())
                {
                    if (resultado.getActualizacionObligatoria())
                    {
                        Intent intento = new Intent(MainActivity.this, Activity_version_obsoleta.class);
                        intento.putExtra("direccion", resultado.getURI());
                        startActivity(intento);
                        finish();
                    }
                    else
                    {
                        Toast tostada = Toast.makeText(MainActivity.this, R.string._act_version_antigua, Toast.LENGTH_LONG);
                        tostada.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        tostada.show();
                    }
                }

                if (resultado.getServicioOffLine())
                {
                    Intent intento = new Intent(MainActivity.this, Estado_servicio.class);
                    intento.putExtra("telefono", _pref_telefono);
                    intento.putExtra("area", _pref_area);
                    startActivity(intento);
                    return;
                }

                if (resultado.getPedirConfirmacion())
                {
                    Intent intento = new Intent(MainActivity.this, Confirmacion.class);

                    intento.putExtra("direccion", resultado.getTexto().toString());
                    intento.putExtra("calle1", resultado.getCalle1().toString());
                    intento.putExtra("calle2", resultado.getCalle2().toString());
                    intento.putExtra("localidad", "" + sp.getSelectedItem());
                    intento.putExtra("telefono", _pref_telefono);
                    intento.putExtra("area", _pref_area);

                    intento.putExtra("comentario", _hComentario);
                    intento.putExtra("altura", editText_altura.getText().toString());
                    intento.putExtra("textoAliasMovil", resultado.getDireccion());
                    intento.putExtra("abrLocalidad", resultado.getAbrLocalidad());

                    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intento);
                    finish();
                }
                else
                {
                    if (resultado.getDireccionInvalida()
                            ||
                        resultado.getCallesAlternativas())
                    {
                        Intent intento = new Intent(MainActivity.this, ErrorActivity.class);
                        intento.putExtra("mensaje", resultado.getTexto().toString());
                        startActivity(intento);
                    }
                    else
                    {
                        if (resultado.getViajeReciente())
                        {
                            Intent intento = new Intent(MainActivity.this, ConViajeActivity.class);
                            intento.putExtra("esmapa", "0");
                            intento.putExtra("mensaje", resultado.getTexto().toString());
                            intento.putExtra("telefono", _pref_telefono);
                            intento.putExtra("area", _pref_area);
                            intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intento);
                            finish();
                        }
                    }
                }
            }

			super.onPostExecute(resultado);
		}
	}

    // ------------------------------------------
    // clase para obtener viaje reciente en forma asincr�nica
    // ------------------------------------------
    public class obtenerViajeReciente extends AsyncTask<String, Void, Respuesta>
	{
    	String _NroTelefono;
    	ProgressDialog pd = new ProgressDialog(MainActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivity.this);
			
			Respuesta _rta;
			_NroTelefono = "" + _texto[0];
			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado)
        {
			if (pd.isShowing() || pd != null)
				pd.dismiss();

            AlertDialog ad = new AlertDialog.Builder(MainActivity.this).create();
            ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if (resultado.getError_conexion())
            {
                ad.setTitle(R.string._error);
                ad.setCancelable(false);
                ad.setMessage(getResources().getString( R.string._error_internet));
                ad.show();
            }
            else
            {
                if (resultado.getNoHayPedidoRealizado())
                {
                    ad.setTitle(R.string._error);
                    ad.setCancelable(false);
                    ad.setMessage(getResources().getString( R.string._vr_No_hay_viaje));
                    ad.show();
                }
                else
                {
                    if (resultado.getNegativo())
                    {
                        ad.setTitle(R.string._error);
                        ad.setCancelable(false);
                        ad.setMessage(getResources().getString( R.string._vr_ViajeNegativo));
                        ad.show();
                    }
                    else
                    {
                        if (resultado.getYaOcupado())
                        {
                            ad.setTitle(R.string._error);
                            ad.setCancelable(false);
                            //	    	 ad.setIcon(R.drawable.icono);
                            ad.setMessage(getResources().getString( R.string._vr_ViajeOcupado));

                            ad.show();
                        }
                        else
                        {
                            if (resultado.getNroViaje() > 0)
                            {
                                Intent intento = new  Intent(MainActivity.this,ConViajeActivity.class);
                                intento.putExtra("esmapa", "0");
                                intento.putExtra("mensaje", resultado.getTexto().toString());
                                intento.putExtra("telefono", _pref_telefono);
                                intento.putExtra("area", _pref_area);
                                if (resultado.getTieneMovil())
                                    intento.putExtra("movil", resultado.getMovil());
                                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intento);
                            }
                        }
                    }
                }
            }

          	super.onPostExecute(resultado);
		}
	}
    
    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class tieneUnViajeReciente extends AsyncTask<String, Void, Respuesta>
	{
    	String _NroTelefono;
    	ProgressDialog pd = new ProgressDialog(MainActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MainActivity.this);
			
			Respuesta _rta;
			_NroTelefono = "" + _texto[0];
			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			//textView_resultado.setText(result);
			
			//textView_resultado.setVisibility(View.VISIBLE);

			AlertDialog ad = new AlertDialog.Builder(MainActivity.this).create();
			ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			if (resultado.getNroViaje() > 0 && !resultado.getYaOcupado() && !resultado.getNegativo() && !resultado.getYaInformoMovil())
			{
				Intent intento = new  Intent(MainActivity.this, ConViajeActivity.class);
				intento.putExtra("esmapa", "0");
				intento.putExtra("mensaje", resultado.getTexto().toString());
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area);
				intento.putExtra("vinoAutomatico", true);

				if (resultado.getTieneMovil())
					intento.putExtra("movil", resultado.getMovil());
				
				intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				startActivity(intento);
				finish();
			}

			super.onPostExecute(resultado);
		}
	}
}