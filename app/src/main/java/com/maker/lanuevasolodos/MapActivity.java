package com.maker.lanuevasolodos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.OverlayItem;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class MapActivity extends Activity {

	// ------------------------------------------
	// Declaraci�n de variables de preferencias
    // ------------------------------------------
	String _pref_telefono,  _pref_area, _version;
	
	// ------------------------------------------
	// Variables de vista
	// ------------------------------------------
	EditText txtAyuda;
	TextView txtBuscandoGPS, txtPresioneParaVer;
	ImageButton btnVolver, btnBrujula, btnAbajo, btnArriba;
	Button btnConfirmar;	
	MapView map;
	
	// ------------------------------------------
	// Variables para localizacion
	// ------------------------------------------
	RelativeLayout relMap;
	LocationManager locManager;
	Location ubicacion;
	LocationListener locationListener;
	String proveedorPosicion, proveedorPosicionTMP;
	float orientacionMapa = 0;
	boolean yaCentro, yaPusoGuia;
	
	// ------------------------------------------
	// Variables comunes
	// ------------------------------------------
	String _NroTelefono, _Area;
	Boolean _VerificarViaje;

	//static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;
	static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		if (Build.VERSION.SDK_INT >= 23)
//		{
//			//int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
//			
//			if (ContextCompat.checkSelfPermission(this,	 Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//				// Al comentar hace que si no tiene el permiso siempre lo vuelva a pedir
////	            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
////	            	 ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
////
////	            } else {
//
//	                    ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//	 
////	            }
//	        }
//		}
		
		setContentView(R.layout.activity_map);
		
		yaCentro = false;
		yaPusoGuia= false;
		_VerificarViaje = true;

		try
		{
			//para marshmallow
			configurarMapa();

			obtenerPrimeraUbicacion();
			
			Guardar_preferencia_de_pantalla("MAP");
			
			PackageInfo info;
			PackageManager manager = this.getPackageManager();
			try {
				info = manager.getPackageInfo(this.getPackageName(), 0);
				
				_version = "" + info.versionName;
			} catch (NameNotFoundException e) {
				Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
				finish();
			}
			  
			// Obtener preferencias
			if (getIntent().getExtras() != null)
			{
				if (getIntent().getExtras().getString("telefono") != null)
				{
					_pref_telefono = getIntent().getExtras().getString("telefono");
					_pref_area = getIntent().getExtras().getString("area");
				}
				if (getIntent().getExtras().getString("verificaViaje") != null)
				{
					_VerificarViaje = false;
				}
			}
			
			if (ubicacion == null)
			{
				try {
					proveedorPosicionTMP = LocationManager.NETWORK_PROVIDER;
					ubicacion = locManager.getLastKnownLocation(proveedorPosicion);

					if (ubicacion == null) {
						Toast.makeText(MapActivity.this, R.string._suv_error_de_inicio, Toast.LENGTH_SHORT).show();
						iniciarActivityClasica();
					}
				} catch (SecurityException e) {
					Toast.makeText(MapActivity.this, R.string._suv_error_de_inicio, Toast.LENGTH_SHORT).show();
					iniciarActivityClasica();
				}
			}

			final GeoPoint geoPoint = new GeoPoint(ubicacion.getLatitude(), ubicacion.getLongitude()); 
			
			final ViewTreeObserver vto = map.getViewTreeObserver();
			
			vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { 
				@Override public void onGlobalLayout() 
				{
					if (!yaCentro)
					{
						map.getController().setCenter(geoPoint);
						yaCentro = true;
					}
				}
			}
			);
			
			if (getIntent().getExtras().getString("telefono") != null)
			{
				_pref_telefono = getIntent().getExtras().getString("telefono");
				_pref_area= getIntent().getExtras().getString("area");
			}
			
			// Esconder brujula
			btnBrujula = (ImageButton)findViewById(R.id.imageButtonGPSLoca);
			btnBrujula.setEnabled(false);
			//btnBrujula.setVisibility(View.INVISIBLE);
			
	
			createViews();
			
			crearEventos();
			
			if (_VerificarViaje)
				verificarSiTieneViajePedido();
			

			
			MostrarMapa();
			
		}
		catch (Exception ex)
		{
		//	Toast.makeText(MapActivity.this, R.string._suv_error_de_inicio, Toast.LENGTH_SHORT).show();
			iniciarActivityClasica();
		}

	}

//	@Override
//	public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
//	        switch (requestCode) {
//	            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//	                // If request is cancelled, the result arrays are empty.
//	                if (grantResults.length > 0
//	                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//	 
//	                } else {
//	 
//	                }
//	                return;
//	            }
//	        }
//	    }
	
	// ------------------------------------------
	// Sobreescribir por si toca la tecla atras y termina saliendo, que apague el escuchador de gps
	// ------------------------------------------
	@Override
	public void onBackPressed() {
		
		locManager.removeUpdates( locationListener);
		finish();
		//super.onBackPressed();
	}

	// ------------------------------------------
	// Control para ver si tiene un viaje pedido
    // ------------------------------------------
	private void verificarSiTieneViajePedido() {
		 tieneUnViajeReciente or = new tieneUnViajeReciente();
	     // Paso el par�metor al hilo paralelo
	     or.execute(_pref_area, _pref_telefono);
	}
	
	// ------------------------------------------
	// Guardar preferencia de �ltima pantalla
	// ------------------------------------------
	private void Guardar_preferencia_de_pantalla(String pantalla) {
			
			try
			{
				SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
				

				// crear el editor de preferencias
				SharedPreferences.Editor editor = preferencias.edit();
				 
				// Agregar como preferencia el telefono
				editor.putString("ultimaPantalla", pantalla);

				editor.apply();
			}
			catch (Exception ex)
			{
				
			}
			return;
		
	}


	// ----------------------------------
	// Obtener primera ubicacion
	// ----------------------------------
	private void obtenerPrimeraUbicacion() {

		try {
			iniciarEscuchaGPS();

			proveedorPosicion = LocationManager.PASSIVE_PROVIDER;

			ubicacion = locManager.getLastKnownLocation(proveedorPosicion);
		} catch (SecurityException e) {
			Toast.makeText(MapActivity.this, R.string._suv_error_de_inicio, Toast.LENGTH_SHORT).show();
			iniciarActivityClasica();
		}
	}

	// ------------------------------
	// Obtener fecha y hora actual
	// --------------------------------
	public static String getCurrentTimeStamp(){
	    try {

	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        String currentTimeStamp = dateFormat.format(new Date()); // Find todays date

	        return currentTimeStamp;
	    } catch (Exception e) {
	        e.printStackTrace();

	        return null;
	    }
	}
	
	// ------------------------------
	// Iniciar escucha GPS
	// ------------------------------
	private void iniciarEscuchaGPS() {
		
		try
		{
			locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

			locationListener = new LocationListener() {
	            @Override
	            public void onLocationChanged(Location location) {
					if (!yaPusoGuia)
					{
						yaPusoGuia = true;
						btnBrujula.setEnabled(true);
						btnBrujula.setImageDrawable(getResources().getDrawable(R.drawable.gps32verde));
						txtPresioneParaVer.setVisibility(View.VISIBLE);
						txtBuscandoGPS.setVisibility(View.INVISIBLE);
						//PonerGuiaLocalizacionGPS();
					}
					ubicacion = location;
					btnBrujula.setVisibility(View.VISIBLE);
	            }
	            @Override
	            public void onProviderDisabled(String provider) {
	            	Toast.makeText(MapActivity.this, R.string._map_SinPosicionGPS, Toast.LENGTH_SHORT).show();
	            	iniciarActivityClasica();
	            }
	            @Override
	            public void onProviderEnabled(String provider) {
	                // TODO Auto-generated method stub
	            }
	            @Override
	            public void onStatusChanged(String provider, int status,
	                    Bundle extras) {
	                // TODO Auto-generated method stub
	            }           
	        };
		 
	        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
		}
		catch (SecurityException ex)
		{
			Toast.makeText(MapActivity.this, R.string._map_ErrorEnGPS, Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	// ------------------------------
	// Eventos de botones, etc
	// ------------------------------
	private void crearEventos() {
		
		// toca la brujula
		btnBrujula.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				  txtPresioneParaVer.setVisibility(View.INVISIBLE);
				  MostrarMapa();
			}
		});
		
		// Girar el mapa para abajo
		btnAbajo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				orientacionMapa += 12;
				if (orientacionMapa >= 360)
					orientacionMapa = 0;
				map.setMapOrientation(orientacionMapa);
			}
		});
		
		// Girar el mapa para arriba
		btnArriba.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				orientacionMapa -= 12;
				if (orientacionMapa <= 12)
					orientacionMapa = 360;
				map.setMapOrientation(orientacionMapa);
			}
		});
		
		// boton para volver al clasico
		btnVolver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				locManager.removeUpdates( locationListener);
				iniciarActivityClasica();
			}
		});
		
		// Confirmacion
		btnConfirmar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				
				if (!Funciones.hayInternet(MapActivity.this))
				{
					AlertDialog ad = new AlertDialog.Builder(MapActivity.this).create();
					ad.setTitle(R.string._error);
					ad.setCancelable(false);

					//		    	 ad.setIcon(R.drawable.icono);
					ad.setMessage(getResources().getString( R.string._err_sin_internet));
					ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();

						}
					});
			    	 
					ad.show();
				}
				else
				{
					resoverDireccion rd = new resoverDireccion();
					rd.execute(("" + map.getMapCenter().getLatitude()).replace(",", "."), ("" + map.getMapCenter().getLongitude()).replace(",", "."));
				}
			}
		});
	}

	// -----------------------------
	// Iniciar la activity cl�sica
	// ----------------------------
	private void iniciarActivityClasica()
	{
		Intent intento = new  Intent(MapActivity.this,MainActivity.class);
		intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intento.putExtra("telefono", _pref_telefono);
		intento.putExtra("area", _pref_area);
		intento.putExtra("verificaViaje", "false");
		startActivity(intento);
		finish();
	}
	
	// -----------------------------
	// Configurar mapa
	// ----------------------------
	@SuppressWarnings("deprecation")
	private void configurarMapa() {

		// Location Manager para obtener el mejor proveedor de ubicaci�n
		locManager = (LocationManager)getSystemService(LOCATION_SERVICE);

		// Relative layout de mapa
		relMap = (RelativeLayout)findViewById(R.id.relMap);
		
		// Mapa
		map = (MapView) findViewById(R.id.mapView);
        map.setTileSource(TileSourceFactory.MAPNIK);
        
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.setClickable(true);
        map.setAlwaysDrawnWithCacheEnabled(true);
	}
	
	// --------------------------------------------
	// crarm menu
	// --------------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		Intent intento;
		
		switch (id) {
		case R.id.action_ayuda:
			intento = new Intent(getApplicationContext(), Activity_help.class);
			startActivity(intento);
			break;
		case R.id.action_estado:
			intento = new Intent(getApplicationContext(), Estado_servicio.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area );
			startActivity(intento);
			break;
		case R.id.action_contacto:
			intento = new Intent(getApplicationContext(), ContactoActivity.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area);
			startActivity(intento);
			break;
		case R.id.action_acercade:
			intento = new Intent(getApplicationContext(), AcercaDeActivity.class);
			startActivity(intento);
			break;
		case R.id.action_ultimoviaje:
			intento = new Intent(getApplicationContext(), DatosUltimoViajeActivity.class);
			intento.putExtra("telefono", _pref_telefono);
			intento.putExtra("area", _pref_area );
			intento.putExtra("deDonde", "map" );
			startActivity(intento);
			break;
		default:
			return true;
		}
	
		return super.onOptionsItemSelected(item);
	}
	
	// --------------------------------------------
	// Crear vistas
	// --------------------------------------------
	private void createViews() {

		relMap.setVisibility(View.INVISIBLE);
		// botones
		btnConfirmar = (Button)findViewById(R.id.button_confirmarMap);
		btnVolver = (ImageButton)findViewById(R.id.imageButtonVolverMap);
		btnAbajo =(ImageButton)findViewById(R.id.imageButton_ArrowDown);
		btnArriba = (ImageButton)findViewById(R.id.imageButton_ArrowUp);

		txtBuscandoGPS = (TextView)findViewById(R.id.txtBuscandoGPS);
		txtBuscandoGPS.setVisibility(View.VISIBLE);
		
		txtPresioneParaVer =(TextView)findViewById(R.id.txtPresioneParaVer);
		txtPresioneParaVer.setVisibility(View.INVISIBLE);
	}
	
	// --------------------------------------------
	// 	Poner gu�a de localizaci�n GPS
	// --------------------------------------------
	private void PonerGuiaLocalizacionGPS()
	{
		GeoPoint geoPoint = new GeoPoint(ubicacion.getLatitude(), ubicacion.getLongitude()); 
//		
//	     ArrayList<OverlayItem> arregloDeOverlays;
//	       arregloDeOverlays = new ArrayList<OverlayItem>();
//	        
//	       arregloDeOverlays.add(new OverlayItem("", "", geoPoint));
//	       
//	       //marker perzonalizado
//	     //  @SuppressWarnings("deprecation")
//		  //Drawable marker = toBitmapDrawable(getResources().getDrawable(R.drawable.dotblue));
//	       
//	       // Si hay overlays, borrarlos
//	       if (!map.getOverlays().isEmpty())
//	       	map.getOverlays().clear();
//	 
//	       ItemizedIconOverlay<OverlayItem> overlayAAgregar = new ItemizedIconOverlay<>(arregloDeOverlays, marker, ListenerClickMarker, new DefaultResourceProxyImpl(getApplicationContext()));
//	       
//	    //   ItemizedOverlayWithFocus<OverlayItem> overlayAAgregar2 = new ItemizedOverlayWithFocus<OverlayItem>(this, arregloDeOverlays, ListenerClickMarker);
//	       map.getOverlays().add(overlayAAgregar);
	}
	
	// --------------------------------------------
	// 	Muestra mapa, ubica m�vil y completa datos 
	// --------------------------------------------
	private void MostrarMapa() {

		if (ubicacion == null)
		{
			Toast.makeText(MapActivity.this, getResources().getString(R.string._map_NoHayPosicion), Toast.LENGTH_SHORT).show();
			return;
		}
		
		// centrar y poner zoom
		IMapController mapController = map.getController();
		
		GeoPoint geoPoint = new GeoPoint(ubicacion.getLatitude(), ubicacion.getLongitude()); 

		map.setMapOrientation(orientacionMapa);

       	// create an ArrayLista para poner el overlay que tiene el marker
  
	    mapController.setCenter(new IGeoPoint() {
				
				@Override
				public int getLongitudeE6() {

					int rta = 0;
					
					rta = ((int)(ubicacion.getLatitude() * 1000000));
					
					return rta;
				}
				
				@Override
				public double getLongitude() {
					return (double)ubicacion.getLongitude();
				}
				
				@Override
				public int getLatitudeE6() {
					
					int rta = 0;
					
					rta = ((int)(ubicacion.getLatitude() * 1000000));
					
					return rta;
				}
				
				@Override
				public double getLatitude() {

					return (double)ubicacion.getLatitude();
				}
			});
	       

		   mapController.setZoom(18);
	       map.getController().setCenter(geoPoint);

		 
		   relMap.setVisibility(View.VISIBLE);

	}

	
    // ------------------------------
	// Convertir un dibujo a icono
	// ------------------------------
	private Drawable toBitmapDrawable(Drawable anyDrawable) {
		 Bitmap bmp = Bitmap.createBitmap(anyDrawable.getIntrinsicWidth(), anyDrawable.getIntrinsicHeight(), Config.ARGB_8888); 
         Canvas canvas = new Canvas(bmp);
         anyDrawable.setBounds(0, 0, anyDrawable.getIntrinsicWidth(), anyDrawable.getIntrinsicHeight());
         anyDrawable.draw(canvas);
         return new BitmapDrawable(getResources(), bmp);
	}

    // ------------------------------
	// Click sobre punto azul
	// ------------------------------
	OnItemGestureListener<OverlayItem> ListenerClickMarker = new OnItemGestureListener<OverlayItem>() {
		 
	     @Override
	     public boolean onItemLongPress(int arg0, OverlayItem arg1) {
	         // TODO Auto-generated method stub
	         return false;
	     }

		@Override
		public boolean onItemSingleTapUp(int arg0, OverlayItem arg1) {
			// TODO Auto-generated method stub
			return false;
		}
	};
	
	
	// Clase para resolver la direccion de una latitud y longitud
	public class resoverDireccion extends AsyncTask<String , Void, CALLE_ALTURA>	{
		 
        String _latitud , _longitud;
			
	 	ProgressDialog pd = new ProgressDialog(MapActivity.this);
	 	
        // Se ejecuta antes de llamar a la tarea
        @Override
        protected void onPreExecute() {
            pd.setMessage(getResources().getString(R.string._map_ResolviendoDireccion));
            pd.show();
            pd.setCancelable(false);

            super.onPreExecute();
        }
			
        // Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
        @Override
        protected CALLE_ALTURA doInBackground(String... _texto) {

            CALLE_ALTURA _rta = new CALLE_ALTURA();

            conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MapActivity.this);

            _latitud = "" + _texto[0];
            _longitud = "" + _texto[1];

            _rta = cw.ResolverDireccion("" + _texto[0], "" + _texto[1], _version);

            return _rta;
        }
		
        // Una vez que termino
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(CALLE_ALTURA resultado) {

            if (pd.isShowing() || pd != null)
                pd.dismiss();

            if (!resultado.isHayError())
            {
                // Control de versi�n de aplicaci�n
                if (resultado.isVersionIncorrecta())
                {
                    if (resultado.isActualizacionObligatoria())
                    {
                        Intent intento = new Intent(MapActivity.this, Activity_version_obsoleta.class);

                        intento.putExtra("direccion", resultado.getURI());

                        startActivity(intento);

                        finish();
                    }
                    else
                    {
                        Toast tostada = Toast.makeText(MapActivity.this, R.string._act_version_antigua, Toast.LENGTH_LONG);
                        tostada.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        tostada.show();
                    }
                }

                if (resultado.isServicioOffLine())
                {
                    Intent intento = new Intent(MapActivity.this, Estado_servicio.class);

                    intento.putExtra("telefono", _pref_telefono);
                    intento.putExtra("area", _pref_area);

                    startActivity(intento);
                }

                locManager.removeUpdates(locationListener);

                if (resultado.getCalle().length() > 0){

                    Intent intento = new Intent(MapActivity.this, ConfirmacionMapa.class);

                    intento.putExtra("area", _pref_area);
                    intento.putExtra("telefono", _pref_telefono);
                    intento.putExtra("latitud", _latitud);
                    intento.putExtra("longitud", _longitud);
                    intento.putExtra("idlocalidad", resultado.getIdlocalidad());
                    intento.putExtra("localidad", resultado.getLocalidad());
                    intento.putExtra("calle", resultado.getCalle());
                    intento.putExtra("altura", resultado.getAltura());
                    intento.putExtra("desde", resultado.getDesde());
                    intento.putExtra("hasta", resultado.getHasta());
                    intento.putExtra("demora", resultado.getDemora());
                    intento.putExtra("zona", resultado.getZona());

                    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(intento);

                    finish();
                }
            }
            else
            {
				AlertDialog ad = new AlertDialog.Builder(MapActivity.this).create();
				ad.setTitle(R.string._error);
				ad.setCancelable(false);

				ad.setMessage(getResources().getString(R.string._map_err_sin_posicionvalida));
				ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				ad.show();
            }

            super.onPostExecute(resultado);
        }
    }
	
    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class tieneUnViajeReciente extends AsyncTask<String , Void, Respuesta>
	{
    	ProgressDialog pd = new ProgressDialog(MapActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._diag_aguardeViajeReciente));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MapActivity.this);
			
			Respuesta _rta;

			_rta = cw.Obtener_viaje_reciente("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			//textView_resultado.setText(result);
			
			//textView_resultado.setVisibility(View.VISIBLE);

			AlertDialog ad = new AlertDialog.Builder(MapActivity.this).create();
	    	ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
				}
			});
	    	 
			if (resultado.getNroViaje() > 0 && !resultado.getYaOcupado() && !resultado.getNegativo() && !resultado.getYaInformoMovil())
			{
				Intent intento = new  Intent(MapActivity.this, ConViajeActivity.class);

				intento.putExtra("esmapa", "1");
				intento.putExtra("mensaje", resultado.getTexto());
				intento.putExtra("telefono", _pref_telefono);
				intento.putExtra("area", _pref_area );
				intento.putExtra("vinoAutomatico", true);

				if (resultado.getTieneMovil())
					intento.putExtra("movil", resultado.getMovil());
				
				intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				startActivity(intento);
				finish();
			}

			super.onPostExecute(resultado);
		}
	}
}