package com.maker.lanuevasolodos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

public class RegistrationActivity extends Activity  {

	EditText editText_nroTelefono,  editText_area;
	Button button_Solicitar, button_Salir;
	LinearLayout linearLayout_Telefono;
	Boolean _habilitado;
	 
	// ------------------------------------------
	// Declaraci�n de variables de preferencias
    // ------------------------------------------
	String _pref_nro_telefono, _pref_localidad, _pref_area, _pref_telefonoCompleto, _ultimaPantalla;
	Integer _pref_cantidadPedidos;
 	SharedPreferences preferencias;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		
		//find views
		finds_views();

		HelloWorld hw = new HelloWorld();
		hw.execute();
		    
//		//crear eventos
//		event_create();
//
//		if (validaPreferencias(true))
//		{
//			editText_nroTelefono.setText(_pref_nro_telefono);
//			editText_area.setText(_pref_area);
//
//			Habilitar_ingreso_codigo(true);
//		}
//
//		editText_area.requestFocus();
	}
	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.R_action_acercade) {
			Intent intento = new Intent(getApplicationContext(), AcercaDeActivity.class);
			startActivity(intento);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
		
	// ------------------------------------------
	// Realizar todos los findViews de los componentes a usar
    // ------------------------------------------
	private void finds_views() {
		// botones
		button_Solicitar = (Button)findViewById(R.id.regbutton_Solicitar);
		button_Salir = (Button)findViewById(R.id.regbutton_salir);
		
		// edit texts
		editText_nroTelefono = (EditText)findViewById(R.id.reg_editText_nroTelefono);

		editText_area = (EditText)findViewById(R.id.editText_area);

		// Layouts
		linearLayout_Telefono = (LinearLayout)findViewById(R.id.regLinearLayout_telefono);
		
		// Si not tiene como preferencias entonces inhabilitar el layout
		// Si tiene el telefono habilitarlo pero deshabilitar el otro
		if (!validaPreferencias(false))
		{
			linearLayout_Telefono.setEnabled(true);
			button_Solicitar.setEnabled(true);
		}
		else
		{
			linearLayout_Telefono.setEnabled(false); 
		}
	}
	
    // ------------------------------------------
	// Verificar si hay preferencias, por ahora ser� el nro de tel�fono
    // la marca de habilitado y la localidad, ya que es Mar del Plata cableada
    // ------------------------------------------
    private boolean validaPreferencias(Boolean lanzarActivity) {

    	Boolean _rta = false, _pref_habilitado;
		
		try
		{
			preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
			_pref_nro_telefono = preferencias.getString("telefono", "");
			_pref_area = preferencias.getString("area", "");
			_pref_telefonoCompleto = preferencias.getString("telefonoCompleto", "");
			_ultimaPantalla =  preferencias.getString("ultimaPantalla", "ST");
					
			_pref_habilitado = preferencias.getBoolean("habilitado", false);
			
			if (_pref_nro_telefono.length() > 0)
				_rta = true;
			else
			{
				_rta = false;
				return _rta;
			}
			
			if (_pref_habilitado && lanzarActivity)
			{
				LocationManager locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
				// Si no tiene el gps encendido, directamente inicio la pantalla clasica
				
				if (! locManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && _ultimaPantalla.equals("MAP"))
				{
					Intent intento = new  Intent(RegistrationActivity.this,MainActivity.class);
					intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intento.putExtra("telefono", _pref_nro_telefono);
					intento.putExtra("area", _pref_area);
					startActivity(intento);
					
					if (Build.VERSION.SDK_INT < 23)
					{
						Toast.makeText(RegistrationActivity.this, R.string._map_GPSApagado, Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					if (_ultimaPantalla.equals("ST"))
					{
						//aca hay que revisar si apunta a comun o con esquina
						Intent intento = new  Intent(RegistrationActivity.this,MainActivity.class);
						intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intento.putExtra("telefono", _pref_nro_telefono);
						intento.putExtra("area", _pref_area);
						startActivity(intento);
					}
					else 
					{
						Intent intento = new  Intent(RegistrationActivity.this,MapActivity.class);
						intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intento.putExtra("telefono", _pref_nro_telefono);
						intento.putExtra("area", _pref_area);
						startActivity(intento);
					}
				}
				
				finish();
			}
		}
		catch (Exception ex)
		{
			_rta = false;
		}

		return _rta;
	}
    
    // ------------------------------------------
	// Verificar si hay preferencias, por ahora ser� el nro de tel�fono
    // la marca de habilitado y la localidad, ya que es Mar del Plata cableada
    // ------------------------------------------
    private boolean AgregarPreferencias(String _telefono, Boolean _habilitado) {
		Boolean _rta = false;
		
		try
		{
			preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
			//_pref_nro_telefono = preferencias.getString("prefTelefono", "");

			// crear el editor de preferencias
			SharedPreferences.Editor editor = preferencias.edit();
			 
			// Agregar como preferencia el telefono
			editor.putString("telefono", editText_nroTelefono.getText().toString());
			editor.putString("area", editText_area.getText().toString());
			editor.putString("telefonoCompleto", editText_area.getText().toString() + "15" + editText_nroTelefono.getText().toString());
			editor.putString("ultimaPantalla","MAP");
			
			// Agregar la preferencia _codigo con el valor pasado
			//if (_habilitado)
			editor.putBoolean("habilitado", _habilitado);

			editor.apply();
			 
			if (_habilitado)
			{
				Toast t = Toast.makeText(getApplicationContext(), getResources().getString(R.string._reg_autorizado), Toast.LENGTH_SHORT);
				t.setGravity(Gravity.CENTER, 0, 0);
				t.show();
				
				LocationManager locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
				// Si no tiene el gps encendido, directamente inicio la pantalla clasica
				
				if (! locManager.isProviderEnabled(LocationManager.GPS_PROVIDER) )
				{
					Intent intento = new  Intent(RegistrationActivity.this,MainActivity.class);
					intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intento.putExtra("telefono", editText_nroTelefono.getText().toString());
					intento.putExtra("area", editText_area.getText().toString());
					startActivity(intento);
					Toast.makeText(RegistrationActivity.this, R.string._map_GPSApagado, Toast.LENGTH_SHORT).show();
				}
				else
				{
					Intent intento = new  Intent(RegistrationActivity.this,MapActivity.class);
					intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intento.putExtra("telefono", editText_nroTelefono.getText().toString());
					intento.putExtra("area", editText_area.getText().toString());
					startActivity(intento);
				}

				finish();
			}
		}
		catch (Exception ex)
		{
			_rta = false;
		}
		
		return _rta;
	}
    
    // ------------------------------------------
	// Habilitar_ingreso_codigo
    // ------------------------------------------
	private void Habilitar_ingreso_codigo(Boolean _habilitar) {
		
		// Inhabilitar el text view de telefono
	    editText_nroTelefono.setEnabled(!_habilitar);
	    editText_area.setEnabled(!_habilitar);
	    
	    // Inhabilitar el bot�n de solocitar codigo
	    button_Solicitar.setText(_habilitar ? getString(R.string._reg_recibiraCodigo) : getString(R.string._reg_botonSolicitar));
	    	
	    button_Solicitar.setEnabled(!_habilitar);
	    
    	editText_nroTelefono.requestFocus();
	}
	    
    // ------------------------------------------
	// Crear los eventos
    // ------------------------------------------
	private void event_create() {
		
		// Bot�n Solicitar
		button_Solicitar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View vista) {
				if (editText_nroTelefono.length()< 5 || editText_area.length() < 2)
				{
					AlertDialog ad = new AlertDialog.Builder(RegistrationActivity.this).create();
					ad.setTitle(R.string._error);
					ad.setCancelable(false);

					//		    	 ad.setIcon(R.drawable.icono);
					ad.setMessage(getResources().getString( R.string._reg_nroTelefonoCorto));
					ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			    	 
					ad.show();
				}
				else
				{
					if (Funciones.hayInternet(RegistrationActivity.this))
					{
						AgregarPreferencias(""+editText_nroTelefono.getText(), true);
					}
					else
					{
						AlertDialog ad = new AlertDialog.Builder(RegistrationActivity.this).create();
						ad.setTitle(R.string._error);
						ad.setCancelable(false);

						//		    	 ad.setIcon(R.drawable.icono);
						ad.setMessage(getResources().getString( R.string._err_sin_internet));
						ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}

						});
				    	 
						ad.show();
					}
				}
			}
		});
		
		// Bot�n salir
		button_Salir.setOnClickListener(new OnClickListener() {
			
			public void onClick(View vista) {
				finish();
			}
		});
	}

	// ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class HelloWorld extends AsyncTask<String, Void, String>
	{
		// Se ejecuta antes de llamar a la tarea
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected String doInBackground(String... _texto) {

			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(RegistrationActivity.this);

			return cw.HelloWorld();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String resultado) {

			AlertDialog ad = new AlertDialog.Builder(RegistrationActivity.this).create();
			ad.setTitle(R.string._error);
			ad.setCancelable(false);

			ad.setMessage(resultado);
			ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			ad.show();

			super.onPostExecute(resultado);
		}
	}
	
//    // ------------------------------------------
//    // clase para ejecutar una tarea asincr�nica
//    // ------------------------------------------
//    public class obtenerResultado extends AsyncTask<String , Void, String>
//    
//	{   
//    	ProgressDialog pd = new ProgressDialog(RegistrationActivity.this);
//    	
//		// Se ejecuta antes de llamar a la tarea
//		@Override 
//		protected void onPreExecute() {
//			pd.setMessage(getString(R.string._reg_solicitandosms));
//			pd.show();
//			pd.setCancelable(false);
//			
//			super.onPreExecute();
//		}
//
//		
//		
//    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
//		@Override
//		protected String doInBackground(String... _texto) {
//
////			conexionWeb cw = new conexionWeb();
////			return cw.Obtener_Resultado();
//			
//			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(RegistrationActivity.this);
//			
//			
//			String _rta;
//			_rta = cw.Pedir_codigo_x_sms("" + _texto[0], "" + _texto[1]);
//			
//			if (_rta.equals("ok"))
//				_rta = "" + _texto[0];
//			else
//				_rta = "error";
//			
//			return _rta;
//		}
//	
//		@SuppressWarnings("deprecation")
//		@Override
//		protected void onPostExecute(String resultado) {
//			
//			if (pd.isShowing() || pd != null)
//				pd.dismiss();
//
//
//			if ( ! resultado.equals("error") )  
//			{
//				AgregarPreferencias(resultado, false);
//				Habilitar_ingreso_codigo(true);
//			}
//			else
//			{
//				 AlertDialog ad = new AlertDialog.Builder(RegistrationActivity.this).create();
//		    	 ad.setTitle(R.string._error);
//		    	 ad.setCancelable(false);
//		    	 
//	//	    	 ad.setIcon(R.drawable.icono);
//		    	 ad.setMessage(getResources().getString( R.string._reg_errorAlSolicitar));
//		    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//						
//					}
//				});
//		    	 
//		    	 ad.show();
//				
//				return;
//			}
//			
//			super.onPostExecute(resultado);
//		}
//
//	  }
//
//    
//    // ------------------------------------------
//    // clase para ejecutar una tarea asincr�nica
//    // ------------------------------------------
//    public class validarCodigo extends AsyncTask<String , Void, Boolean>
//	{   
//    	ProgressDialog pd = new ProgressDialog(RegistrationActivity.this);
//    	
//    	String _pTelefono;
//    	    	
//		// Se ejecuta antes de llamar a la tarea
//		@Override 
//		protected void onPreExecute() {
//			pd.setMessage(getString(R.string._reg_verificandoCodigo));
//			pd.show();
//			pd.setCancelable(false);
//			
//			super.onPreExecute();
//		}
//
//		
//		
//    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
//		@Override
//		protected Boolean doInBackground(String... _texto) {
//
////			conexionWeb cw = new conexionWeb();
////			return cw.Obtener_Resultado();
//			
//			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(RegistrationActivity.this);
//			
//			
//			Boolean _rta;
//			_rta = cw.Validar_codigo_x_sms("" + _texto[0], "" + _texto[1], "" + _texto[2]);
//			_pTelefono = "" + _texto[1];
//		
//			return _rta;
//		}
//	
//		@SuppressWarnings("deprecation")
//		@Override
//		protected void onPostExecute(Boolean resultado) {
//			
//			if (pd.isShowing() || pd != null)
//				pd.dismiss();
//
//
//			
//			if ( resultado )  
//			{
//				AgregarPreferencias(_pTelefono, true);
//			}
//			else
//			{
//				 AlertDialog ad = new AlertDialog.Builder(RegistrationActivity.this).create();
//		    	 ad.setTitle(R.string._error);
//		    	 ad.setCancelable(false);
//		    	 
//	//	    	 ad.setIcon(R.drawable.icono);
//		    	 ad.setMessage(getResources().getString( R.string._reg_errorCodigo));
//		    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//						
//					}
//				});
//		    	 
//		    	 ad.show();
//				
//				return;
//			}
//			
//			super.onPostExecute(resultado);
//		}
//
//	  }
}
