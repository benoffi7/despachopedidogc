package com.maker.lanuevasolodos;

import com.maker.funciones.Funciones;
import com.maker.soap.conexionServicioWebSOAP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ConfirmacionMapa extends Activity {

	// ------------------------------------------
	// Variables
	// ------------------------------------------
	private String _latitud, _longitud, _calle, _altura, _desde, _hasta, _demora, _NroTelefono, _Area, _zona, _localidad, _idLocalidad, _version;
	private TextView txtCalle, txt_altura;
	private EditText txtComentarios;
	Button  button_confirmar;
	Boolean _agregaAFavoritas = false, controlarAltura = false;
	CheckBox chkHabitual, chkBaul, chkCambio;
	TextView textViewPrueba, TextView_comentarioMap, textView_altura;
	String _textoAliasMovil;
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmacion);
		 
		findViews();
		
		// verificar si el intent no es nulo
		// si no es nulo lo tomo
		if (getIntent() != null)
		{
			// verificar si el intent no es nulo
			// si no es nulo lo tomo
			if (getIntent() != null)
			{
				// Si tiene extras
				if (getIntent().getExtras() != null)
					
					_NroTelefono = getIntent().getExtras().getString("telefono");
					_Area = getIntent().getExtras().getString("area");
					
					if (getIntent().getExtras().getString("calle") != null)
					{
						_latitud  		= getIntent().getExtras().getString("latitud");
						_longitud 		= getIntent().getExtras().getString("longitud");
						_altura   		= getIntent().getExtras().getString("altura").replace(" ", "");
						_desde    		= getIntent().getExtras().getString("desde");
						_hasta    		= getIntent().getExtras().getString("hasta");
						_calle    		= getIntent().getExtras().getString("calle");
						_demora   		= getIntent().getExtras().getString("demora");
						_idLocalidad  	= getIntent().getExtras().getString("idlocalidad");
						_localidad		= getIntent().getExtras().getString("localidad");
						_zona			= getIntent().getExtras().getString("zona");
						
						if ((_desde.equals("0") && _hasta.equals("0")) || _altura.indexOf("any") > -1)
						{
							controlarAltura = false;
							txtCalle.setText( _calle + "\n"+  _demora);
						}
						else
						{
							txtCalle.setText( _calle + " (" + _altura + ")\n"+ _demora);
							controlarAltura = true;
						}
					}
			}
			
		}
		
		// Crear eventos
		CreateEvents();
		
		// Si no controla altura entonces debe inhabilitar la altura
		if (!controlarAltura)
		{
			txt_altura.setEnabled(false);
			txtComentarios.requestFocus();
		}
		else
		{
			txt_altura.setEnabled(true);
			txt_altura.requestFocus();
		}
	}

	// ------------------------------------------
	// Sobreescribir que pasa si vuelve atras
	// ------------------------------------------
	@Override
	public void onBackPressed() {
		
		Intent intento = new Intent(ConfirmacionMapa.this, MapActivity.class);
		
		intento.putExtra("telefono", _NroTelefono);
		intento.putExtra("area", _Area);
		intento.putExtra("verificaViaje", "true");

	    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	    	
		startActivity(intento);
		finish();
		//super.onBackPressed();
	}
	
	// ------------------------------------------
	// Encontrar vistas
	// ------------------------------------------
	private void findViews() {
		
		txtCalle = (TextView)findViewById(R.id.cnf_textView_direccion);
		txtComentarios = (EditText)findViewById(R.id.EditText_comentarios);
		txtComentarios.setHint(R.string._hint_comentario);
		
		TextView_comentarioMap = (TextView)findViewById(R.id.TextView_comentarioMap);
		
		TextView_comentarioMap.setText(getResources().getString(R.string._comentario_mapa));
		button_confirmar = (Button)findViewById(R.id.conf_buttonConfirmar);
		
		// Altura, ocultar
		textView_altura = (TextView)findViewById(R.id.TextView_comentarioMap);
		textView_altura.setVisibility(View.VISIBLE);
		txt_altura = (EditText)findViewById(R.id.editText_AlturaConf);
		txt_altura.setVisibility(View.VISIBLE);
		
		txtComentarios.requestFocus();
		
		chkBaul = (CheckBox)findViewById(R.id.checkBoxBaul);
		chkCambio = (CheckBox)findViewById(R.id.checkBoxCambio);
		chkHabitual = (CheckBox)findViewById(R.id.checkBox_habitualConf);
		chkHabitual.setVisibility(View.INVISIBLE);
		
		PackageInfo info;
		PackageManager manager = this.getPackageManager();
		 try {
			info = manager.getPackageInfo(this.getPackageName(), 0);

			_version = "" + info.versionName;
		} catch (NameNotFoundException e) {
			Toast.makeText(this, "Error al obtener la versi�n", Toast.LENGTH_LONG).show();
			finish();
		}
		 
		txt_altura.requestFocus();
	}
	
	// ------------------------------------------
	// Encontrar vistas
	// ------------------------------------------
	private void CreateEvents() {
		button_confirmar.setOnClickListener(new OnClickListener() {

			Toast toast;
			@Override
			public void onClick(View v) {
				
				if (controlarAltura)
				{
					// verificar que la altura est� entre los rangos
					if (("" + txt_altura.getText()).trim().length() < 1)
					{
						toast = Toast.makeText(ConfirmacionMapa.this, R.string._conf_sin_altura, Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.TOP, 0, txtCalle.getTop());
						toast.show();
						return;
					}
					
					if (Integer.parseInt(("" + txt_altura.getText()).trim()) < Integer.parseInt(_desde) ||
							Integer.parseInt(("" + txt_altura.getText()).trim()) > Integer.parseInt(_hasta))
					{
						toast = Toast.makeText(ConfirmacionMapa.this, R.string._conf_altura_invalida, Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.TOP, 0, txtCalle.getTop());
						toast.show();
						return;
					}
				}	
				else
				{
					if (("" + txtComentarios.getText()).trim().length() < 3)
					{
						toast = Toast.makeText(ConfirmacionMapa.this, R.string._err_sinComentario, Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.TOP, 0, txtCalle.getTop());
						toast.show();
						return;
					}
				}
				
				_textoAliasMovil = generarTextoAliasMovilNuevo();

				// Llamar a la clase asincronica que pide el viaje
				/*
				 * Par�metros de confirmar pedido mapa
				  		String area, String numero, String latitud, 
					   String longitud, String idLocalidad, String localidad, 
					   String calleynro, String baul, String cambio, String mensaje,
					   String zona, String version
				 */

				// verificar el largo de la latitud y longitud, no puede pasar de 12
				if (_longitud.length() > 10)
					_longitud = "" + _longitud.subSequence(0, 10);
				if (_latitud.length() > 10)
					_latitud = "" + _latitud.subSequence(0, 10);
				
				confirmarResultado cr = new confirmarResultado();
				cr.execute(_Area, _NroTelefono,
						_latitud.replace(".", ","), _longitud.replace(".", ","), _idLocalidad, _localidad,
						_textoAliasMovil, chkBaul.isChecked() ? "true" : "false", chkCambio.isChecked() ? "true":"false", 
								"" + txtComentarios.getText(), _zona, _version);
				
				txtComentarios.setText("");
				
				Funciones.ocultarTeclado(ConfirmacionMapa.this, button_confirmar);
			}
		});
	}

	private String generarTextoAliasMovilNuevo() {
		
		String resultado = "";
	
		if (_desde.equals("0") && _hasta.equals("0"))
		{
			//resultado = _calle + " - " + ("" + txtComentarios.getText());

			resultado = _calle;

			if (!txtComentarios.getText().toString().equals(""))
				resultado = resultado + " - " + txtComentarios.getText();

			return resultado;
		}
		
		try
		{
			if (controlarAltura)
				resultado = _calle + " Nº " + txt_altura.getText();
			else
				resultado = _calle;

			if (!txtComentarios.getText().toString().equals(""))
				resultado = resultado + " - " + txtComentarios.getText();
		}
		catch (Exception ex)
		{
			resultado += " Nº " + txt_altura.getText();
		}

		if (resultado.length() > 80)
			resultado = resultado.substring(0, 79);
		
		return resultado;
	}

	// Ac� s�lo entrar�a en el supuesto caso de error de numero de telefono
	private void iniciarErrorTelefono()
	{
		SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferencias.edit();
		editor.putString("telefono", "");
		editor.putString("area", "");
		editor.putString("telefonoCompleto", "");
		editor.putString("habilitado", "");
        editor.apply();
		
		Intent intento = new  Intent(ConfirmacionMapa.this,RegistrationActivity.class);
		intento.putExtra("mensaje", _textoAliasMovil);
		intento.putExtra("telefono", _NroTelefono);
		intento.putExtra("area", _Area);
		intento.putExtra("esmapa", "1");
		intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		startActivity(intento);
		
		finish();
	}
	
	// ------------------------------------------
    // clase para ejecutar una tarea asincr�nica
    // ------------------------------------------
    public class confirmarResultado extends AsyncTask<String, Void, Respuesta>	{
    	String _mensaje = "";
    	
    	ProgressDialog pd = new ProgressDialog(ConfirmacionMapa.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._conf_confirmado_pedido_aguarde));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Respuesta doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(ConfirmacionMapa.this);
			
			// area, telefono, localidad 
			Respuesta _rta;
			/*
			 * Par�metros de confirmar pedido mapa
			  		String area, String numero, String latitud, 
				   String longitud, String idLocalidad, String localidad, 
				   String calleynro, String baul, String cambio, String mensaje,
				   String zona, String version
			 */
			
			_mensaje = ""+_texto[9];
			
			if (("" + _texto[0]).trim().length() < 1 || ("" + _texto[1]).trim().length() < 1)
			{
				iniciarErrorTelefono();
			}
			
			if (( "" + _texto[1]).contains("n") || ( "" + _texto[0]).contains("n"))
			{
				iniciarErrorTelefono();
			}
				
			_rta = cw.Confirmar_pedido_mapa("" + _texto[0], "" + _texto[1], "" + _texto[2], "" + _texto[3], "" + _texto[4], "" + _texto[5],  "" + _texto[6], "" + _texto[7], "" + _texto[8],  "" + _texto[9], "" + _texto[10], "" + _texto[11]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Respuesta resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();

			AlertDialog ad = new AlertDialog.Builder(ConfirmacionMapa.this).create();
			 
			if (resultado.getPedidoConfirmado())  
			{
				ad.setTitle(R.string._conf_titulo_viaje_confirmado);
		    	ad.setCancelable(false);
		    	 
				// ad.setIcon(R.drawable.icono);
		    	ad.setMessage(getResources().getString( R.string._conf_viaje_confirmado));
		    	ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
					@Override
				public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						Intent intento = new  Intent(ConfirmacionMapa.this,ConViajeActivity.class);
						intento.putExtra("mensaje", _textoAliasMovil );
						intento.putExtra("telefono", _NroTelefono);
						intento.putExtra("area", _Area);
						intento.putExtra("esmapa", "1");
						intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

						startActivity(intento);
						
						finish();
					}

				});
		    	 
				ad.show();
			}
			else
			{
                if (resultado.getNoHayPedidoRealizado())
                {
                    ad.setTitle(R.string._conf_pedido_reciente);
                    ad.setCancelable(false);

                    // ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._conf_hay_viaje_registrado));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });

                    ad.show();
                }
                else if (resultado.getNoPuedeSolicitar())
                {
                    ad.setTitle(R.string._error);
                    ad.setCancelable(false);

                    // ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._conf_error_conexcion));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });

                    ad.show();
                }
                else
                {
                    ad.setTitle(R.string._conf_no_hay_viaje_realizado);
                    ad.setCancelable(false);

                    // ad.setIcon(R.drawable.icono);
                    ad.setMessage(getResources().getString( R.string._msg_error_generico_viaje));
                    ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });

                    ad.show();
                }
            }
			
			super.onPostExecute(resultado);
		}
    }
}