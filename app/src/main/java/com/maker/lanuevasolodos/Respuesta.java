package com.maker.lanuevasolodos;

public class Respuesta {
    private Boolean pedirConfirmacion = false;
    private String texto;
    private Boolean mostrarAyuda = false;
    private Boolean callesAlternativas =false;
    private Boolean viajeReciente = false;
    private Boolean otraAgencia = false;
    private Boolean noPuedeSolicitar = false;
    private Boolean direccionInvalida = false;
    private Boolean  pedidoConfirmado = false;
    private Boolean pedidoCancelado = false;
    private Boolean noHayPedidoRealizado = false;
    private Boolean yaOcupado = false;
    private Boolean yaInformoMovil = false;
    private Boolean tieneMovil = false;
    private String Movil = "";
    private Boolean negativo = false;
    private int nroViaje = 0;
    private String calle1 = "";
    private String calle2 = "";
    private String direccion = "";
    private Boolean eliminado = false;
    private Boolean actualizacionObligatoria = false;
    private Boolean versionIncorrecta = false;
    private String uri = "";
    private Boolean servicioOffLine = false;
    private Boolean esAdministrador = false;
    private String abrLocalidad = "";
    private Boolean esmapa = true;

    private Boolean error_conexion = false;
    private Boolean error_conexionServidor = false;

    public String getUri() {
        return uri;
    }

    public Boolean getError_conexionServidor() {
        return error_conexionServidor;
    }

    public void setError_conexionServidor(Boolean error_conexionServidor) {
        this.error_conexionServidor = error_conexionServidor;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Boolean getError_conexion() {
        return error_conexion;
    }

    public void setError_conexion(Boolean error_conexion) {
        this.error_conexion = error_conexion;
    }

    public Boolean getEsmapa() {
		return esmapa;
	}
	public void setEsmapa(Boolean esmapa) {
		this.esmapa = esmapa;
	}
	public String getAbrLocalidad() {
		return abrLocalidad;
	}
	public void setAbrLocalidad(String abrLocalidad) {
		this.abrLocalidad = abrLocalidad;
	}
	public Boolean getEsAdministrador() {
		return esAdministrador;
	}
	public void setEsAdministrador(Boolean esAdministrador) {
		this.esAdministrador = esAdministrador;
	}
	public Boolean getServicioOffLine() {
		return servicioOffLine;
	}
	public void setServicioOffLine(Boolean servicioOffLine) {
		this.servicioOffLine = servicioOffLine;
	}
	public String getURI() {
		return uri;
	}
	public void setURI(String _uri) {
		this.uri = _uri;
	}
	public Boolean getActualizacionObligatoria() {
		return actualizacionObligatoria;
	}
	public void setActualizacionObligatoria(Boolean actualizacionObligatoria) {
		this.actualizacionObligatoria = actualizacionObligatoria;
	}
	public Boolean getVersionIncorrecta() {
		return versionIncorrecta;
	}
	public void setVersionIncorrecta(Boolean versionIncorrecta) {
		this.versionIncorrecta = versionIncorrecta;
	}
	public Boolean getEliminado() {
		return eliminado;
	}
	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	
    public String getCalle1() {
		return calle1;
	}
	public void setCalle1(String calle1) {
		this.calle1 = calle1;
	}
	public String getCalle2() {
		return calle2;
	}
	public void setCalle2(String calle2) {
		this.calle2 = calle2;
	}
	public int getNroViaje() {
		return nroViaje;
	}
	public void setNroViaje(int nroViaje) {
		this.nroViaje = nroViaje;
	}
	public Boolean getNegativo() {
		return negativo;
	}
	public void setNegativo(Boolean negativo) {
		this.negativo = negativo;
	}
	public String getMovil() {
		return Movil;
	}
	public void setMovil(String Movil) {
		this.Movil = Movil;
	}
	
	public Boolean getYaOcupado() {
		return yaOcupado;
	}
	public void setYaOcupado(Boolean yaOcupado) {
		this.yaOcupado = yaOcupado;
	}
	public Boolean getYaInformoMovil() {
		return yaInformoMovil;
	}
	public void setYaInformoMovil(Boolean yaInformoMovil) {
		this.yaInformoMovil = yaInformoMovil;
	}
	public Boolean getTieneMovil() {
		return tieneMovil;
	}
	public void setTieneMovil(Boolean tieneMovil) {
		this.tieneMovil = tieneMovil;
	}
	public Boolean getPedidoConfirmado() {
		return pedidoConfirmado;
	}
	public void setPedidoConfirmado(Boolean pedidoConfirmado) {
		this.pedidoConfirmado = pedidoConfirmado;
	}
	public Boolean getPedidoCancelado() {
		return pedidoCancelado;
	}
	public void setPedidoCancelado(Boolean pedidoCancelado) {
		this.pedidoCancelado = pedidoCancelado;
	}
	public Boolean getNoHayPedidoRealizado() {
		return noHayPedidoRealizado;
	}
	public void setNoHayPedidoRealizado(Boolean noHayPedidoRealizado) {
		this.noHayPedidoRealizado = noHayPedidoRealizado;
	}
	public Boolean getPedirConfirmacion() {
		return pedirConfirmacion;
	}
	public void setPedirConfirmacion(Boolean pedirConfirmacion) {
		this.pedirConfirmacion = pedirConfirmacion;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Boolean getMostrarAyuda() {
		return mostrarAyuda;
	}
	public void setMostrarAyuda(Boolean mostrarAyuda) {
		this.mostrarAyuda = mostrarAyuda;
	}
	public Boolean getCallesAlternativas() {
		return callesAlternativas;
	}
	public void setCallesAlternativas(Boolean callesAlternativas) {
		this.callesAlternativas = callesAlternativas;
	}
	public Boolean getViajeReciente() {
		return viajeReciente;
	}
	public void setViajeReciente(Boolean viajeReciente) {
		this.viajeReciente = viajeReciente;
	}
	public Boolean getOtraAgencia() {
		return otraAgencia;
	}
	public void setOtraAgencia(Boolean otraAgencia) {
		this.otraAgencia = otraAgencia;
	}
	public Boolean getNoPuedeSolicitar() {
		return noPuedeSolicitar;
	}
	public void setNoPuedeSolicitar(Boolean noPuedeSolicitar) {
		this.noPuedeSolicitar = noPuedeSolicitar;
	}
	public Boolean getDireccionInvalida() {
		return direccionInvalida;
	}
	public void setDireccionInvalida(Boolean direccionInvalida) {
		this.direccionInvalida = direccionInvalida;
	}
    
    
}
