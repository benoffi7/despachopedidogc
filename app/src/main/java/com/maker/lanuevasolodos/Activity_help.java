package com.maker.lanuevasolodos;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Activity_help extends Activity {

	Button btnVolver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		btnVolver = (Button)findViewById(R.id.button_volver_help);

	
		btnVolver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();;
				
			}
		});
	}


}
