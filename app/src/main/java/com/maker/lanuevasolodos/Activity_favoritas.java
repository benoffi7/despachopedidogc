package com.maker.lanuevasolodos;

import android.app.Activity;
import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
//import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class Activity_favoritas extends Activity  {

	// ------------------------------------------
	// Variables de componentes
	// ------------------------------------------
	ListView listView;
	Button button_Eliminar, button_Seleccionar;
	Adaptador adaptador;
		
	// ------------------------------------------
	// Variables comunes
	// ------------------------------------------
	String _NroTelefono, _Area;
	
	// ------------------------------------------
	// Base de datos
	// ------------------------------------------
	BaseDeDatos bdd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favoritas);
		
		if (getIntent().getExtras().getString("telefono") != null)
		{
			_NroTelefono = getIntent().getExtras().getString("telefono");
			_Area= getIntent().getExtras().getString("area");
		}
		
	    // Find Views
	    findViews();
	    
	    // Iniciar base de datos
	   	bdd = new BaseDeDatos(getApplicationContext(), getResources().getString(R.string.DB_nombre) , 
				null , Integer.parseInt(getResources().getString(R.string.DB_version)));
	   	
	    //Setear adaptador
		setAdapter();

	    // Setear eventos
		setEvents();
	}
	


	// --------------------------------------------
	// Obtener el id de cada componente xml
	// 99------------------------------------------
    private void findViews() {
		
    	listView = (ListView)findViewById(R.id.fav_listView_direcciones);
    	button_Seleccionar = (Button)findViewById(R.id.fav_button_Seleccionar) ;
    	button_Eliminar = (Button)findViewById(R.id.fav_button_Eliminar) ;
    	
	}
	// --------------------------------------------
	// Setear los eventos de los botones
	// ------------------------------------------
    private void setEvents() {
    	button_Seleccionar.setOnClickListener(new OnClickListener() {
			
    		// Llamar a la activityMain
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View vista) {
				
				// Obtener los datos de la base de datos con el id de la favorita elegida
				if (adaptador.direccion.getCalle() != null )
				{

					Intent intento = new  Intent(Activity_favoritas.this, MainActivity.class);
					
					intento.putExtra("calle", adaptador.direccion.getCalle());
				    intento.putExtra("altura", ""+adaptador.direccion.getAltura());
	     	    	intento.putExtra("esquina", adaptador.direccion.getEsquina().toString());
					intento.putExtra("comentario", adaptador.direccion.getComentario().toString());
					intento.putExtra("localidad", adaptador.direccion.getLocalidad().toString());
					intento.putExtra("idLocalidad", ""+adaptador.direccion.getIdLocalidad());
					intento.putExtra("telefono", _NroTelefono);
					intento.putExtra("area",_Area);
					intento.putExtra("verificaViaje", "false");

					
					intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intento);
					finish();
				}
				else
				{
					 AlertDialog ad = new AlertDialog.Builder(Activity_favoritas.this).create();
			    	 ad.setTitle(R.string._error);
			    	 ad.setCancelable(false);
			    	 
		//	    	 ad.setIcon(R.drawable.icono);
			    	 ad.setMessage(getResources().getString( R.string._fav_dlg_sin_seleccionar));
			    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			    	 
			    	 ad.show();
				}
			}
		});
    
    	// Bot�n eliminar favorita
    	button_Eliminar.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View arg0) {
				if (adaptador.direccion.getCalle() != null )
				{
					// No anda con versiones viejas, la clase DialogoPesonalizado esta mas abajo
//				DialogoPersonalizado dp = new DialogoPersonalizado();
//				dp.show(getFragmentManager(), null);
					
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Activity_favoritas.this);
			 
						// set title
						alertDialogBuilder.setTitle(getString(R.string.title_activity_favoritas));
						// set dialog message
						alertDialogBuilder
							.setMessage(getString(R.string._dlg_sino_eliminarFavorita))
							.setCancelable(false)
							.setPositiveButton(getString(R.string._si),new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, close
									// current activity
									bdd.Eliminar_direccion(adaptador.direccion.getIdFavorita());
									//Setear adaptador
									setAdapter();
								}
							  })
							.setNegativeButton(getString(R.string._no),new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, just close
									// the dialog box and do nothing
									dialog.cancel();
								}
							});
			 
							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();
			 
							// show it
							alertDialog.show();
				}
				else
				{
					 AlertDialog ad = new AlertDialog.Builder(Activity_favoritas.this).create();
			    	 ad.setTitle(R.string._error);
			    	 ad.setCancelable(false);
			    	 
	//		    	 ad.setIcon(R.drawable.icono);
			    	 ad.setMessage(getResources().getString( R.string._fav_dlg_sin_seleccionar));
			    	 ad.setButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			    	 
			    	 ad.show();
				}
			}
		});
    }
	

	public float convertFromDp(int input) {
	    final float scale = getResources().getDisplayMetrics().density;
	    return ((input - 0.5f) / scale);
	}
	
	// -----------------------------
    // Crear el adaptador
    // -----------------------------
	private void setAdapter(){                                    
		
		//Creo una instancia del adaptador
		//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), this);
		//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), getApplicationContext());
		
		adaptador = new Adaptador(bdd.Obtener_Direcciones(), Activity_favoritas.this);
		
		//Setea el adapatador a la lista
		listView.setAdapter(adaptador);

				                                                                                                                                                                                                                                                                                                                                  
	}
	
	
	// ------------------------------------------
	// clase dialog box
	// ------------------------------------------
//	public class DialogoPersonalizado extends DialogFragment {
//
//		public Boolean rta = false;
//		@SuppressLint("InflateParams") @Override
//		public Dialog onCreateDialog(final Bundle savedInstanceState) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//		
//		LayoutInflater inflater = getActivity().getLayoutInflater();
//
//		builder.setView(inflater.inflate(R.layout.dialogeliminadireccion, null))
//			.setNegativeButton(getString(R.string._no), new  DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//					
//				}
//			})
//			.setPositiveButton(getString(R.string._si), new DialogInterface.OnClickListener() {
//		
//				public void onClick(DialogInterface dialog, int id) {
//					
//					try
//					{
//						bdd.Eliminar_direccion(adaptador.direccion.getIdFavorita());
//						//Setear adaptador
//						setAdapter();
//					}
//					catch( Exception ex)
//					{
//						
//					}
//					dialog.dismiss();
//				}
//		}
//
//			);
//		return builder.create();
//		}
//		
//	}
	
    
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_favoritas, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
