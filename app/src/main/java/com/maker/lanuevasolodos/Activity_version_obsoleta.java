package com.maker.lanuevasolodos;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class Activity_version_obsoleta extends Activity {

	Button _btnSalir, _btnIrAPlay;
	String _direccion = "";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_version_obsoleta);
		
		if (getIntent().getExtras() != null)
			if (getIntent().getExtras().getString("direccion") != null)
			{
				_direccion = getIntent().getExtras().getString("direccion");
				
			}
		
		_btnSalir = (Button)findViewById(R.id.button_salir_obsoleto);
		_btnIrAPlay = (Button)findViewById(R.id.button_ir_a_play);
		
		_btnSalir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		_btnIrAPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Intent intent = new Intent(Intent.ACTION_VIEW);
				
				intent.setData(Uri.parse(_direccion));
				startActivity(intent);
				finish();
			}
		});
	}

	
}
