package com.maker.lanuevasolodos;

import com.maker.soap.conexionServicioWebSOAP;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

public class CalificarActivity extends Activity {

	RatingBar rbEstrellas;
	Button btnVolver, btnCalificar;
	String _nroViaje;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calificar);

		_nroViaje = "0";
		
		if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
				if (getIntent().getExtras().getString("nroViaje") != null)
					_nroViaje = getIntent().getExtras().getString("nroViaje");
				else
					finish();
		}
		
		btnVolver = (Button)findViewById(R.id.btnCalificaVolver);
		btnCalificar = (Button)findViewById(R.id.btnCalificaChofer);
		
		rbEstrellas = (RatingBar)findViewById(R.id.ratingBarCho);
		rbEstrellas.setRating(3);
		rbEstrellas.setNumStars(5);
		rbEstrellas.setStepSize((float)1);
		
		createEvents();
	}

	private void createEvents() {
		// Volver
		btnVolver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		// Calificar
		btnCalificar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				calificarViaje cv = new calificarViaje();
				cv.execute(_nroViaje, "" + ((int)rbEstrellas.getRating()));
			}
		});
	}

    // ------------------------------------------
    // clase para saber si tiene un viaje reciente
    // ------------------------------------------
    public class calificarViaje extends AsyncTask<String , Void, Boolean> {
    	 
    	String _nroViajeACalificar = "";
    	ProgressDialog pd = new ProgressDialog(CalificarActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getString(R.string._rat_enviandoCalificacion));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}
		
    	// Aca se ejecuta lo que quiero que se haga en el 2do hilo (autom�tico!)
		@Override
		protected Boolean doInBackground(String... _texto) {

			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(CalificarActivity.this);
			
			Boolean _rta;
			_nroViajeACalificar = "" + _texto[0];
			
			_rta = cw.Calificar_viaje("" + _texto[0], "" + _texto[1]);
			
			return _rta;
		}
	
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Boolean r) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();
			
			if (r)
			{
				SharedPreferences preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

				// crear el editor de preferencias
				SharedPreferences.Editor editor = preferencias.edit();
				 
				// Agregar como preferencia el telefono
				editor.putInt("nroViajeCalificado", Integer.parseInt(_nroViajeACalificar));

                editor.apply();
					
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CalificarActivity.this);
				 
				// set title
				alertDialogBuilder.setTitle(getString(R.string.title_activity_calificar));
				// set dialog message
				alertDialogBuilder
					.setMessage(getString(R.string._rat_ok))
					.setCancelable(false)
					.setPositiveButton(getString(R.string._ok),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								btnCalificar.setEnabled(false);
								dialog.dismiss();
								finish();
							}
					  	}
					);
	 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
			else
			{
				Toast.makeText(CalificarActivity.this, getString(R.string._rat_error), Toast.LENGTH_SHORT).show();
				
				finish();
			}
				
			super.onPostExecute(r);
		}
	}
}
