package com.maker.funciones;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Funciones {
    // ------------------------------------------
    // Control para ver si hay internet
    // ------------------------------------------
	public static boolean hayInternet(Context contexto) 
	{
		ConnectivityManager connectivityManager = (ConnectivityManager)contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	
    // ------------------------------------------
    // Control para ver si hay internet
    // ------------------------------------------
	public static void ocultarTeclado(Context contexto, View view) 
	{	// Ocultar teclado
		 InputMethodManager teclado = (InputMethodManager)contexto.getSystemService(Context.INPUT_METHOD_SERVICE);
		 teclado.hideSoftInputFromWindow(view.getWindowToken(),0);

	}
	

}
